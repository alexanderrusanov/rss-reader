(function() {
  var dao, daomodule, feedparser, getUtcNow, htmlparser, request;

  daomodule = require('./dao');

  feedparser = require('feedparser');

  request = require('request');

  htmlparser = require('htmlparser2');

  dao = daomodule.getDao();

  exports.discoveryFeeds = function(url, callback) {
    var getFeed;
    getFeed = function(url, callback) {
      return request(url).on('error', function(err) {
        return callback(err);
      }).pipe(new feedparser).on('error', function(err) {
        return callback(err);
      }).on('meta', function(meta) {
        return callback(null, meta);
      });
    };
    return getFeed(url, function(err, meta) {
      if (err) {
        return request(url, function(err, res, body) {
          var feeds, parser, recursiveGetFeeds, urls;
          if (!err && res.statusCode === 200) {
            urls = [];
            parser = new htmlparser.Parser({
              onopentag: function(name, attrs) {
                if (name === 'link' && attrs.rel === 'alternate' && (attrs.href != null)) {
                  return urls.push(attrs.href);
                }
              },
              onclosetag: function(name) {
                if (name === 'head') {
                  return parser.reset();
                }
              }
            });
            parser.write(body);
            parser.end();
            feeds = [];
            recursiveGetFeeds = function(urls) {
              url = urls.pop();
              if (url != null) {
                return getFeed(url, function(err, meta) {
                  if (!err) {
                    feeds.push(meta);
                  }
                  return recursiveGetFeeds(urls);
                });
              } else {
                return callback(null, feeds);
              }
            };
            return recursiveGetFeeds(urls);
          } else {
            return callback(err != null ? err : new Error("Http request " + url + " return " + res.statusCode + " status."));
          }
        });
      } else {
        return callback(null, [meta]);
      }
    });
  };

  exports.discoveryItems = function(url, callback) {
    var error, items;
    items = [];
    error = null;
    return request(url).on('error', function(err) {
      return callback(err);
    }).pipe(new feedparser).on('error', function(err) {
      return error = err;
    }).on('readable', function() {
      var item, stream, _results;
      stream = this;
      _results = [];
      while (item = stream.read()) {
        _results.push(items.push(item));
      }
      return _results;
    }).on('end', function() {
      return callback(error, items);
    });
  };

  exports.discoveryAndSaveItems = function(feed, callback) {
    return this.discoveryItems(feed.link, function(err, items) {
      if (err) {
        return callback(err);
      } else {
        return dao.feedLastUpdate(feed.id, function(err) {
          if (err) {
            return callback(err);
          } else {
            return dao.saveItems(feed.id, items, function(err) {
              return callback(err);
            });
          }
        });
      }
    });
  };

  exports.getFeeds = function(user_id, callback) {
    return dao.getFeeds(user_id, callback);
  };

  exports.saveFeed = function(user_id, feed, callback) {
    return dao.saveFeed(user_id, feed, callback);
  };

  exports.removeFeed = function(user_id, feed_id, callback) {
    return dao.removeFeed(user_id, feed_id, callback);
  };

  exports.tagFeed = function(user_id, feed_id, tag, callback) {
    return dao.tagFeed(user_id, feed_id, tag, callback);
  };

  exports.getItems = function(feed_id, options, callback) {
    var _this = this;
    if (feed_id === "recommended") {
      return callback(new Error('Not Implemented'));
    }
    if (feed_id == null) {
      feed_id = "all";
    }
    if (feed_id === "starred") {
      options.starred = true;
    }
    if (feed_id.substring(0, 4) === "tag:") {
      options.tag = feed_id.substring(4);
    }
    if (isNaN(parseInt(feed_id))) {
      return dao.getItems(options, function(err, items) {
        if (err) {
          return callback(err);
        } else {
          return dao.getItemsCount(options, function(err, count) {
            if (err) {
              return callback(err);
            } else {
              return callback(null, {
                id: feed_id,
                unread: count
              }, items);
            }
          });
        }
      });
    } else {
      return dao.getFeed(feed_id, function(err, feed) {
        if (err) {
          return callback(err);
        }
        if (!feed) {
          return callback(new Error('Not Found'));
        }
        options.feed_id = feed.id;
        if (getUtcNow() - feed.lastupdate > 1000 * 60 * 5) {
          return _this.discoveryAndSaveItems(feed, function(err) {
            if (err) {
              return callback(err);
            } else {
              return dao.getItems(options, function(err, items) {
                return callback(err, feed, items);
              });
            }
          });
        } else {
          return dao.getItems(options, function(err, items) {
            return callback(err, feed, items);
          });
        }
      });
    }
  };

  exports.readItems = function(ids, read, callback) {
    return dao.readItems(ids, read, callback);
  };

  exports.starItem = function(item_id, state, callback) {
    return dao.starItem(item_id, state, callback);
  };

  getUtcNow = function() {
    var now;
    now = new Date();
    return now.getTime() + now.getTimezoneOffset() * 60000;
  };

}).call(this);
