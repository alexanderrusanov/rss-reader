configs = require('../../configs');
pg = require 'pg'
anydb = require 'any-db'
crypto = require 'crypto'
pgQuery = require './pgQuery'

class Dao

  constructor: (@connectionString) ->
    @pool = anydb.createPool @connectionString, {min: 1, max: 100}
    @selectFeed =
      'select s.id id, s.user_id, s.url link, s.name title, last_update lastUpdate, s."order", ' +
      'array(select t.tag from tags t where t.user_id = s.user_id and t.sub_id = s.id) tag, ' +
      '(select count(*) from items i where i.sub_id = s.id and not i.readed) unread from subs s '
    process.on 'exit', =>
      @close()

  close: ->
    @pool.close() if @pool


  getUser: (id, callback) ->
    @single 'select id, name from users where id = $1', [id], callback

  saveUser: (user, callback) ->
    @single 'select "saveUser"($1, $2)', [user.id, user.name], (err, row) ->
      callback err, user if callback


  getFeeds: (user_id, callback) ->
    @select @selectFeed + 'where user_id = $1 order by s."order"', [user_id], callback

  getFeed: (id, callback) ->
    @single @selectFeed + 'where id = $1', [id], callback

  saveFeed: (user_id, feed, callback) ->
    @single 'select "saveFeed"($1, $2, $3, $4)', [feed.id, user_id, feed.xmlurl ? feed.link, feed.title], (err, row) ->
      if err
        callback err if callback
      else
        feed.id = row.saveFeed
        feed.tag = []
        feed.order = 0
        callback null, feed if callback

  removeFeed: (user_id, feed_id, callback) ->
    @single 'select "removeFeed"($1,$2)', [user_id, feed_id], callback

  feedLastUpdate: (feed_id, callback) ->
    @single "update subs set last_update = timezone('UTC', now()) where id = $1", [feed_id], callback

  tagFeed: (user_id, feed_id, tag, callback) ->
    @single 'select "saveTag"($1,$2,$3)', [user_id, feed_id, tag], callback


  getItems: (options, callback) ->
    select = pgQuery
      .select(["i.id", "i.sub_id", "i.link", "i.title", "i.text", "i.author", "i.categories", "i.created","not i.readed unread", "i.starred"])
      .from("items i")
      .order("i.created " + if options.order? and options.order == "oldest" then "asc" else "desc")
      .limit(options.to ? 25)
      .offset(options.from ? 0)

    select.where "i.sub_id = @feed_id", options if options.feed_id? and 0 < options.feed_id

    select.where "i.readed != @unread", options if options.unread?

    select.where "i.starred = @starred", options if options.starred?

    select.from("tags t").where("i.sub_id = t.sub_id and t.user_id = @user_id and t.tag = @tag",options) if options.tag? and options.user_id?

    @select select.toString(), select.getParams(), callback

  getItemsCount: (options, callback)->
    if (options.starred? and options.starred) or !options.user_id?
      return callback null, 0

    select = pgQuery
      .select(["count(*)"])
      .from("subs s")
      .from("items i")
      .where("s.id = i.sub_id and i.readed = false and s.user_id = @user_id", options)

    select.where "s.id = @feed_id", options if options.feed_id? and 0 < options.feed_id

    if options.tag?
      select
        .from("tags t")
        .where("t.user_id = s.user_id and t.sub_id = s.id and t.tag = @tag", options)

    @select select.toString(), select.getParams(), callback

  saveItems: (feed_id, items, callback) ->
    saveItem = (tx, items, callback) ->
      item = items.pop()
      if item?
        hash = crypto.createHash('md5').update(item.link ? item.title + item.description).digest("hex")
        tx.query(
          'select "addItem"($1,$2,$3,$4,$5,$6,$7,$8)',
          [feed_id, hash, item.link, item.title, item.description, item.author, item.categories, item.pubdate],
          (err)->
            if err
              tx.rollback()
              callback err
            else
              saveItem tx, items, callback
        )
      else
        tx.commit()
        callback null

    if 0 < items.length
      tx = @pool.begin()
      saveItem tx, items, callback
    else
      callback null

  readItems: (items, read, callback) ->
    readItem = (tx, items, callback) ->
      item = items.pop()
      if item?
        tx.query 'update items set readed = $2 where id = $1', [item, read], (err) ->
          if err
            tx.rollback()
            callback err
          else
            readItem tx, items, callback
      else
        tx.commit()
        callback null

    if 0 < items.length
      tx = @pool.begin()
      readItem tx, items, callback
    else
      callback null

  starItem: (item_id, state, callback) ->
    @single 'update items set starred = $2 where id = $1', [item_id, state], callback


  single: (select, params, callback) ->
    q = @pool.query select, params
    result = null
    q.on 'error', (err) ->
      callback err if callback
    q.on 'row', (row) ->
      result = row
    q.on 'end', (end) ->
      callback null, result if callback and end

  select: (select, params, callback) ->
    q = @pool.query select, params
    result = []
    q.on 'error', (err) ->
      callback err if callback
    q.on 'row', (row) ->
      result.push row
    q.on 'end', (end) ->
      callback null, result if callback and end


exports.getDao = () ->
  new Dao(configs.connectionString)