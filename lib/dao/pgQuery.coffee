
class pgSelect

  constructor: ->
    @tables = []
    @fields = []
    @whereExp = ""
    @params = []

  from: (table) ->
    @tables.push table
    this

  field: (field) ->
    @fields.push field
    this

  where: (where, params) ->
    if params?
      for own key, value of params
        if 0 <= where.indexOf "@" + key
          @params.push value
          where = where.replace("@" + key, "$#{@params.length}")

    @whereExp += " and " if @whereExp != ""
    @whereExp += where
    this

  order: (@orderExp) ->
    this

  limit: (@limitValue) ->
    this

  offset: (@offsetValue) ->
    this

  toString: ->
    sql = "select "

    (sql += f + ", ") for f in @fields
    sql = sql.substring(0, sql.length - 2)

    sql += " from "
    (sql += t + ", ") for t in @tables
    sql = sql.substring(0, sql.length - 2)

    sql += " where " + @whereExp if @whereExp != ""

    sql += " order by " + @orderExp if @orderExp?

    sql += " limit " + @limitValue if @limitValue?
    sql += " offset " + @offsetValue if @offsetValue?
    return sql

  getParams: ->
    @params

exports.select = (fields) ->
  query = new pgSelect
  query.field f for f in fields if fields?
  return query
