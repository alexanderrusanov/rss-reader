--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

DROP DATABASE rss;
--
-- Name: rss; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE rss WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_United States.1252' LC_CTYPE = 'English_United States.1252';


ALTER DATABASE rss OWNER TO postgres;

\connect rss

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: addItem(integer, character varying, character varying, character varying, character varying, character varying, character varying[], timestamp without time zone); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "addItem"(integer, character varying, character varying, character varying, character varying, character varying, character varying[], timestamp without time zone) RETURNS integer
    LANGUAGE plpgsql
    AS $_$
begin
  loop
    update items set link = $3, title = $4, text = $5, author = $6, categories = $7, created = $8, readed = (readed and created = $8) where sub_id = $1 and hash = $2;
    if found then
      return 0;
    end if;
    begin
      insert into items(sub_id, hash, link, title, text, author, categories, created) values ($1,$2,$3,$4,$5,$6,$7,$8);
      return currval('seq_items');
    exception when unique_violation then
    end;
  end loop;
end
$_$;


ALTER FUNCTION public."addItem"(integer, character varying, character varying, character varying, character varying, character varying, character varying[], timestamp without time zone) OWNER TO postgres;

--
-- Name: removeFeed(character varying, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "removeFeed"(character varying, integer) RETURNS void
    LANGUAGE plpgsql
    AS $_$
begin
  delete from subs where user_id = $1 and id = $2;
end$_$;


ALTER FUNCTION public."removeFeed"(character varying, integer) OWNER TO postgres;

--
-- Name: saveFeed(integer, character varying, character varying, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "saveFeed"(integer, character varying, character varying, character varying) RETURNS integer
    LANGUAGE plpgsql
    AS $_$
begin
  if $2 is not null and $3 is not null then
    loop
      select id into $1 from subs where user_id = $2 and url = $3;
      if not found then
        begin
          insert into subs(id, user_id, url, name) values (default, $2, $3, $4) returning id into $1;
          return $1;
        exception when unique_violation then
        end;
      else
        update subs set name = $4 where id = $1;
        return $1;
      end if;
    end loop;
  elseif $1 is not null and $4 is not null then
    update subs set name = $4 where id = $1;
    return $1;
  else
    raise exception 'Invalid parameters.';
  end if;
end;
$_$;


ALTER FUNCTION public."saveFeed"(integer, character varying, character varying, character varying) OWNER TO postgres;

--
-- Name: saveTag(character varying, integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "saveTag"(character varying, integer, character varying) RETURNS integer
    LANGUAGE plpgsql
    AS $_$
begin
  begin
    insert into tags (user_id, sub_id, tag) values ($1,$2,$3);
  exception when unique_violation then
    delete from tags where user_id = $1 and sub_id = $2 and tag = $3;
  end;  
  return 0;
end
$_$;


ALTER FUNCTION public."saveTag"(character varying, integer, character varying) OWNER TO postgres;

--
-- Name: saveUser(character varying, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION "saveUser"(character varying, character varying) RETURNS void
    LANGUAGE plpgsql
    AS $_$
BEGIN
  LOOP
    UPDATE users SET name = $2 WHERE id = $1;
    IF found THEN
      RETURN;
    END IF;
    BEGIN
      INSERT INTO users(id, name) VALUES ($1, $2);
      RETURN;
    EXCEPTION WHEN unique_violation THEN
    END;
  END LOOP;
END;
$_$;


ALTER FUNCTION public."saveUser"(character varying, character varying) OWNER TO postgres;

--
-- Name: seq_items; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE seq_items
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_items OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: items; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE items (
    id integer DEFAULT nextval('seq_items'::regclass) NOT NULL,
    sub_id integer NOT NULL,
    hash character varying(2048),
    link character varying(2048),
    title character varying(2048),
    text text,
    author character varying(2048),
    categories character varying[],
    created timestamp without time zone,
    readed boolean DEFAULT false NOT NULL,
    starred boolean DEFAULT false NOT NULL
);


ALTER TABLE public.items OWNER TO postgres;

--
-- Name: seq_subs; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE seq_subs
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_subs OWNER TO postgres;

--
-- Name: subs; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE subs (
    id integer DEFAULT nextval('seq_subs'::regclass) NOT NULL,
    user_id character varying(128) NOT NULL,
    url character varying(4096) NOT NULL,
    name character varying(1024),
    last_update timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone NOT NULL,
    "order" integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.subs OWNER TO postgres;

--
-- Name: tags; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tags (
    user_id character varying(128) NOT NULL,
    sub_id integer DEFAULT 0 NOT NULL,
    tag character varying(128) NOT NULL
);


ALTER TABLE public.tags OWNER TO postgres;

--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE users (
    id character varying(128) NOT NULL,
    name character varying(1024)
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Data for Name: items; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (257, 4, 'da321b2839ab580615185eb9ff110039', 'http://habrahabr.ru/post/185782/', 'VDS в Нидерландах Intel Xeon X3440 / 2-3 GB RAM / 220 GB HDD RAID1 / 25Mbps Unmetered — $29 / месяц', 'Приветствую!<br/>
<br/>
Не так давно <a href="http://habrahabr.ru/company/ua-hosting/blog/185692/">мы анонсировали распродажу выделенных серверов</a> в Нидерландах в ЦОД премиум-качества EvoSwitch:<br/>
<br/>
Intel Quad-Core Xeon X3440 / 8GB DDR3 / 2x1TB SATA2 / 100Mbps Unmetered — $89 / месяц<br/>
Заказать можно тут: <a href="http://ua-hosting.com.ua/nl-servers.html">http://ua-hosting.com.ua/nl-servers.html</a><br/>
<br/>
Сегодня мы решили сделать эти серверы еще более доступными. Теперь у Вас есть возможность арендовать четверть этого сервера, в виде виртуального сервера, по невероятно низкой цене:<br/>
<br/>
VDS Intel Xeon X3440 / 2-3 GB RAM / 220 GB HDD RAID1 / 25Mbps Unmetered — $29 / месяц<br/>
Заказать можно тут: <a href="http://ua-hosting.com.ua/vds.html">http://ua-hosting.com.ua/vds.html</a><br/>
<br/>
Сразу честно скажу, что помимо преимуществ у предложения есть недостаток: разграничить IOPS (операции чтения / записи) на VDS невозможно, потому этот ресурс рассчитан на честное использование. Тем не менее, на ноде у Вас будет всего лишь 3 соседа, а это означает, что зачастую Вы получите ресурсов больше, чем предусмотрено тарифным планом. Мы специально не ставили жестких ограничений на процессор и память, так как показала практика — в этом нет никакого смысла, лучше дать возможность каждому из пользователей завершить операцию, как можно скорее, тем самым освободив ресурсы и сократив число IOPS и зависших процессов.<br/>
<br/>
<img src="http://habrastorage.org/storage2/751/40a/0e8/75140a0e81f62deb456a93eee4013302.jpg"/><img src="http://habrastorage.org/storage2/48c/025/2ab/48c0252ab46d4bb10129ca536c1d296f.jpg"/> <a href="http://habrahabr.ru/post/185782/#habracut">Читать дальше &rarr;</a>', 'HostingManager', '{"Блог компании «ua-hosting.com.ua»","серверы в нидерландах",vps,vds}', '2013-07-06 22:50:59', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (401, 12, '994528a9f20ab5ac178222a180a7f4a0', 'http://lenta.ru/news/2013/07/12/assault/', 'Убитый украинским инкассатором бандит оказался бывшим сотрудником СБУ', 'Один из преступников, совершивших вооруженное нападение на инкассаторскую машину «Укрпочты» в городе Николаев, оказался бывшим сотрудником Службы безопасности Украины. Об этом сообщили источники в областном управлении милиции. Нападение на инкассаторов было совершено утром 12 июля, оба преступника были убиты.', NULL, '{"Бывший СССР"}', '2013-07-12 15:22:00', false, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (254, 4, 'f373ded2a316e8f737df493cccc952c3', 'http://habrahabr.ru/post/185738/', 'Нанософт: мы говорим о nanoCAD', 'nanoCAD — российская универсальная САПР-платформа, содержащая мощные инструменты базового проектирования, выпуска чертежей и разработки приложений с помощью открытого API. И в июне 2013 года компания «Нанософт» объявила о выходе пятой версии своего основного продукта, которая стала еще более удобной, быстрой и качественной.<br/>
<br/>
<img src="http://habrastorage.org/storage2/7bf/ce5/2be/7bfce52be77b92dc09fe9ff4181b2607.png"/><br/>
<br/>
Несмотря на то, что у нас уже выходит пятая версия, мало кто знает, сколько нас… Поэтому мы решили стать более открытыми. Показать, что nanoCAD создает не один-два человека, а большая дружная команда, работающая по разным направлениям. «Для нас это не просто продукт — это наша философия!». И мы делимся этой философией с вами…<br/>
<iframe width="560" height="349" src="http://www.youtube.com/embed/ZteBum33vK4?wmode=opaque" frameborder="0" allowfullscreen></iframe><br/>
<br/>
 <a href="http://habrahabr.ru/post/185738/#habracut">Читать дальше &rarr;</a>', 'dows', '{"Я пиарюсь",сапр,nanocad,черчение,"альтернатива AutoCAD"}', '2013-07-05 22:11:26', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (237, 4, '95608226d67dbf7f214e142095945f5c', 'http://habrahabr.ru/post/185738/', 'Нанософт: мы говорим о nanoCAD', 'nanoCAD — российская универсальная САПР-платформа, содержащая мощные инструменты базового проектирования, выпуска чертежей и разработки приложений с помощью открытого API. И в июне 2013 года компания «Нанософт» объявила о выходе пятой версии своего основного продукта, которая стала еще более удобной, быстрой и качестве��ной.<br/>
<br/>
<img src="http://habrastorage.org/storage2/7bf/ce5/2be/7bfce52be77b92dc09fe9ff4181b2607.png"/><br/>
<br/>
Несмотря на то, что у нас уже выходит пятая версия, мало кто знает, сколько нас… Поэтому мы решили стать более открытыми. Показать, что nanoCAD создает не один-два человека, а большая дружная команда, работающая по разным направлениям. «Для нас это не просто продукт — это наша философия!». И мы делимся этой философией с вами…<br/>
<iframe width="560" height="349" src="http://www.youtube.com/embed/ZteBum33vK4?wmode=opaque" frameborder="0" allowfullscreen></iframe><br/>
<br/>
 <a href="http://habrahabr.ru/post/185738/#habracut">Читать дальше &rarr;</a>', 'dows', '{"Я пиарюсь",сапр,nanocad,черчение,"альтернатива AutoCAD"}', '2013-07-05 22:11:26', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (236, 4, 'a0438607be471bbbeac6440295ed9e6e', 'http://habrahabr.ru/post/181187/', 'Бытовой компьютер БК-0010-01. Дубль два', 'Наверняка многие хабражители (особенно те, которые подписаны на хаб <a href="http://habrahabr.ru/hub/antikvariat/">Старое железо</a>) помнят о существовании бытового компьютера советского производства с неброским названием БК-0010-01. Упоминание о нем уже было на Хабре в <a href="http://habrahabr.ru/post/116749/">этом обзоре</a>, написанным камрадом <a href="http://habrahabr.ru/users/ftp27/" class="user_link">ftp27</a>. Так как обзор закончился на самом интересном месте, я искренне ожидал увидеть продолжение. <br/>
<br/>
Это ведь логично: заставить компьютер включаться — это пол дела. А запустить на нем Popcorn и иже с ним — вот истинное наслаждение для любителя компьютерных древностей. Однако, с момента публикации первого обзора про БК-0010-01 прошло уже достаточно много времени, а на просторах Хабра не появилось ни одного более подробного материала об этом замечательном бытовом компьютере.<br/>
<br/>
<img src="http://habr.habrastorage.org/post_images/41a/ef3/538/41aef353871baf36b6cf726cc27dac5a.jpg" alt="image"/><br/>
<br/>
И вот совсем недавно в мои руки наконец-то попал компьютер Электроника БК-0010-01 в практически идеальном состоянии. Помимо самого компьютера, мне достались: блок МСТД, блок нагрузок, дисковод 5,25&quot; с КНГМД, джойстик, несколько блоков питания и монитор Электроника МС 6105 с внешним блоком питания. За мышью УВК Марсианка и принтером Электроника МС 6312 мне пришлось немного поохотиться, но как не странно данные устройства удалось достать за вполне приемлемые деньги.<br/>
<br/>
В связи с этим, я хотел бы рассказать о некоторой периферии для БК-0010-01, а также запустить на данном бытовом компьютере несколько культовых игр с фотоотчетом сего процесса.<br/>
<br/>
Тем, кто слабо помнит, что такое БК-0010-01 (или не помнит совсем), крайне рекомендуется ознакомиться с имеющимся на Хабре <a href="http://habrahabr.ru/post/116749/">обзором этого бытового компьютера</a>, и лишь потом нажимать кнопку «Читать дальше»<br/>
 <a href="http://habrahabr.ru/post/181187/#habracut">Читать дальше &rarr;</a>', 'kerenskiy', '{"Старое железо","История ИТ",БК,0010.01,"бытовой компьютер"}', '2013-07-05 20:45:56', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (319, 4, '2a93dbe7772d8b2c1c749a7bb002d286', 'http://habrahabr.ru/post/185804/', 'Проверьте на готовность ваш Gemfile к Rails 4', '<img align="left" src="http://habrastorage.org/storage2/ab9/24d/811/ab924d81135f321e5c49060563ca37f7.png"/>Rails 4 вышли две недели назад, а вы все еще сидите на третьих? Вас останавливает трудоемкий процесс проверки работоспособности всех подключенных гемов? Не беда! Буквально 10 дней назад стартовал веб-сервис по проверке совместимости гемов и Rails 4 от <a href="http://www.frodsan.com/">frodsan</a> и <a href="http://florentguilleux.fr/">Florent</a>. <br/>
 <a href="http://habrahabr.ru/post/185804/#habracut">Читать дальше &rarr;</a>', 'Timrael', '{"Ruby on Rails",Ruby,"ruby on rails","rails 4","rails 3",gem,gemfile}', '2013-07-07 10:07:10', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (321, 4, 'c2a502a3b380c979b6e3d56770a7d5bb', 'http://habrahabr.ru/post/185808/', 'Solar Impulse завершил перелет через континентальную часть США (5 тысяч километров)', '<img src="http://habrastorage.org/storage2/f43/3fa/e09/f433fae09c89fc9f735610d7fc737831.jpg"/><br/>
<br/>
Да, «солнечный» самолет <a href="http://www.solarimpulse.com/">Solar Impulse</a> снова отличился — на этот раз летательный аппарат, работающий исключительно на энергии, полученной от Солнца, завершил перелет, начатый еще 3 мая. Целью перелета было пересечение континентальной части США с восточного на западное побережье. Перелет совершили два пилота, Андре Боршберг и Бертран Пиккар. <br/>
<br/>
 <a href="http://habrahabr.ru/post/185808/#habracut">Читать дальше &rarr;</a>', 'marks', '{"Энергия и элементы питания","Будущее здесь","solar impulse"}', '2013-07-07 12:01:45', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (248, 4, '2ffb24ade25c59c6cb9127def3515c7d', 'http://habrahabr.ru/post/185768/', 'Oracle 12c Data Redaction. Сокрытие информации от непривилегированных пользователей', 'Задача разделения доступа к данным в информационных системах возникает всегда. Так или иначе ее нужно решать. Если доступ к базе данных возможен только из сервера приложений, то можно возложить эту обязаннасть на него. Но почти всегда есть потребность прямого доступа к данным, например для аналитиков или персонала поставщика системы. <br/>
В статье рассматривается возможность частичного сокрытия информации, доступ к которой строго ограничен. Тут же вспоминаем про <abbr title="О персональных данных">152-ФЗ</abbr>.<br/>
 <a href="http://habrahabr.ru/post/185768/#habracut">Читать дальше &rarr;</a>', 'xlix123', '{"Администрирование баз данных",Oracle,"Информационная безопасность",oracle,"information security",redacting}', '2013-07-06 18:49:32', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (402, 12, 'c7f3948be1e4506642a5c606b69492e7', 'http://lenta.ru/news/2013/07/12/aerosvit/', 'Бывшего гендиректора «Аэросвита» объявили в международный розыск', 'Украинские правоохранительные органы объявили в международный розыск бывшего гендиректора авиакомпании «Аэросвит». Топ-менеджер, имя которого не называется, проходит по уголовному делу, возбужденном по статьям о невыплате заработной платы, уклонении от уплаты страховых взносов и другим.', NULL, '{Экономика}', '2013-07-12 15:24:00', false, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (318, 4, '525ea7a8e00bd42f9e161aef84c965c3', 'http://habrahabr.ru/post/185212/', 'QtDbus — тьма, покрытая тайною. Часть 1', 'Наше путешествие началось Qt Graphics Framework, нас завербовали его светлой стороной, а потом мы долго получали граблями по разным частям тела.<br/>
<br/>
Данная статья — это спин-офф основного сюжета. В ней сказ пойдет о QtDBus. Этот модуль Qt появился еще в четвертой версии и был хоть как-то документирован и снабжен примерами. Но грянул Qt 5.0, и уж не знаю по чему, но это привело к тому, что на сторону тьмы перешла вышеназванная дока.. <a href="http://habrahabr.ru/post/185212/#habracut">Читать дальше &rarr;</a>', 'DancingOnWater', '{"Qt Software",C++,Linux,qt,qtdbus,dbus,"темные стороны документации"}', '2013-07-07 08:00:12', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (338, 4, 'f8760d80afacfb6e5a47e11b7df63ecb', 'http://habrahabr.ru/post/185792/', 'CATNIP – еще одна CAT система для переводчиков', 'Как-то давно мой знакомый <a href="http://habrahabr.ru/users/t_moor/" class="user_link">T_Moor</a> рекламировал в хабе «Я пиарюсь» мою систему для переводчиков MT2007 (<a href="http://habrahabr.ru/post/30639/">статья</a>). Данная статья — продолжение той ис��ории.<br/>
 <a href="http://habrahabr.ru/post/185792/#habracut">Читать дальше &rarr;</a>', 'Manson', '{"Я пиарюсь",переводчик,ПО,кошки}', '2013-07-07 00:44:45', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (239, 4, '3c8a6210bf4aaf593c5cfa55be7ef5b7', 'http://habrahabr.ru/post/185704/', 'Устройство файла UEFI BIOS, часть первая: UEFI Capsule и Intel Flash Image', 'Выпуск материнских плат на чипсетах Intel шестой серии (P67 и его братьях) принес на массовый рынок ПК новый вариант BIOS — <abbr title="Unified Extensible Firmware Interface">UEFI</abbr>. В этой статье мы поговорим об устройстве файлов UEFI Capsule и Intel Flash Image. <br/>
Структура EFI Firmware Volume и полезные в хозяйстве патчи будут описаны во второй части.<br/>
 <a href="http://habrahabr.ru/post/185704/#habracut">Читать первую часть</a>', 'CodeRush', '{"Системные платы","Системное программирование","Операционные системы",UEFI,Capsule,Intel,"Flash Image"}', '2013-07-06 00:33:31', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (403, 12, '8893b06f31915e8808d5f5e912467e65', 'http://lenta.ru/news/2013/07/12/setabase/', 'Пакистанские талибы решили оборудовать базу в Сирии', 'Представитель пакистанского отделения экстремистского движения «Талибан» сообщил о планах открыть базу в Сирии. По его словам, эта база будет служить «для оценки потребностей джихада» в Сирии и регионе. Планируется, что в работе над этим проектом примут участие бывшие афганские моджахеды из стран Ближнего Востока.', NULL, '{Мир}', '2013-07-12 15:53:02', false, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (244, 4, '42b1055233897760832aaf619f3c9c5a', 'http://habrahabr.ru/post/185754/', 'Разблокировка AES-NI на Lenovo U310', 'Пост практически продолжение темы UEFI.<br/>
<br/>
<h4>История покупки</h4><br/>
Началась вся история с того для работы был приобретен ультрабук Lenovo U310 (с Windows 8). Выбирался ультрабук по таким параметрам как:<br/>
<ul>
<li>Тонкий</li>
<li>Долго держит заряд</li>
<li>Не сильно дорогой</li>
<li><b>Наличие аппаратного AES</b></li>
</ul><br/>
<br/>
Шифрование само по себе было важно из-за постоянной работы с конфиденциальными данными + исходные коды рабочих программ.<br/>
По этому всё это занимало целый раздел на HDD, который был зашифрован через TrueCrypt. <br/>
<br/>
Так как объем данных был довольно большой, то программной реализации шифрования уже не хватало бы. Вернее хватало бы, но поиск по ним был бы довольно долгий + длительная компиляция.<br/>
<br/>
Именно по этому и хотелось взять ультрабук с поддержкой аппаратного шифрования AES. <br/>
Выбор пал на U310 с процессором Intel i5-3317u. Посмотрев описание процессора, точно удостоверился в том, что там присутствует аппаратный AES (реализованный через набор инструкций AES-NI). <br/>
<br/>
 <a href="http://habrahabr.ru/post/185754/#habracut">Читать дальше &rarr;</a>', 'shevmax', '{"Системные платы","Системное программирование",Железо,"Lenovo U310",aes-ni,"Аппаратное шифрование",UEFI}', '2013-07-06 14:20:30', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (243, 4, 'd9007c8c660835d48c1b3152a5a4a0a8', 'http://habrahabr.ru/post/185756/', '4К-видео Сатурна и его спутников', 'Автоматическая межпланетная станция «Кассини» стала искусственным спутником Сатурна 30 июня 2004 года, стартовав с Земли немногим менее чем за 7 лет до этого — 15 октября 1997 года. С самого начала проекта предполагалось, что аппарат совершит 74 витка вокруг планеты и 45 витков вокруг её спутника — Титана, однако НАСА уже несколько раз продлевало миссию и теперь конечным сроком, на который рассчитывает агенство, является 2017 год. Мало того, существуют оценки ресурса трёх двигателей аппарата, которые оценивают сроки их работы по меньшей мере ещё в 200 лет.<br/>
 <a href="http://habrahabr.ru/post/185756/#habracut">Узнать подробности</a>', 'jeston', '{Космонавтика,"Работа с видео",YouTube,видео,"сверхвысокое разрешение",4К,сатурн,кассини,миссия}', '2013-07-06 12:00:43', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (340, 4, '98e2e908dfbf56041dda4978d62c6f82', 'http://habrahabr.ru/post/185782/', 'VDS в Нидерландах Intel Xeon X3440 / 2-3 GB RAM / 220 GB HDD RAID1 / 25Mbps Unmetered — $29 / месяц', 'Приветствую!<br/>
<br/>
Не так давно <a href="http://habrahabr.ru/company/ua-hosting/blog/185692/">мы анонсировали распродажу выделенных серверов</a> в Нидерландах в ЦОД премиум-качества EvoSwitch:<br/>
<br/>
Intel Quad-Core Xeon X3440 / 8GB DDR3 / 2x1TB SATA2 / 100Mbps Unmetered — $89 / месяц<br/>
Заказать можно тут: <a href="http://ua-hosting.com.ua/nl-servers.html">http://ua-hosting.com.ua/nl-servers.html</a><br/>
<br/>
Сегодня мы решили сделать эти серверы еще более доступными. Теперь у Вас есть возможность арендовать четверть этого сервера, в виде виртуального сервера, по невероятно низкой цене:<br/>
<br/>
VDS Intel Xeon X3440 / 2-3 GB RAM / 220 GB HDD RAID1 / 25Mbps Unmetered — $29 / месяц<br/>
Заказать можно тут: <a href="http://ua-hosting.com.ua/vds.html">http://ua-hosting.com.ua/vds.html</a><br/>
<br/>
Сразу честно скажу, что помимо преимуществ у предложения есть недостаток: разграничить IOPS (операции чтения / зап��си) на VDS невозможно, потому этот ресурс рассчитан на честное использование. Тем не менее, на ноде у Вас будет всего лишь 3 соседа, а это означает, что зачастую Вы получите ресурсов больше, чем предусмотрено тарифным планом. Мы специально не ставили жестких ограничений на процессор и память, так как показала практика — в этом нет никакого смысла, лучше дать возможность каждому из пользователей завершить операцию, как можно скорее, тем самым освободив ресурсы и сократив число IOPS и зависших процессов.<br/>
<br/>
<img src="http://habrastorage.org/storage2/751/40a/0e8/75140a0e81f62deb456a93eee4013302.jpg"/><img src="http://habrastorage.org/storage2/48c/025/2ab/48c0252ab46d4bb10129ca536c1d296f.jpg"/> <a href="http://habrahabr.ru/post/185782/#habracut">Читать дальше &rarr;</a>', 'HostingManager', '{"Блог компании «ua-hosting.com.ua»","серверы в нидерландах",vps,vds}', '2013-07-06 22:50:59', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (246, 4, '00e4c1ab4c4889ffe582ac4064af36cd', 'http://habrahabr.ru/post/185766/', 'Вконтакте iOS SDK v2', 'Добрый вечер!<br/>
<br/>
Всё началось с того, что необходим был более или менее удобный инструмент для работы с API социальной сети ВКонтакте под iOS. Однако Google меня достаточно быстро расстроил результатами поиска:<br/>
<ul>
<li> <a href="https://github.com/StonerHawk/Vkontakte-iOS-SDK">StonewHawk — GitHub</a> </li>
<li> <a href="https://github.com/maiorov/VKAPI">maiorov/VKAPI — GitHub</a> </li>
<li> <a href="https://github.com/AndrewShmig/Vkontakte-iOS-SDK">AndrewShmig/Vkontakte-iOS-SDK</a> (да-да, с первой версией было не всё так хорошо, как могло показаться с первого взгляда) </li>
</ul><br/>
Вроде бы всё хорошо, самое главное есть, но вот использование не вызывает приятных ощущений.<br/>
<br/>
Под катом я расскажу, как работает обновленная версия ВКонтакте iOS SDK v2, с чего всё начиналось и к чему в итоге пришли.<br/>
 <a href="http://habrahabr.ru/post/185766/#habracut">Читать дальше &rarr;</a>', 'AndrewShmig', '{"Objective C","Разработка под iOS","Социальные сети и сообщества",ios,objective-c,"социальные сети","вконтакте api"}', '2013-07-06 15:36:00', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (253, 4, 'b3e49dd607f89d4d85ae553dff74281e', 'http://habrahabr.ru/post/181187/', 'Бытовой компьют��р БК-0010-01. Дубль два', 'Наверняка многие хабражители (особенно те, которые подписаны на хаб <a href="http://habrahabr.ru/hub/antikvariat/">Старое железо</a>) помнят о существовании бытового компьютера советского производства с неброским названием БК-0010-01. Упоминание о нем уже было на Хабре в <a href="http://habrahabr.ru/post/116749/">этом обзоре</a>, написанным камрадом <a href="http://habrahabr.ru/users/ftp27/" class="user_link">ftp27</a>. Так как обзор закончился на самом интересном месте, я искренне ожидал увидеть продолжение. <br/>
<br/>
Это ведь логично: заставить компьютер включаться — это пол дела. А запустить на нем Popcorn и иже с ним — вот истинное наслаждение для любителя компьютерных древностей. Однако, с момента публикации первого обзора про БК-0010-01 прошло уже достаточно много времени, а на просторах Хабра не появилось ни одного более подробного материала об этом замечательном бытовом компьютере.<br/>
<br/>
<img src="http://habr.habrastorage.org/post_images/41a/ef3/538/41aef353871baf36b6cf726cc27dac5a.jpg" alt="image"/><br/>
<br/>
И вот совсем недавно в мои руки наконец-то попал компьютер Электроника БК-0010-01 в практически идеальном состоянии. Помимо самого компьютера, мне достались: блок МСТД, блок нагрузок, дисковод 5,25&quot; с КНГМД, джойстик, несколько блоков питания и монитор Электроника МС 6105 с внешним блоком питания. За мышью УВК Марсианка и принтером Электроника МС 6312 мне пришлось немного поохотиться, но как не странно данные устройства удалось достать за вполне приемлемые деньги.<br/>
<br/>
В связи с этим, я хотел бы рассказать о некоторой периферии для БК-0010-01, а также запустить на данном бытовом компьютере несколько культовых игр с фотоотчетом сего процесса.<br/>
<br/>
Тем, кто слабо помнит, что такое БК-0010-01 (или не помнит совсем), крайне рекомендуется ознакомиться с имеющимся на Хабре <a href="http://habrahabr.ru/post/116749/">обзором этого бытового компьютера</a>, и лишь потом нажимать кнопку «Читать дальше»<br/>
 <a href="http://habrahabr.ru/post/181187/#habracut">Читать дальше &rarr;</a>', 'kerenskiy', '{"Старое железо","История ИТ",БК,0010.01,"бытовой компьютер"}', '2013-07-05 20:45:56', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (249, 4, '03c1526371dc5306c3baa1bfcfd88148', 'http://habrahabr.ru/post/185778/', 'Дайджест интересных материалов из мира веб-разработки и IT за последнюю неделю №64 (30 июня — 6 июля 2013)', 'Предлагаем вашему вниманию очередную подборку с ссылками на новости, интересные материалы и полезные ресурсы.<br/>
<br/>
<img src="http://habrastorage.org/storage2/35c/416/4cc/35c4164cc02246279e9a492c74c7f413.jpg"/><br/>
 <a href="http://habrahabr.ru/post/185778/#habracut">Читать дальше &rarr;</a>', 'alexzfort', '{"Блог компании Zfort Group",Веб-разработка,Веб-дизайн,дайджест,новости,css,js,html,интересное,браузеры,ресурсы,ссылки,"ссылки на сми",веб-дизайн,веб-разработка,jquery,css3,html5,"firefox os",Geeksphone,canvas,boilerplate}', '2013-07-06 20:28:50', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (233, 4, '14ea81297dbbabf1d951f46af1f40c10', 'http://habrahabr.ru/post/185724/', 'UNIGINE Open Air 2013: про разработку игр и 3D-технологии под открытым небом (Томск, 20-21 июля)', 'Совершенно внезапно в прошлом году мы решили провести опен-эйр про разработку игр в Томске. Мы, конечно, подозревали, что тема будет очень востребована, но результат превзошел наши ожидания: несмотря на поздний анонс, собралось <a href="http://2013.openair.unigine.com/past">почти 200 человек из самых разных городов</a>, даже был разработчик из киевского отделения Ubisoft.<br/>
<br/>
<img src="http://habrastorage.org/storage2/b94/a90/225/b94a90225be3c27d6df0e931a160f3a5.jpg"/><br/>
<br/>
В общем, в этом году мы решили повторить, да и вообще сделать мероприятие ежегодным. Послушать интересное и поговорить про технологии 3D-графики и разработку игр можно будет 20-21 июля на солнечной поляне под Томском: <a href="http://2013.openair.unigine.com/">2013.openair.unigine.com</a><br/>
<br/>
На UNIGINE Open Air соберутся разработчики игр и 3D-приложений из Томска, Новосибирска, Красноярска, Омска, Барнаула, Бийска и других сибирских городов. Кстати, уже знакомые читателям нашего блога авторы статьи про процедурную генерацию (<a href="/company/unigine/blog/167075/">часть 1</a>, <a href="/company/unigine/blog/184614/">часть 2</a>), технические художники UNIGINE, на мероприятии тоже будут делиться своим опытом.<br/>
 <a href="http://habrahabr.ru/post/185724/#habracut">Читать дальше &rarr;</a>', 'Unigine', '{"Блог компании UNIGINE","Game Development","Анимация и 3D графика",unigine,gamedev,"3d графика","open air",томск,новосибирск,омск,красноярск,барнаул,бийск}', '2013-07-05 18:50:21', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (252, 4, '83e56629caec3586542d6dc36fa37057', 'http://habrahabr.ru/post/185786/', 'Google платит разработчикам Adblock Plus за пропуск своей рекламы', '<div style="text-align:center;"><img src="http://habr.habrastorage.org/post_images/83e/f69/071/83ef69071e8093b518a4637d3fc13ac2.png" /></div><br/>
Google платит создателям Adblock Plus — самого популярного расширения для Chrome и Firefox — за то, чтобы пользователи, решившие блокировать рекламные баннеры в своём браузере, всё-таки видели рекламу от Google. Как <a href="http://www.theverge.com/2013/7/5/4496852/adblock-plus-eye-google-whitelist">пишет</a> The Verge с ссылкой на немецкий сайт Horizont, Google и другие неназванные компании платят за включение в «белый список», что предотвращает блокирование их рекламы.<br/>
 <a href="http://habrahabr.ru/post/185786/#habracut">Читать дальше &rarr;</a>', 'aleksandrit', '{"Google Chrome",Firefox,"Adblock Plus",Google}', '2013-07-06 23:17:09', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (259, 4, '637c810aa3f1603539d4562d25b3ddc0', 'http://habrahabr.ru/post/185784/', '[recovery mode] Сотрудники Яндекса указывают на некорректную работу сервиса odnaknopka.ru', '<img src="http://habrastorage.org/storage2/3a9/5ab/964/3a95ab964bccb68ef60aabe8acc1fe0c.png" align="left"/>Небольшая новость вдогонку к <a href="http://habrahabr.ru/post/182430/">посту о некорректном поведении скрипта AddThis</a>. Сотрудники техподдержки Yandex в ответах на многочисленные обращения владельцев «забаненных» ресурсов сообщают, что <b>скрипт от Однакнопка.ру редиректит посетителей, использующих мобильные устройства, на вредоносные цели</b>. Если используете на своих проектах скрипт от Однакнопка.ру проверьте корректность работы ваших сайтов, дабы не потерять посетителей и не попасть под фильтры поисковых систем и браузеров.<br/>
 <a href="http://habrahabr.ru/post/185784/#habracut">Подробности</a>', 'pragmat', '{Веб-разработка,"Информационная безопасность",редирект}', '2013-07-06 23:35:27', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (256, 4, '84cb90804d24d19394f9f3fa66c2d09f', 'http://habrahabr.ru/post/185772/', 'Технологиче��кие компании занимаются распознаванием сарказма', 'Французская компания Spotter разработала инструмент, который, по их словам, способен идентифицировать сарказм в комментариях в Интернете.<br/>
<br/>
<img src="http://habr.habrastorage.org/post_images/a05/e1b/23e/a05e1b23e4227845acf4c30db1247e2c.jpg" alt="image" align="left"/>Созданная программная платформа сканирует социальные медиа и другие интернет-источники для создания отчетов о репутации своих клиентов — среди которых есть Европейская комиссия, Air France и другие крупные заказчики. Как и большая часть подобного ПО, приложение занимается анализом семантики, лингвистики и эвристики. Однако, как и любая другая система с машинным анализом данных, их инструмент часто испытывает проблемы с такими тонкими частями человеческой речи, как сарказм и ирония — и, вроде бы, как раз эту проблему Spotter и удалось преодолеть — пусть их руководители и признают, что результат пока что далек от идеального, и что полностью доверять машине еще рано. Процент распознавания достигает 80%, и, по заявлению авторов, еще несколько лет назад даже подобный результат был немыслим — тогда сарказм опознавался в 50% случаев. Авторы говорят, что алгоритм работает с 29 языками (включая русский и китайский), а чаще всего им приходится иметь дело с распознаванием сообщений о плохом уровне обслуживания.<br/>
 <a href="http://habrahabr.ru/post/185772/#habracut">Читать дальше &rarr;</a>', 'HotWaterMusic', '{Алгоритмы,сарказм,распознавание}', '2013-07-06 18:34:45', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (235, 4, 'd5581a9ef2437830f616a250aeeba57c', 'http://habrahabr.ru/post/185710/', '[Из песочницы] RMI для нескольких сетевых интерфейсов', 'Здравствуй, Хабр!<br/>
В ходе работы появилась задача создать несколько RMI реестров, доступных через разные сетевые интерфейсы (локальная сеть и интернет). И к моему удивлению я ничего толком не нашел в сети по этому вопросу. Поэтому разобравшись сам, решил поделиться решением с людьми. <br/>
<br/>
<h4>Дано</h4>Сервер с двумя сетевыми интерфейсами: локальный и внешний IP-адреса. Интерфейс, используемый клиентом и реализуемый сервером:<br/>
<pre><code class="java">public interface Server extends Remote {
    public String getMessage() throws RemoteException;
}
</code></pre><br/>
<h4>Задача</h4>Создать два RMI реестра, каждый для своего сетевого интерфейса.<br/>
 <a href="http://habrahabr.ru/post/185710/#habracut">Решение</a>', 'stepanovD', '{"Сетевые технологии",JAVA,java,rmi}', '2013-07-05 20:32:24', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (322, 4, 'fb4817514e6fcb7b1d676ac5751e8fdb', 'http://habrahabr.ru/post/185810/', 'Свободный ноутбук Novena', '<img src="http://habrastorage.org/storage2/d37/3fd/dc8/d373fddc8e5af375af3facd4a2da2dd2.jpg"/><br/>
<sup><a href="http://bunniefoo.com/novena/novena_depop_clean_labels.jpg">hi-res</a></sup><br/>
<br/>
Известный хакер, автор книги <a href="http://habrahabr.ru/post/172247/">“Hacking Xbox”</a> Эндрю Хуанг (Andrew “bunnie” Huang) в июне 2012 года вместе с коллегами начал разработку свободного ноутбука Novena на процессоре <a href="http://www.freescale.com/webapp/sps/site/prod_summary.jsp?code=i.MX6Q&amp;webpageId=129226228141673454B24A&amp;nodeId=018rH3ZrDRB24A">Freescale iMX6</a>. Идея в том, чтобы у сообщества была открытая платформа, с открытой документацией и руководством по программированию, так что каждый может собрать из комплектующих себе ноутбук/маршрутизатор/etc. на свой вкус, с любой клавиатурой, корпусом и размером экрана.<br/>
 <a href="http://habrahabr.ru/post/185810/#habracut">Читать дальше &rarr;</a>', 'alizar', '{Ноутбуки,"DIY или Сделай Сам",Железо,"Freescale iMX6",Novena,"свободный ноутбук",FPGA,retina}', '2013-07-07 12:53:35', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (331, 4, '8ee165bdab0d37de7dd9edb75f0a672d', 'http://habrahabr.ru/post/185818/', 'Набор DIY: делаем беспилотник из любого предмета', '<img src="http://habrastorage.org/storage2/9be/410/50a/9be41050a10d8fd1b5be69ee0c455561.jpg"/><br/>
<br/>
Конструктор <a href="http://jaspervanloenen.com/diy/">Drone It Yourself</a> (<a href="https://webcache.googleusercontent.com/search?q=cache%3Ahttp%3A%2F%2Fjaspervanloenen.com%2Fdiy%2F&amp;ie=utf-8&amp;oe=utf-8&amp;aq=t">кэш</a>) представляет собой набор деталей для распечатки на 3D-принтере. С их помощью можно поднять в воздух практически любой предмет. Как говорит автор конструктора, осуществить «дронификацию» объекта. <br/>
 <a href="http://habrahabr.ru/post/185818/#habracut">Читать дальше &rarr;</a>', 'alizar', '{"DIY или Сделай Сам",Робототехника,"Drone It Yourself",конструкция,дронификация,квадрокоптер}', '2013-07-07 14:30:27', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (250, 4, '5585a41638e50173b64c444199d929ff', 'http://habrahabr.ru/post/185776/', 'Улучшенное распознование речи используя категории', 'На данный момент большой проблемой распознавания речи (и смысла текста) является сложность предугадать смысл, а точнее контекст в котором находится слово. Часть проблемы решается дополнительным анализом соседних слов и предложения, а в тексте также анализируются заголовки документов. Большая проблема состоит в сложности реализации алгоритмов, особенно если речь идет о мобильных приложениях которые имеют ограниченные ресурсы.<br/>
<br/>
Проблема может быть решена если автор приложения сам укажет контекст в котором произнесена фраза или написано сообщение для анализа.<br/>
 <a href="http://habrahabr.ru/post/185776/#habracut">Узнать больше</a>', 'xeos', '{"Google API","Google App Engine","Data Mining","распознавание речи","распознавание текста"}', '2013-07-06 20:37:50', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (293, 4, '80df4fceaf41f5262db8f8d7af4b340e', 'http://habrahabr.ru/post/185792/', 'CATNIP – еще одна CAT система для переводчиков', 'Как-то давно мой знакомый <a href="http://habrahabr.ru/users/t_moor/" class="user_link">T_Moor</a> рекламировал в хабе «Я пиарюсь» мою систему для переводчиков MT2007 (<a href="http://habrahabr.ru/post/30639/">статья</a>). Данная статья — продолжение той истории.<br/>
 <a href="http://habrahabr.ru/post/185792/#habracut">Читать дальше &rarr;</a>', 'Manson', '{"Я пиарюсь",переводчик,ПО,кошки}', '2013-07-07 00:44:45', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (245, 4, 'b461c5b76c341cb872c0ebe391ebcd4d', 'http://habrahabr.ru/post/185764/', 'Устройство файла UEFI BIOS, часть полуторная: UEFI Platform Initialization', 'В <a href="http://habrahabr.ru/post/185704/">первой части этой статьи</a> мы познакомились с форматом UEFI Capsule и Intel Flash Image. Осталось рассмотреть структуру и содержимое EFI Firmware Volume, но для понимания различий между модулями <abbr title="Pre-EFI Initialization">PEI</abbr> и драйверами <abbr title="Driver Execution Environment">DXE</abbr> начнем с процесса загрузки UEFI, а структуру EFI Firmware Volume отставим на вторую часть.<br/>
 <a href="http://habrahabr.ru/post/185764/#habracut">Читать полуторную часть</a>', 'CodeRush', '{"Системные платы","Системное программирование","Операционные системы",UEFI,"Platform Initialization"}', '2013-07-06 15:15:57', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (329, 4, '868b9ff56669e588d96f63303ac98fdf', 'http://habrahabr.ru/post/185812/', 'BitTorrent исполнилось 12 лет', 'Да, причем 12 лет <b>BitTorrent </b>исполнилось еще 2 июля. Можно сказать, что прошло уже 12 лет с того момента, как произошел скачок в развитии Сети, благодаря появлению этого протокола обмена данными типа peer-to-peer. и соответствующего приложения. 2 июля 2001 года на доске объявлений Yahoo появилось сообщение от Брэма Коэна. Сообщение было коротким: “<i>My new app, BitTorrent, is now in working order, check it out here</i>&quot;. Нельзя сказать, чтобы сразу посыпались радостные комментарии типа «ура, наконец-то Интернет изменится». <br/>
<br/>
 <a href="http://habrahabr.ru/post/185812/#habracut">Читать дальше &rarr;</a>', 'marks', '{"История ИТ",Peer-to-Peer,BitTorrent,peer-to-peer}', '2013-07-07 13:19:51', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (258, 4, 'f1fa0e0f4e3d3b911484077ae3110971', 'http://habrahabr.ru/post/185772/', 'Технологические компании занимаются распознаванием сарказма', 'Французская компания Spotter разработала инструмент, который, по их словам, способен идентифицировать сарказм в комментариях в Интернете.<br/>
<br/>
<img src="http://habr.habrastorage.org/post_images/a05/e1b/23e/a05e1b23e4227845acf4c30db1247e2c.jpg" alt="image" align="left"/>Созданная программная платформа сканирует социальные медиа и другие интернет-источники для создания отчетов о репутации своих клиентов — среди которых есть Европейская комиссия, Air France и другие крупные заказчики. Как и большая часть подобного ПО, приложение занимается анализом семантики, лингвистики и эвристики. Однако, как и любая другая система с машинным анализом данных, их инструмент часто испытывает проблемы с такими тонкими частями человеческой речи, как сарказм и ирония — и, вроде бы, как раз эту проблему Spotter и удалось преодолеть — пусть их руководители и признают, что результат пока что далек от идеального, и что полностью доверять машине еще рано. Процент распознавания достигает 80%, и, по заявлению авторов, еще несколько лет назад даже подобный результат был немыслим — тогда сарказм опознавался в 50% случаев. Авторы говорят, что алгоритм работает с 29 языками (включая русский и китайский), а чаще всего им приходится иметь дело с распознаванием сообщений о плохом уровне обслуживания.<br/>
 <a href="http://habrahabr.ru/post/185772/#habracut">Читать дальше &rarr;</a>', 'HotWaterMusic', '{Алгоритмы,сарказм,распознавание}', '2013-07-06 18:34:45', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (320, 4, 'df102bf765fbbded3e0640a54f1b7606', 'http://habrahabr.ru/post/185794/', 'Coursera для музыкантов: краткий обзор курсов', 'О системе обучения Coursera на хабре писали неоднократно. И даже приводили анонсы некоторых курсов. Я же решил отобрать те из них, которые могут быть интересны и полезны людям, работающим со звуком: музыкантам, композиторам, звукорежиссёрам, как опытным, так и только помышляющим сделать первые шаги. Предлагаемые курсы помогут:<br/>
<ul>
<li>ознакомиться с физическими основами звука и акустики;</li>
<li>получить базовое или расширить понимание теории музыки, психоакустики и т.п.;</li>
<li>познакомиться с цифровой обработкой звука, программными инструментами и механизмами обработки;</li>
<li>научиться писать свои простейшие программы для обработки звука;</li>
<li>наконец, научиться игре на гитаре, джазовой импровизации, управлению репетициями и другим интересным вещам.</li>
</ul><br/>
 <a href="http://habrahabr.ru/post/185794/#habracut">Читать дальше &rarr;</a>', 'Vasiliskov', '{"Работа со звуком","Учебный процесс в IT",музыка,"обучение онлайн","звук и музыка"}', '2013-07-07 11:18:33', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (291, 4, 'daeae54f98cb223673a67d5dba807114', 'http://habrahabr.ru/post/185750/', 'jQuery plugin для использования SVG графики. Часть 1', 'Для работы с SVG есть очень много библиотек для рисования. Мы рассмотрим плагин для jQuery.<br/>
 <a href="http://habrahabr.ru/post/185750/#habracut">Читать дальше &rarr;</a>', 'viklviv', '{jQuery,"Векторная графика",JavaScript,svg,"jquery plugins","векторная графика",javascript}', '2013-07-07 00:01:04', true, true);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (316, 4, '0fd998f7d922786d0afa9180897c2883', 'http://habrahabr.ru/post/185800/', 'Вышла бета‐версия Vim 7.4', 'Вчера, 6 июля 2013, Брам Мооленаар <a href="https://groups.google.com/forum/?fromgroups=#!topic/vim_dev/N8jzif4e9L8">объявил</a> о выходе первой бета‐версии Vim 7.4: одного из лучших текстовых редакторов мира *nix.<br/>
Наиболее значимым изменением в новой версии является новый движок регулярных выражений. Также была сильно улучшена поддержка Python.<br/>
 <a href="http://habrahabr.ru/post/185800/#habracut">Читать дальше &rarr;</a>', 'ZyXI', '{VIM,vim,announce}', '2013-07-07 04:55:41', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (234, 4, 'f2ce974423066ca5907f74f38553c1e9', 'http://habrahabr.ru/post/185708/', '[Из песочницы] Продукт Acronis Backup & Recovery 11 или что это было?', 'Доброе время суток, дорогие хабровчане!<br/>
<br/>
Я работаю в крупной муниципальной компании и мне бы очень хотелось несколько слов написать об используемых нами продуктах компании Acronis.<br/>
<br/>
Раньше, до 10ой версии я всегда был доволен скоростью и стабильностью работы продуктов по резервному копированию и созданию образов и считал продукты компании Acronis одними из лучший продуктов в этой области использования. Было приятно, что ПО просто работает и делает то, что должно делать. Время идёт, продукты развиваются и на windows 2008 сервер 9ую версию уже не поставишь. Приобретенный нами продукт Acronis Backup & Recovery 11 вместе с Universal Restore считаю сильно подпортил репутацию линейки Ваших продуктов! На протяжении всего времени его использования, с момента покупки мы постоянно «боремся» с ним пытаясь настроить его так, как оно должно работать.<br/>
 <a href="http://habrahabr.ru/post/185708/#habracut">Читать дальше &rarr;</a>', 'r10n', '{"Восстановление данных","Системное администрирование",Acronis,образы,разочарование}', '2013-07-05 20:11:59', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (404, 12, '9257af4473e8208fc95926afb116a674', 'http://lenta.ru/news/2013/07/12/putin/', 'Путин поручил в рамках учений спасти подводный флот', 'Президент Владимир Путин поручил в ночь на 13 июля начать внезапную проверку Восточного военного округа. В рамках этих учений глава государства потребовал отработать навыки по спасению подводного флота. Внезапные проверки проходят с весн��. В прошлый раз войска учились отражать космическое нападение.', NULL, '{Россия}', '2013-07-12 16:09:01', false, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (314, 4, '3dd1e1cad32decdf2817cbe0853d19b9', 'http://habrahabr.ru/post/185784/', '[recovery mode] Сотрудники Яндекса предупреждают о некорректной работе сервиса odnaknopka.ru', '<img src="http://habrastorage.org/storage2/3a9/5ab/964/3a95ab964bccb68ef60aabe8acc1fe0c.png" align="left"/>Небольшая новость вдогонку к <a href="http://habrahabr.ru/post/182430/">посту о некорректном поведении скрипта AddThis</a>. Сотрудники техподдержки Yandex в ответах на многочисленные обращения владельцев «забаненных» ресурсов сообщают, что <b>скрипт от Однакнопка.ру редиректит посетителей, использующих мобильные устройства, на вредоносные цели</b>. Если используете на своих проектах скрипт от Однакнопка.ру, проверьте корректность работы ваших сайтов, дабы не потерять посетителей и не попасть под фильтры поисковых систем и браузеров.<br/>
 <a href="http://habrahabr.ru/post/185784/#habracut">Подробности</a>', 'pragmat', '{Веб-разработка,"Информационная безопасность",редирект}', '2013-07-06 23:35:27', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (334, 4, '646f24fa8ddf787c0daa8d0c600e53a1', 'http://habrahabr.ru/post/185774/', 'Устройство файла UEFI BIOS, часть вторая: UEFI Firmware Volume и его содержимое', 'Позади уже полторы (<a href="http://habrahabr.ru/post/185704/">первая</a>, <a href="http://habrahabr.ru/post/185764/">полуторная</a>) части этой статьи, теперь наконец пришло время рассказать о структуре UEFI Firmware Volume и формате UEFI File System.<br/>
 <a href="http://habrahabr.ru/post/185774/#habracut">Читать вторую часть</a>', 'CodeRush', '{"Системные платы","Системное программировани��","Операционные системы",UEFI,"Firmware Volume",FFS}', '2013-07-07 16:04:32', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (323, 4, '6d81f417a6a8465ec08c924ba9c259e2', 'http://habrahabr.ru/post/185812/', 'BitTorrent исполнилось 12 лет', '<img src="http://habrastorage.org/storage2/fc9/205/cfc/fc9205cfc635405f14f3888f76d6e8ed.png"/><br/>
<br/>
Да, причем 12 лет <b>BitTorrent </b>исполнилось еще 2 июля. Можно сказать, что прошло уже 12 лет с того момента, как произошел скачок в развитии Сети, благодаря появлению этого протокола обмена данными типа peer-to-peer. и соответствующего приложения. 2 июля 2001 года на доске объявлений Yahoo появилось сообщение от Брэма Коэна. Сообщение было коротким: “<i>My new app, BitTorrent, is now in working order, check it out here</i>&quot;. Нельзя сказать, чтобы сразу посыпались радостные комментарии типа «ура, наконец-то Интернет изменится». <br/>
<br/>
 <a href="http://habrahabr.ru/post/185812/#habracut">Читать дальше &rarr;</a>', 'marks', '{"История ИТ",Peer-to-Peer,BitTorrent,peer-to-peer}', '2013-07-07 13:19:51', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (333, 4, 'e71a71d9b6a2f4aee3a7ca5c7b8c0414', 'http://habrahabr.ru/post/185750/', 'jQuery plugin для использования SVG графики. Часть 1', 'Для работы с SVG есть очень много библ��отек для рисования. Мы рассмотрим плагин для jQuery.<br/>
 <a href="http://habrahabr.ru/post/185750/#habracut">Читать дальше &rarr;</a>', 'viklviv', '{jQuery,"Векторная графика",JavaScript,svg,"jquery plugins","векторная графика",javascript}', '2013-07-07 00:01:04', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (315, 4, '31ffa3dc425b5da26b00eddf556c2850', 'http://habrahabr.ru/post/185770/', 'QLiveBittorrent — консольный bittorrent клиент для просмотра файлов до скачивания', 'Ровно месяц назад была опубликована статья <a href="http://habrahabr.ru/post/181906/">LiveDC — Быстрый доступ к p2p файлам</a>. Смысл ее в том, что <a href="http://habrahabr.ru/users/erty_hackward/" class="user_link">Erty_Hackward</a> написал DC-клиент с возможностью просмотра файлов до их окончательной загрузки. С его помощью можно, например, смотреть фильмы спустя пару минут после начала закачки, перематывать их, смотреть с любого момента. А можно извлечь нужный файл из большого архива, не перекачивая целый архив.<br/>
<br/>
Мне очень понравилась идея этой програ��мы. Но она написана на C#. А хотелось бы использовать ее в линуксе. Поэтому с разрешения автора я с большим удовольствием взялся за создание аналогичной программы для линукса. В результате получился консольный битторрент клиент QLiveBittorrent.<br/>
<img src="http://habrastorage.org/storage2/adc/e13/f76/adce13f760b0a4f6c759a0302f77fa51.png"/><br/>
<br/>
 <a href="http://habrahabr.ru/post/185770/#habracut">Читать дальше &rarr;</a>', 'vtyulb', '{"Qt Software",Софт,Linux,bittorrent,p2p,linux,qt,консоль}', '2013-07-06 23:42:44', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (242, 4, '94cb34e4302f1014ff47765ab48437d2', 'http://habrahabr.ru/post/185752/', 'Бирюльки и Гуглосервис', '<blockquote>Программировать — не в бирюльки играть.<br/>
И. В. Сталин</blockquote><br/>
<img src="http://habrastorage.org/storage2/dd9/819/7ae/dd98197ae3fd53532a909df99adffae1.jpg" alt="image"/><br/>
Я хочу напомнить о древней русской игре Бирюльки, об игрушке для iPhone и об Adwhirl, закрывающемся сервисе Google, с которым надо что-то срочно делать. <br/>
 <a href="http://habrahabr.ru/post/185752/#habracut">Читать дальше &rarr;</a>', 'PapaBubaDiop', '{"Блог компании Papa Buba Diop","Mobile Development","Разработка под iOS",admob,adwhirl,mediation,honey,mag,игры}', '2013-07-06 11:41:39', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (292, 4, '030e8ce3210846ccd30970f98b52eb4d', 'http://habrahabr.ru/post/185790/', 'Еще одна Success story о замене мыши Logitech', 'Приветствую хабражителей.<br/>
Хочу с Вами поделиться своей историей успешной замены неисправной мыши Logitech G700 Gaming Mouse.<br/>
<br/>
После года успешного использования мыши, ее левая кнопка начала срабатывать вместо одиночного клика — дважды, вместо двойного — трижды.<br/>
Повез мышь в сервисный центр, указанный на гарантийном талоне. Было это 31 марта. 27 апреля меня оповестили, что моя проблема решена, но теперь не работает скролл. Решил дождаться, пока починят. 1 июня она ко мне вернулась, с таки неисправным скроллом.<br/>
<br/>
 <a href="http://habrahabr.ru/post/185790/#habracut">Читать дальше &rarr;</a>', 'argnist', '{Железо,"Гаджеты. Устройства для гиков",Сервис,техподдержка,logitech,g700,g700s}', '2013-07-07 00:29:41', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (251, 4, 'beb65aa241c83fcf0fce910454045030', 'http://habrahabr.ru/post/185782/', 'VDS в Нидерландах Intel Xeon X3440 / 2-3 GB RAM / 220 GB HDD RAID1 / 25Mbps Unmetered — $29 / месяц', 'Приветствую!<br/>
<br/>
Не так давно <a href="http://habrahabr.ru/company/ua-hosting/blog/185692/">мы анонсировали распродажу выделенных серверов</a> в Нидерландах в ЦОД премиум-качества EvoSwitch:<br/>
<br/>
Intel Quad-Core Xeon X3440 / 8GB DDR3 / 2x1TB SATA2 / 100Mbps Unmetered — $89 / месяц<br/>
Заказать можно тут: <a href="http://ua-hosting.com.ua/nl-servers.html">http://ua-hosting.com.ua/nl-servers.html</a><br/>
<br/>
Сегодня мы решили сделать эти серверы еще более доступными. Теперь у Вас есть возможность арендовать четверть этого сервера, в виде виртуального сервера, по невероятно низкой цене:<br/>
<br/>
VDS Intel Xeon X3440 / 2-3 GB RAM / 220 GB HDD RAID1 / 25Mbps Unmetered — $29 / месяц<br/>
Заказать можно тут: <a href="http://ua-hosting.com.ua/vds.html">http://ua-hosting.com.ua/vds.html</a><br/>
<br/>
Сразу честно скажу, что помимо преимуществ у предложения есть недостаток: разграничить IOPS (операции чтения / записи) на VDS невозможно, потому этот ресурс рассчитан на честное использование. Тем не менее, на ноде у Вас будет всего лишь 3 сосе��а, а это означает, что зачастую Вы получите ресурсов больше, чем предусмотрено тарифным планом. Мы специально не ставили жестких ограничений на процессор и память, так как показала практика — в этом нет никакого смысла, лучше дать возможность каждому из пользователей завершить операцию, как можно скорее, тем самым освободив ресурсы и сократив число IOPS и зависших процессов.<br/>
<br/>
<img src="http://habrastorage.org/storage2/751/40a/0e8/75140a0e81f62deb456a93eee4013302.jpg"/><img src="http://habrastorage.org/storage2/48c/025/2ab/48c0252ab46d4bb10129ca536c1d296f.jpg"/> <a href="http://habrahabr.ru/post/185782/#habracut">Читать дальше &rarr;</a>', 'HostingManager', '{"Блог компании «ua-hosting.com.ua»","серверы в нидерландах",vps,vds}', '2013-07-06 22:50:59', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (330, 4, '9ca4682d6a9f1cddac96da9da3a04223', 'http://habrahabr.ru/post/185814/', 'RWpod. 18 выпуск 01 сезона. Rails 4, EuRuKo 2013, Jasper, JavaScript Cookbook и прочее', 'Добрый день уважаемые слушатели. Представляем новый выпуск подкаста RWpod. В этом выпуске:<br/>
<br/>
<img src="http://habr.habrastorage.org/post_images/742/799/bf8/742799bf8327d7f8d5e6d581ac0af521.png" alt="image"/><br/>
<br/>
 <a href="http://habrahabr.ru/post/185814/#habracut">Читать дальше &rarr;</a>', 'rwpod', '{JavaScript,Ruby,Веб-разработка,rwpod,javascript,ruby,web-разработка}', '2013-07-07 13:48:55', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (260, 4, 'e38637498ce0e5d22b0fc108bf73d4fb', 'http://habrahabr.ru/post/185770/', 'QLiveBittorrent — консольный bittorrent клиент для просмотра файлов до скачивания', 'Ровно месяц назад была опубликована статья <a href="http://habrahabr.ru/post/181906/">LiveDC — Быстрый доступ к p2p файлам</a>. Смысл ее в том, что <a href="http://habrahabr.ru/users/erty_hackward/" class="user_link">Erty_Hackward</a> написал DC-клиент с возможностью просмотра файлов до их окончательной загрузки. С его помощью можно, например, смотреть фильмы спустя пару минут после начала закачки, перематывать их, смотреть с любого момента. А можно извлечь нужный файл из большого архива, не перекачивая целый архив.<br/>
<br/>
Мне очень понравилась идея этой программы. Но она написана на C#. А хотелось бы использовать ее в линуксе. Поэтому с разрешения автора я с большим удовольствием взялся за создание аналогичной программы для линукса. В результате получился консольный битторрент клиент QLiveBittorrent.<br/>
<img src="http://habrastorage.org/storage2/adc/e13/f76/adce13f760b0a4f6c759a0302f77fa51.png"/><br/>
<br/>
 <a href="http://habrahabr.ru/post/185770/#habracut">Читать дальше &rarr;</a>', 'vtyulb', '{"Qt Software",Софт,Linux,bittorrent,p2p,linux,qt,консоль}', '2013-07-06 23:42:44', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (313, 4, 'db4593698fd0deaf16ea7c00e52f6394', 'http://habrahabr.ru/post/185766/', 'Вконтакте iOS SDK v2', 'Добрый вечер!<br/>
<br/>
Всё началось с того, что необходим был более или менее удобный инструмент для работы с API социальной сети ВКонтакте под iOS. Однако Google меня достаточно быстро расстроил результатами поиска:<br/>
<ul>
<li> <a href="https://github.com/StonerHawk/Vkontakte-iOS-SDK">StonewHawk — GitHub</a> </li>
<li> <a href="https://github.com/maiorov/VKAPI">maiorov/VKAPI — GitHub</a> </li>
<li> <a href="https://github.com/AndrewShmig/Vkontakte-iOS-SDK">AndrewShmig/Vkontakte-iOS-SDK</a> (да-да, с первой версией было не ��сё так хорошо, как могло показаться с первого взгляда) </li>
</ul><br/>
Вроде бы всё хорошо, самое главное есть, но вот использование не вызывает приятных ощущений.<br/>
<br/>
Под катом я расскажу, как работает обновленная версия ВКонтакте iOS SDK v2, с чего всё начиналось и к чему в итоге пришли.<br/>
 <a href="http://habrahabr.ru/post/185766/#habracut">Читать дальше &rarr;</a>', 'AndrewShmig', '{"Objective C","Разработка под iOS","Социальные сети и сообщества",ios,objective-c,"социальные сети","вконтакте api"}', '2013-07-06 15:36:00', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (328, 4, '74bd9fada7b225bde225f6aef5f79e01', 'http://habrahabr.ru/post/185804/', 'Проверьте на готовность ваш Gemfile к Rails 4', '<img align="left" src="http://habrastorage.org/storage2/ab9/24d/811/ab924d81135f321e5c49060563ca37f7.png"/>Rails 4 вышли две недели назад, а вы все еще сидите на третьих? Вас останавливает трудоемкий процесс проверки работоспособности вс��х подключенных гемов? Не беда! Буквально 10 дней назад стартовал веб-сервис по проверке совместимости гемов и Rails 4 от <a href="http://www.frodsan.com/">frodsan</a> и <a href="http://florentguilleux.fr/">Florent</a>. <br/>
 <a href="http://habrahabr.ru/post/185804/#habracut">Читать дальше &rarr;</a>', 'Timrael', '{"Ruby on Rails",Ruby,"ruby on rails","rails 4","rails 3",gem,gemfile}', '2013-07-07 10:07:10', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (317, 4, '28c9ac0786d39d6928d58a7f792fe226', 'http://habrahabr.ru/post/185426/', '[Из песочницы] Размышления на тему пользовательского рейтинга', 'Во многих случаях для оценки чего-либо в интернете мы привыкли нажимать «лайк» или «дислайк». Самым очевидным примером являются социальные сети. Но иногда разделения на нравится и не нравится попросту недостаточно для более точной системы оценок.<br/>
<br/>
Возьмем, к примеру, задачу оценки пользователями нового ресторана, недавно появившегося в городе. Такая задача обычно решается «школьной» шкалой из 5 баллов. Это могут быть звёздочки или всё чаще появляющиеся смайлы с различными эмоциями. Работает просто, быстро и удобно. Но, как обычно, не обходится без «но». <a href="http://habrahabr.ru/post/185426/#habracut">Читать дальше &rarr;</a>', 'maccuro', '{Песочница,интерфейсы,рейтинг,лайк}', '2013-07-07 07:08:48', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (240, 4, 'cb495ad4394f7d085ad200ae5f9e5fac', 'http://habrahabr.ru/post/185744/', '«Вконтакте» начал договариваться с правообладателями', '<img src="http://habrastorage.org/storage2/121/fbf/f5f/121fbff5faad8fc01fdb15579833b6b1.jpg"/><br/>
<br/>
Социальная сеть «Вконтакте» начала процесс переговоров с медиакомпаниями, планируя вернуть контент, удаленный ранее по требованию звукозаписывающих компаний, обратно на ресурс. По данным некоторых СМИ, сейчас представители социальной сети действительно ведут активные переговоры с правообладателями о возможности легализации контента. В частности, социальная сеть ведет переговоры с такими крупными лейблами, как Sony Music, Warner Music и Universal Music.<br/>
<br/>
 <a href="http://habrahabr.ru/post/185744/#habracut">Читать дальше &rarr;</a>', 'marks', '{"Dura Lex","Социальные сети и сообщества",Копирайт,вконтакте,музыка,правообладатели}', '2013-07-06 02:14:45', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (241, 4, '46c4c06c98b99319e70c9f2994204854', 'http://habrahabr.ru/post/185746/', 'Тяжелый FPV-квадрокоптер — продолжение: APM 2.5 и активный подвес для камеры', '<img src="http://habrastorage.org/storage2/a56/085/693/a56085693b7d1e1d2ed216b5018f8be8.jpg"/><br/>
<br/>
<i><a href="http://habrahabr.ru/post/178509/">Часть первая</a> — в которой я придумал и построил свой квадрокоптер.</i><br/>
<br/>
Продолжаю рассказ о своем квадрокоптере. Со времени опубликования первой статьи я налетал немало часов, и проделал множество модификаций на коптере. Обо всех мелких деталях рассказывать — будет долго, да и малоинтересно. Поэтому ограничусь основными изменениями, а именно:<ul>
<li>Новый полетный контроллер — простенькая платка Crius MultiWii SE уступила место новому ArduPilot Mega 2.5, резко улучшив летные характеристики и добавив коптеру некое кол-во продвинутых фич.</li>
<li>Активный подвес для камеры — GoPro теперь не жестко закреплена, а установлена на стабилизирующем подвесе, который удерживает ее в горизонтальном положении при любом положении коптера.</li>
<li>Новые моторы — в связи с выросшим весом коптера (а также с внезапной смертью одного из старых моторов) были установлены новые, болеe мощные и значительно более качественные моторы.</li>
<li>Отдельная FPV-камера — GoPro теперь используется толькo для записи, для полета через видеоочки используется отдельная жестко монтированная камера.</li>
</ul><br/>
Но обо всем по порядку…<br/>
 <a href="http://habrahabr.ru/post/185746/#habracut">Под катом многo текста и видео с подвеса</a>', 'leviathan', '{"DIY или Сделай Сам",квадрокоптер,квадроторы,quadcopter,fpv,gopro,"ardupilot mega",apm,arducopter,"brushless gimbal"}', '2013-07-06 07:41:28', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (247, 4, 'b2f3319c15d839244147c0d8a1ffe71e', 'http://habrahabr.ru/post/185772/', 'Технологичес��ие компании занимаются распознаванием сарказма', 'Французская компания Spotter разработала инструмент, который, по их словам, способен идентифицировать сарказм в комментариях в Интернете.<br/>
<br/>
<img src="http://habr.habrastorage.org/post_images/a05/e1b/23e/a05e1b23e4227845acf4c30db1247e2c.jpg" alt="image" align="left"/>Созданная программная платформа сканирует социальные медиа и другие интернет-источники для создания отчетов о репутации своих клиентов — среди которых есть Европейская комиссия, Air France и другие крупные заказчики. Как и большая часть подобного ПО, приложение занимается анализом семантики, лингвистики и эвристики. Однако, как и любая другая система с машинным анализом данных, их инструмент часто испытывает проблемы с такими тонкими частями человеческой речи, как сарказм и ирония — и, вроде бы, как раз эту проблему Spotter и удалось преодолеть — пусть их руководители и признают, что результат пока что далек от идеального, и что полностью доверять машине еще рано. Процент распознавания достигает 80%, и, по заявлению авторов, еще несколько лет назад даже подобный результат был немыслим — тогда сарказм опознавался в 50% случаев. Авторы говорят, что алгоритм работает с 29 языками (включая русский и китайский), а чаще всего им приходится иметь дело с распознаванием сообщений о плохом уровне обслуживания.<br/>
 <a href="http://habrahabr.ru/post/185772/#habracut">Читать дальше &rarr;</a>', 'HotWaterMusic', '{Алгоритмы,сарказм,распознавание}', '2013-07-06 18:34:45', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (255, 4, '69d8ed37ba7a1c3c91bce60c199cebe2', 'http://habrahabr.ru/post/185756/', '4К-видео Сатурна и его спутников', 'Автоматическая межпланетная станция «Кассини» стала искусственным спутником Сатурна 30 июня 2004 года, стартовав с Земли немн��гим менее чем за 7 лет до этого — 15 октября 1997 года. С самого начала проекта предполагалось, что аппарат совершит 74 витка вокруг планеты и 45 витков вокруг её спутника — Титана, однако НАСА уже несколько раз продлевало миссию и теперь конечным сроком, на который рассчитывает агенство, является 2017 год. Мало того, существуют оценки ресурса трёх двигателей аппарата, которые оценивают сроки их работы по меньшей мере ещё в 200 лет.<br/>
 <a href="http://habrahabr.ru/post/185756/#habracut">Узнать подробности</a>', 'jeston', '{Космонавтика,"Работа с видео",YouTube,видео,"сверхвысокое разрешение",4К,сатурн,кассини,миссия}', '2013-07-06 12:00:43', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (238, 4, '6266249de21b6570f10808e85d5597be', 'http://habrahabr.ru/post/185740/', 'Для начала внедрим Continuous Integration в мозг', 'Тот факт, что Continuous Integration нужен, уже никто не отрицает. Вроде бы всем понятно, что собирать приложение, прогонять тесты на регулярной основе очень полезно. Получить быстрый фидбек, найти проблему, прогнать на чистой машине — это все клево. Это понятно и менеджерам проектов, и девелоперам, даже заказчики радуются, когда есть возможность что-нибудь попробовать поскорее.<br/>
<br/>
Менеджер, как человек, который не должен лезть в технические детали, при виде прошедшего Continuous Integration билда, может однозначно сказать, хороший он или плохой. Зеленый — хороший, красный — плохой. Очень простой индикатор, да и не только для менеджеров, но и для всей команды в целом. Девелоперы на эту ситуацию смотрят немного иначе. <br/>
 <a href="http://habrahabr.ru/post/185740/#habracut">Читать дальше &rarr;</a>', 'alex4Zero', '{Agile,"continuous integration","разработка через тестирование",tdd}', '2013-07-05 22:14:47', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (342, 4, '1edff3c856ab2d29ffa7b2bc28939e76', 'http://habrahabr.ru/post/186442/', 'MNP: ничего не готово', 'У меня давно были подозрения на тему того, что внедрение <a href="http://ru.wikipedia.org/wiki/%D0%9F%D0%B5%D1%80%D0%B5%D0%BD%D0%BE%D1%81%D0%B8%D0%BC%D0%BE%D1%81%D1%82%D1%8C_%D1%82%D0%B5%D0%BB%D0%B5%D1%84%D0%BE%D0%BD%D0%BD%D1%8B%D1%85_%D0%BD%D0%BE%D0%BC%D0%B5%D1%80%D0%BE%D0%B2">MNP</a> в России идет по <a href="http://www.pcweek.ru/themes/detail.php?ID=72985">сценарию ЕГАИС</a>, однако я не являюсь ни сотрудником сотового оператора, ни министерства, ни тем более <a href="http://www.cctld.ru">КЦНДСИ</a>, поэтому подтверждения до вчерашнего дня у меня не было. <br/>
<br/>
Выглядит ситуация действительно весьма подозрительно — уже с декабря все должно работать в продакшене у всех сотовых операторов, а нигде даже не упоминается о хотя бы черновиках API для доступа к базе мобильных номеров или хотя бы об условиях доступа к ней для кого-то не из большой тройки.<br/>
<br/>
Однако, как показало вчерашнее открытое совещание в правительстве — большая тройка тоже не знает как это будет работать. И не знает потому, что этого не знают и в самом министерстве, которое проводит «реформу» и готовило поправки в закон «О связи».<br/>
 <a href="http://habrahabr.ru/post/186442/#habracut">Читать дальше &rarr;</a>', 'fruit', '{Телефония,Телекомы,mnp,минкомсвязи,"большая тройка"}', '2013-07-12 09:37:40', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (405, 12, 'c73c07419f71ca6da84f4b85c66f8cdc', 'http://lenta.ru/news/2013/07/12/train/', 'Под Парижем сошел с рельсов пассажирский поезд', 'На железнодорожной станции в парижском пригороде Бретиньи-сюр-Орж вечером 12 июля сошел с рельсов пассажирский поезд, следовавший в город Лимож. По данным Lе Monde, уже найдены восемь тел погибших. Количество раненых пока подсчету не поддается, точные цифры будут установлены позднее.', NULL, '{Мир}', '2013-07-12 16:14:00', false, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (406, 12, 'f093bb253530e0b493d3cf4f3f161624', 'http://lenta.ru/news/2013/07/12/heathrow/', 'В «Хитроу» загорелся эфиопский Dreamliner', 'В лондонском аэропорту «Хитроу» произошел пожар на борту самолета. В результате руководство приостановило взлет и посадку всех рейсов, закрыв обе взлетно-посадочные полосы аэропорта. Инцидент произошел с Boeing 787 Dreamliner, который принадлежит «Эфиопским авиалиниям». Пассажиров на его борту во время пожара не было.', NULL, '{Мир}', '2013-07-12 16:22:00', false, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (407, 12, 'c2d79aed04b1812cf9048fd27c2e2551', 'http://lenta.ru/news/2013/07/12/pm/', 'В Белоруссии предложили взимать налоги с тунеядцев', 'В Белоруссии может быть введен налог с неработающих граждан. Об этом заявил глава правительства республики Михаил Мясникович на совещании с руководящим активом Могилевской области. В Белоруссии, как отмечается, не работают около 445 тысяч человек из числа трудоспособного населения.', NULL, '{"Бывший СССР"}', '2013-07-12 16:38:00', false, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (408, 12, '6b46be473667c70ce3c9d7170765c5ed', 'http://lenta.ru/news/2013/07/12/renew/', '«Хитроу» открылся после пожара на борту Dreamliner', 'Лондонский аэропорт «Хитроу» возобновил работу после ликвидации пожара на борту Boeing 787 Dreamliner  авиакомпании «Эфиопские авиалинии». Полеты возобновились через полтора часа после закрытия аэропорта. Во время пожара самолет стоял на стоянке, и на борту его не было людей.', NULL, '{Мир}', '2013-07-12 17:36:00', false, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (409, 12, 'f162d98bfa4361b74404b5eb328a85a5', 'http://lenta.ru/news/2013/07/12/vinogradov/', 'Назначен временный мэр Ярославля', 'Врио мэра Ярославля назначен  Олег Виноградов, заместитель Урлашова. Мэр Ярославля Евгений Урлашов был задержан 3 июля. Его обвиняют в покушении на получение взятки в 45 миллионов рублей и получении взятки в 500 тысяч рублей. Урлашов, представляющий «Гражданскую поатформу», считает, дело политически мотивированным.', NULL, '{Россия}', '2013-07-12 18:15:00', false, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (414, 12, 'd2bef27fea7d26b34385f9c9fd355cd3', 'http://lenta.ru/news/2013/07/13/deny/', 'В мэрии Ярославля не подтвердили назначения временного мэра', 'В мэрии Ярославля не подтвердили информацию о назначении Олега Виноградова временно исполняющим обязанности мэра города. Заместитель главы города Александр Нечаев, который исполнял обязанности мэра после ареста Урлашова, также опроверг информацию о назначении Винградова.', NULL, '{Россия}', '2013-07-12 22:50:00', false, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (359, 4, '867b565c40ae12b1db633f01bc160805', 'http://habrahabr.ru/post/186506/', 'Наблюдения и Наука', 'Несколько последних постов позволили сформулироваться точке зрен��я, но чтобы она не выглядела «взятой с потолка», нужно сделать небольшой экскурс в историю. Пройтись во самым вехам, не сильно углубляясь в детали.<br/>
<br/>
Итак, начнём с древнегреческой науки  <a href="http://habrahabr.ru/post/186506/#habracut">Читать дальше &rarr;</a>', 'qehgt', '{Научно-популярное,наука,исследование,гравитация}', '2013-07-13 01:44:45', true, true);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (358, 4, '4658a923a70fc001258ade6889f2d753', 'http://habrahabr.ru/post/186504/', 'Итоги online конференции ruHaxe 2', '<img src="http://habr.habrastorage.org/post_images/99c/66f/038/99c66f038a422f1bd2f4e6ee368e5aea.jpg" alt="image"/><br/>
<br/>
6 июля 2013 года в 13:00 по Москве(10:00 по Гринвичу) состоялась вторая online конференция ruHaxe.<br/>
<a href="http://habrahabr.ru/post/183904/">(отчёт о первой конференции)</a><br/>
Доклады были настолько интересны, что оторваться от трансляций было невозможно, и если на первой темы были посвящены больше программированию, то в этот раз были затронуты довольно специфичные вопросы. <br/>
<br/>
 <a href="http://habrahabr.ru/post/186504/#habracut">Что же там было?</a>', 'k0t0vich', '{"Action Script",Android,haxe,ruHaxe,cross-platform,events,community,as3}', '2013-07-12 21:45:22', true, true);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (362, 4, '2abf6e068a4e09cf81c1e85c3f2d91d2', 'http://habrahabr.ru/post/186440/', '[Перевод] Прекратите восхвалять свой тяжелый труд', '<div style="text-align:center;"><img src="http://habr.habrastorage.org/post_images/938/27d/f71/93827df712e1f016166a0bf4d4b0bda1.jpg" alt="image" /></div><br clear="all"/>
<i>Перевод недавнего популярного поста с <a href="https://news.ycombinator.com/item?id=6019572">Hacker News</a>. </i><br/>
<br/>
Вы покидаете свой офис не раньше, чем глубоко за полночь.<br/>
<br/>
Пишете посты в Твиттер про работу и чекинитесь в офисе по субботам и воскресеньям — отчаянно пытаясь показать, что несмотря на наступившие выходные, вы продолжаете изо всех сил трудиться (в отличие от остальных).<br/>
<br/>
#ВКАЛЫВАЕМ #HUSTLE #РАБОТА<br/>
<br/>
Остановитесь, пожалуйста.<br/>
 <a href="http://habrahabr.ru/post/186440/#habracut">Читать дальше &rarr;</a>', 'HotWaterMusic', '{"Управление проектами","тяжёлые будни"}', '2013-07-12 09:29:07', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (426, 12, 'b4e3c51da60960700b4b85a1e1d21319', 'http://lenta.ru/news/2013/07/13/helicopter/', 'В Канаде создали первый достойный приза Сикорского педальный вертолет', 'Американское вертолетное сообщество впервые за 33 года вручило приз Игоря Сикорского за создание вертолета на мускульной тяге. Приз Сикорского в размере 250 тысяч долларов достался  проекту канадской компании AeroVelo, близкой к университету Торонто. Созданный ими вертолет Atlas поднялся на высоту трех метров.', NULL, '{"Наука и техника"}', '2013-07-13 08:31:00', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (348, 4, '36835458c603228004ac6270f95c6d3f', 'http://habrahabr.ru/post/186458/', 'Структура конфигов на сайтах Алавар', 'Всем привет!<br/>
Сайты Alawar — это сайты для <a href="http://www.alawar.ru">русского</a>, <a href="http://www.alawar.com">американского</a>, европейских и других рынков, отдельные сайты для mobile-устройств, сайты партнерских программ и др. Все они развернуты на одном инстансе Yii, <a href="http://habrahabr.ru/company/alawar/blog/157877/">о чем мы уже писали в нашем блоге на хабре</a>.<br/>
Сегодня я расскажу, как мы организовали хранение, структуру и управление конфигами наших сайтов, какие при этом получили преимущества. А также поведаю, как осуществляется деплой нашего проекта в различных окружениях.<br/>
<br/>
 <a href="http://habrahabr.ru/post/186458/#habracut">Читать дальше &rarr;</a>', 'nasedkin', '{"Блог компании «Alawar Entertainment»",Yii,PHP,php,yii,phing}', '2013-07-12 12:15:28', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (425, 12, 'b78453d4ab7a1d26b55291851384eb98', 'http://lenta.ru/news/2013/07/13/kiev/', 'Глава киевской милиции простил участников штурма райотдела МВД', 'В отношении участников протеста, которые в ночь на субботу штурмовали Святошинский райотдел милиции в Киеве, уголовные дела заведены не будут. Об этом заявил начальник киевского ГУ МВД Валерий Коряк. Он также сообщил, что начальник райотдела отстранен на время расследования дела о причинении телесных повреждений его подчиненным.', NULL, '{"Бывший СССР"}', '2013-07-13 07:48:00', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (354, 4, '470ec2feca2b230056541aaa721e770b', 'http://habrahabr.ru/post/186488/', 'MikroTik + port knocking over ICMP', 'Совсем маленький пост рассказывающий, как отловить ICMP пакеты и отфильтровать их с логикой.<br/>
<br/>
Реализуем технология port knocking на RouterOS через протокол icmp.<br/>
<br/>
Прошу под кат.<br/>
 <a href="http://habrahabr.ru/post/186488/#habracut">Читать дальше &rarr;</a>', 'vasilevkirill', '{"Сетевое оборудование",mikrotik,portknoking,icmp}', '2013-07-12 16:18:22', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (429, 12, '78c94744a7610a047240b3bfb8a40de8', 'http://lenta.ru/news/2013/07/13/switch/', 'Причиной крушения поезда под Парижем назвали неисправность стрелки', 'По предварительным данным, причиной крушения поезда под Парижем в пятницу вечером стала неисправность механизма стрелки. Об этом в субботу собщил французский железнодорожный оператор SNCF. По словам представителя компании, в ближайшее время будет проведена проверка 5000 подобных стрелок по всей стране.', NULL, '{Мир}', '2013-07-13 10:04:00', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (344, 4, 'd3128ac84a26a020cfd9d4049c3f59b7', 'http://habrahabr.ru/post/186354/', 'Экономическая эфективность и целесобразность введения VDI', 'Всем привет! Мы продолжаем наш цикл статей про виртуализацию и различные технологии hp, и на этот раз рассмотрим самый животрепещущий вопрос, по которому получили больше всего вопросов через различные каналы: на каком этапе целесообразно вводить виртуализацию рабочих мест, за какой период времени она начнёт экономить ваши средства и как можно сэкономить на внедрении VDI. <br/>
<br/>
<img alt="Image #1860681, 31.5 KB" src="http://habr.habrastorage.org/post_images/b20/b2d/7db/b20b2d7db6b2f5a65cf8e8664e02c957.jpg" title="Image #1860681, 31.5 KB"/><br/>
 <a href="http://habrahabr.ru/post/186354/#habracut">Приступим!</a>', 'Shirixae', '{Виртуализация,"Финансы для всех","Блог компании HP",виртуализация,virtualization,vdi,hp,экономика,окупаемость,внедрение,"системные интеграторы","рабочие места","всё равно никто не читает теги"}', '2013-07-12 10:42:00', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (345, 4, '96516e5c24b006719cc1924e8f54401e', 'http://habrahabr.ru/post/186452/', 'Чем бы вы стали заниматься, если…?', 'Друзья.<br/>
<br/>
Я — программист с довольно большим стажем. Мое увлечение компьютерами пошло еще со школы, когда в 1998 году у меня появился мой первый Пентиум. С тех пор я твердо решил, что буду писать программы, чем и занимаюсь по сей день.<br/>
<br/>
Однако недавно мне в голову засел странный вопрос, который, пожалуй, мог приходить многим из вас. Я задавал его почти всем своим друзьям программистам и некоторых он довольно сильно заставлял задуматься на длительное время. Сегодня пятница, день, когда можно немного расслабиться и подумать о вечном, поэтому я хочу задать этот же вопрос и вам.<br/>
<br/>
Итак: <b>Чем бы вы стали заниматься, если бы вам сегодня сказали, что вы больше никогда не можете заниматься тем, чем занимаетесь сейчас?</b> Изначально этот вопрос был про программирование, но поскольку аудитория хабра гораздо шире, я решил обобщить.<br/>
<br/>
Немного поясню условия. Допустим, вы — программист. И вот, почему-то, вам больше нельзя этим заниматься. Абсолютно не важно, почему. Это может быть злой рок, неизвестный вирус, несчастный случай или религиозные убеждения. Просто нельзя и все. Нигде и никогда. Никак. Что бы вы такого начали делать? При этом выбор деятельности не ограничивается. Вы можете, если хотите, остаться в сфере IT (стать, например, тестировщиком). Или же пойти продавать пирожки в ларек. Или танцевать (при условии, что вы это умеете или будете учиться). Но ответ обязательно должен быть с учетом ваших реальных возможностей, т.е. если у вас нет слуха, то нельзя ответить «буду оперным певцом». И да, это должно приносить вам денег.<br/>
<br/>
 <a href="http://habrahabr.ru/post/186452/#habracut">Читать дальше &rarr;</a>', 'glamcoder', '{"Human Resources",Программирование,"Статистика в IT",программирование,хобби,альтернатива,мысли}', '2013-07-12 11:11:32', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (360, 4, 'ee77aab2e825d75327e4b773dce1c5e0', 'http://habrahabr.ru/post/186512/', 'Конец эпохи, меланхолия', 'Доброе утро! Нашел свой старый текст написанный в порыве меланхолии. В нормальные хабы не рискую его сувать :)<br/>
<br/>
 <a href="http://habrahabr.ru/post/186512/#habracut">Читаем</a>', 'winox', '{Песочница,меланхолия}', '2013-07-13 03:23:04', true, true);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (361, 4, '9d54163353e62dd7106d4de40c9ba369', 'http://habrahabr.ru/post/186494/', 'Dune 2 The Building of A Dynasty', '<img src="http://habrastorage.org/storage2/061/f16/b97/061f16b97ed309ca5803f8cced32a3b6.png" align="left"/><br/>
<b>Android</b><br/>
<a href="https://play.google.com/store/apps/details?id=com.gamesinjs.dune2">Google Play</a><br/>
+Yandex Store<br/>
<br/>
Powered by<br/>
<a href="http://www.opendune.org/">OpenDUNE</a><br/>
<a href="http://www.libsdl.org/">SDL</a><br/>
<br/>
1992 © Westwood Studios<br/>
<br/>
 <a href="http://habrahabr.ru/post/186494/#habracut">Обсуждение</a>', 'Caiiiycuk', '{"Я пиарюсь","Шпайш машт флоу"}', '2013-07-13 03:42:56', true, true);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (347, 4, '7b866fb51ca57fbbc638dc62a67497b3', 'http://habrahabr.ru/post/186462/', 'Пользователь Reddit создал «вирусный» анимированный ролик про тотальную слежку АНБ по мотивам заставки Pixar', 'Пользователь Reddit под ником <a href="http://www.reddit.com/user/Sqorck">Sqorck</a> создал и выложил на Youtube очень качественно сделанный ролик, в юмористическом ключе описывающий ситуацию с тотальной слежкой правительства США за своими гражданами.<br/>
<br/>
Как отметили в <a href="http://www.reddit.com/r/funny/comments/1i4wij/my_pixar_style_nsa_animationidea_from_joystick354/">комментариях на Reddit</a>, ролик может оказаться наиболее эффективным способом донесения информации о тотальной слежке до широких масс населения.<br/>
<br/>
Кстати, авторам <a href="https://www.roi.ru/poll/petition/gosudarstvennoe_upravlenie1/otmenit-zakon-o-proizvolnyh-blokirovkah-internet-resursov-ot-02072013-187-fz-zakon-protiv-interneta/">петиции на РОИ об отмене пресловутого № 187-ФЗ</a> ИМХО также стоит взять пример с автора ролика.<br/>
<br/>
<iframe width="560" height="349" src="http://www.youtube.com/embed/55D-ybnYQSs?wmode=opaque" frameborder="0" allowfullscreen></iframe><br/>
<br/>
<b>EDIT</b>: перенёс топик из хаба «Информационная безопасность» в хаб «Анимация и 3D графика». Всех с пятницей! <a href="http://habrahabr.ru/post/186462/#habracut"></a>', 'menraen', '{"Dura Lex","Анимация и 3D графика",YouTube,NSA,reddit,мультипликация,"вирусный ролик",pixar}', '2013-07-12 11:42:26', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (349, 4, '9ca41b6c81bbe57b654dc71c1ac17322', 'http://habrahabr.ru/post/186476/', 'Новая летняя версия «Простого бизнеса» 1.8.1.2. «Форт-Нокс»: определение номера, связь таблиц, ускорение работы с документами, делами', 'В самом разгаре лета, когда работать хочется все меньше, а отдыхать – все больше, вышла новая версия <a href="http://www.prostoy.ru/">«Простого бизнеса»</a>. Команда разработчиков подготовила более 100 изменений, которые помогут Вам работать быстрее и продуктивнее, а, значит, у Вас будет больше времени для летнего отдыха. Теперь Вы сможете приветствовать клиентов по имени, настраивать клиентскую базу «под себя», связывать таблицы, создавать задачи под каждого клиента прямо из CRM, быстрее редактировать документы и видеть, какие дела сейчас в приоритете!<br/>
Подробнее о главных нововведениях.<br/>
 <a href="http://habrahabr.ru/post/186476/#habracut">Читать дальше &rarr;</a>', 'prostoy_ru', '{CRM-системы,"Блог компании Простой Бизнес",prostoy,"простой бизнес",crm}', '2013-07-12 13:28:50', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (350, 4, 'a613aac2ef0ab8702a4ea4888c7ab399', 'http://habrahabr.ru/post/186436/', 'Изучаем Storm Framework. Часть II', 'В <a href="http://habrahabr.ru/post/186208/">первой части</a> рассматривались базовые понятия Storm.<br/>
<br/>
Разные классы задач предъявляют различные требования к надежности. Одно дело пропустить пару записей при подсчете статистики посещений, где счет идет на сотни тысяч и особая точность не нужна. И совсем другое — потерять, например, информацию о платеже клиента.<br/>
<br/>
Далее рассмотрим о механизмы защиты от потери данных, которые реализованы в Storm. <br/>
 <a href="http://habrahabr.ru/post/186436/#habracut">Читать дальше &rarr;</a>', 'xlix123', '{"Параллельное программирование",JAVA,java,"storm framework",bigdata}', '2013-07-12 13:45:23', true, true);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (351, 4, '2c08ee93586961f7b1451635a6f3c4f3', 'http://habrahabr.ru/post/186482/', 'Честный glow и скорость', '<img src="http://habrastorage.org/storage2/53d/554/6cc/53d5546cc2b2d80ae5983f4ea8b9693a.png" align="left"/> Наверное все, кто хоть чуть-чуть работал с фотошопом — видели эффект outer glow для слоя, и пробовали с ним играться. В фотошопе есть 2 техники этого самого outer glow. Soft и precise. Soft мне был не так интересен, а вот глядя на precise — я задумался.<br/>
<br/>
Выглядит он вот так:<br/>
<img src="http://habrastorage.org/storage2/e6b/1d6/7ad/e6b1d67ad24fe26c0aa63e1c3e8fce81.png"/><br/>
Это однопиксельная линия. А градиент грубо говоря — отражает расстояние до ближайшего пикселя изображения. Это самое расстояние — могло бы быть очень вкусным для построения разнообразных эффектов. Это и всякие контуры, и собственные градиенты, и <div class="spoiler"><b class="spoiler_title">даже газоразрядные эффекты вокруг и прочее.</b><div class="spoiler_text"><a href="http://screenup.org/tmp/EffectUsage.zip">Пример эффекта</a>, который можно получить, если иметь в наличии карту расстояний. Пример использует OpenGL + GLSL, написан на Delphi</div></div><br/>
Основная проблема такого glow — это сложность вычисления для больших размеров. Если у нас glow на 100 пикселей, то нам надо для каждого пикселя изображения проверить 100*100 соседних пикселей. И для изображения например 800*600 это будет всего 4 800 000 000 проверок.<br/>
<br/>
Однако фотошоп этим не страдает, и прекрасно строит точный glow даже больших (до 250) размеров. Значит решение есть. И мне любопытно было его найти. Нагуглить быстрый алгоритм такого glow у меня не получилось. Большинство алгоритмов использует blur чтобы построить glow, но мы то с вами знаем, что однопиксельная линия не даст нам такого эффекта, как на картинке, она просто сблюрится.<br/>
<br/>
Поэтому я погнал велосипедить.<br/>
 <a href="http://habrahabr.ru/post/186482/#habracut">Велосипедить с автором</a>', 'MrShoor', '{"Обработка изображений",Алгоритмы,"Анимация и 3D графика",графика,"обработка изображений",glow}', '2013-07-12 13:50:30', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (352, 4, '3a6ddcd6540ad062501b89b77f33273e', 'http://habrahabr.ru/post/186456/', 'Универсальный iRig для iPhone и Андроид своими руками или экономим 30 евро', 'Счастливые обладатели айфонов, вероятнее всего слышали про такую вещь как iRig, которая вместе с софтом от IK Multimedia (те самые авторы AmpliTube — софтверного гитарного комбика с педалями для PC) позволяют использовать их яблочную технику в качестве гитарного процессора. Однако цены на официальный iRig несколько завышены, мягко говоря. Плюс для меня огромным минусом данной вещи было отсутствие поддержки устройств на Android (о причинах несовместимости читайте далее).<br/>
<br/>
<img src="http://habrastorage.org/storage2/b0e/62b/86d/b0e62b86d2d08b55c0b7b8ae1e88f959.jpg"/><br/>
<br/>
И так, если вы хотите сделать свой iRig, чтобы он поддерживал ещё и андроид, плюс потратить минимум денег, то добро пожаловать под кат.<br/>
 <a href="http://habrahabr.ru/post/186456/#habracut">Читать дальше &rarr;</a>', 'ErgoZru', '{"DIY или Сделай Сам",iphone,irig,android,diy,lowcost}', '2013-07-12 13:55:06', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (353, 4, '5980ab86ec682443c0ff447b43fd63fb', 'http://habrahabr.ru/post/186490/', 'Роскомнадзор планирует блокировать ресурсы с информацией о заблокированных ресурсах', 'Как бы абсурдно это не звучало, но все обстоит именно так. Роскомнадзор, Роспотребнадзор и ФСКН разработали законопроект, который будет предусматривать общие критерии и основания для блокировки сайтов с т.н. «запрещённым контентом». Помимо всего прочего, данным законопроектом предусматриваются санкции для ресурсов, распространяющих ссылки на уже заблокированные ресурсы, а также в целом «популяризирующие запрещённую информацию».<br/>
<br/>
<img src="http://habrastorage.org/storage2/ca4/adf/43d/ca4adf43da9460fe3136949f85d676d3.png" alt="image"/> <a href="http://habrahabr.ru/post/186490/#habracut">Читать дальше &rarr;</a>', 'ksenobayt', '{"Dura Lex",роскомнадзор,блокировки,роскомсвобода,идиотизм,"пора валить",цензура}', '2013-07-12 16:07:58', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (355, 4, '9d54ab20700e618abdeae247c34b6350', 'http://habrahabr.ru/post/186496/', 'Человеческий парсер на Selenium WD', '<iframe width="560" height="349" src="http://www.youtube.com/embed/oTeEpKMtwQM?wmode=opaque" frameborder="0" allowfullscreen></iframe><br/>
<br/>
<h4>Начало</h4><br/>
И вот пришла моя очередь покупать автомобиль. Как это делают ребята с работы я видел. Заходят на сайт и следят за предложениями, ну кто постарше покупает газету и просматривает объявления. Все это однообразно и отвлекаться на сиденье, исследование и нажатие по ссылкам не хотелось. Хотелось просто что бы кто то делал это за меня, таких людей не нашлось. Значит надо было заставить делать все это компьютер.  <a href="http://habrahabr.ru/post/186496/#habracut">Читать дальше &rarr;</a>', 'thrtuk', '{Node.JS,GreaseMonkey,Веб-разработка,node.js,seleium,парсер,лень}', '2013-07-12 16:45:00', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (356, 4, '9d40a9a830ba25387be18a860bc8c0fc', 'http://habrahabr.ru/post/186448/', 'Игра Backspace с путешествиями во времени от Obsidian', '<img src="http://habrastorage.org/storage2/24a/049/4f3/24a0494f317207c6ab5c4a279c75f11e.jpg"/><br/>
Представьте себе игру с огромным разнообразием оружия, путешествиями во времени, вторжением пришельцев, и всё на движке Skyrim. <br/>
<br/>
Это — <b>Backspace</b>, проект, который разрабатывался в Obsidian. Малочисленная команда разрабатывала дизайн и концепт игры в 2011 году. И хотя дальше концепта игра не ушла, но она бы заинтересовала многих. Obsidian многие должны знать по Fallout: New Vegas и недавнему Project Eternity (про NWN 2 думаю и напоминать не стоит).<br/>
<br/>
Одиночная ролевая игра, действия которой разворачиваются в эпоху космической эры с боевой системой похожей на Skyrim, но несколько динамичней. Что-то вроде смеси Mass Effect+Borderland+System Shock 2. С учётом движка, можно сказать, что это Sci-Fi Skyrim. <br/>
 <a href="http://habrahabr.ru/post/186448/#habracut">Читать дальше &rarr;</a>', 'Suvitruf', '{"Game Development","game development","obsidian entertainment",Obsidian,sci-fi,пятница}', '2013-07-12 18:21:48', true, true);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (357, 4, '16e17461814a2727f0d32eb23b312e36', 'http://habrahabr.ru/post/186486/', '[Из песочницы] Простейшая реализации кросс-платформенного мультиплеера на примере эволюции одной .NET игры', 'Всем привет!<br/>
Сразу попрошу извинения за стилистику моей писанины и незнание русского языка – к сожалению я ни разу не блогописатель, но желание оставлять комменты на хабре — сильный мотиватор. Кроме того, что я не писатель, я еще и не программист, а инженер-строитель, который иногда балуется написанием пары строк кода – такое вот хобби среди прочих. Но про строительство я вдоволь написался во времена подготовки и защиты диссера, и больше к этому возвращаться желания нет.<br/>
А в этой статье я хочу описать личный опыт создания одной игрушки с реализацией многопользовательской игры, позволяющей поддерживать различные платформы (пишу я на .NET, поэтому в первую очередь мой опыт касается Windows/WP, но этот же метод прекрасно подойдет для других платформ, у меня есть рабочие прототипы описываемой игры для Android/iOS, но пока нет желания платить по 1к юсд за коммерческие пакеты Xamarin). И да — в этой вводной статье (которая изначально планировалась исключительно в песочницу) я не буду раскрывать технических деталей, так как в этом случае я рискую еще долго ее не закончить. Кто знает, возможно у меня еще появится время и возможность написать более подробное продолжение.<br/>
 <a href="http://habrahabr.ru/post/186486/#habracut">Читать дальше &rarr;</a>', 'antonby', '{"Разработка под Windows Phone","Game Development",Песочница,"Windows Phone","Windows 8",websockets,игра,кости}', '2013-07-12 18:42:36', true, true);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (398, 4, '7f563f4b3e362149ef0fa254c107f168', 'http://habrahabr.ru/post/186516/', 'Skymouse: мышь с жестовым управлением', '<img src="http://habrastorage.org/storage2/6be/dfd/441/6bedfd4412575dd8c0514abc2f888ad2.jpg"/><br/>
<br/>
Сейчас новые технологии внедряются буквально за считанные месяцы, даже, если такая технология еще вчера казалась фантастикой. Тот же Kinect — сложнейшее устройство, которое было создано для геймеров (не только геймеров, конечно, но все же Kinect — выходец из игровой индустрии). Привычное сочетается с необычным, и одним из продуктов такого сочетания является мышь с жестовым управлением Skymouse. Вероятно, если бы в мире фильма «Особое мнение» была бы мышь, она выглядела бы именно так.<br/>
<br/>
 <a href="http://habrahabr.ru/post/186516/#habracut">Читать дальше &rarr;</a>', 'marks', '{Железо,"Гаджеты. Устройства для гиков",Skymouse,мышь,kickstarter}', '2013-07-13 09:38:19', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (431, 4, '26ebb37b06559d48d24f0ae296ccf22c', 'http://habrahabr.ru/post/186518/', 'Как хорошо вы знаете стандартную библиотеку?', 'Вопрос возник, потому что сам очень плохо помню стандартные АПИ языков, на которых часто пишу (Python, Java, JS). Это снижает производительность и настораживает: доктор, а я один такой?<br/>
<br/>
В опросе верстальщики могут считать «стандартной библиотекой» атрибуты HTML-тегов и CSS-свойства. <a href="http://habrahabr.ru/post/186518/#habracut"></a>', 'leventov', '{Разработка,Программирование,"человеческая память",память,"стандартная библиотека",мозг}', '2013-07-13 11:07:08', false, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (432, 12, '66bc21877dea6a4e700ea072742304d8', 'http://lenta.ru/news/2013/07/12/putin/', 'Путин поручил в рамках учений спасти подводный флот', 'Президент Владимир Путин поручил в ночь на 13 июля начать внезапную проверку Восточного военного округа. В рамках этих учений глава государства потребовал отработать навыки по спасению подводного флота. Внезапные проверки проходят с весны. В прошлый раз войска учились отражать космическое нападение.', NULL, '{Россия}', '2013-07-12 16:09:01', false, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (410, 12, '191b4c173e7fe8c875e41a227d0036fe', 'http://lenta.ru/news/2013/07/12/propaganda/', 'США обвинили РФ в предоставлении Сноудену «платформы для пропаганды»', 'США обвинили Россию в предоставлении Эдварду Сноудену, которого Вашингтон обвиняет в распространении секретной информации, трибуны для пропагандистских высказываний. По мнению Белого дома, Москва тем самым нарушает нейтралитет. Ранее Сноуден встретился с правозащитниками и парламентариями в аэропорту Шереметьево.', NULL, '{Мир}', '2013-07-12 19:15:00', false, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (411, 12, '7b6c3285dacca317560ec9acef219b66', 'http://lenta.ru/news/2013/07/13/kiiv/', 'В Киеве протестующие применили против милиции слезоточивый газ', 'В Киеве участники акции протеста возле управления милиции применили против милиционеров слезоточивый газ. В результате пострадали шесть сотрудников милиции. Они были госпитализированы с  ушибами и отравлениями. Причиной беспорядков стал конфликт между милицией и участницей Коалиции участников оранжевой революции.', NULL, '{"Бывший СССР"}', '2013-07-12 20:03:00', false, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (412, 12, '51fd97f5274ecec2d44203ce11ce17cf', 'http://lenta.ru/news/2013/07/13/arrest/', 'Финдиректора завода Дерипаски арестовали', 'Суд в Подгорице 12 июля санкционировал арест на 30 суток финдиректора черногорского алюминиевого завода Kombinat Aluminijuma Podgorica (KAP) Дмитрия Потрубача. Его подозревают в хищении электричества на сумму 9,64 миллиона евро в период с 22 февраля по 25 мая 2013 года.', NULL, '{Экономика}', '2013-07-12 20:45:00', false, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (413, 12, '82dfa030d3b0948c1b809118e51fa8d1', 'http://lenta.ru/news/2013/07/13/sudden/', 'Шойгу начал масштабную проверку боеготовности войск', 'В российской армии в ночь на 13 июля началась внезапная проверка боеготовности войск, в которой участвуют более 80 тысяч военнослужащих Центрального и Восточного военных округов, Тихоокеанского флота и  Дальней и Военно-транспортной авиации России. Это самая масштабная проверка за последние 20 лет.', NULL, '{Россия}', '2013-07-12 21:27:00', false, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (433, 12, 'fbface903659dceb9b28e2b7542d846d', 'http://lenta.ru/news/2013/07/13/deny/', 'В мэрии Ярославля не подтвердили назначения временного мэра', 'В мэрии Ярославля не подтвердили информацию о назначении Олега Виноградова временно исполняющим обязанно��ти мэра города. Заместитель главы города Александр Нечаев, который исполнял обязанности мэра после ареста Урлашова, также опроверг информацию о назначении Винградова.', NULL, '{Россия}', '2013-07-12 22:50:00', false, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (415, 12, '6de377e888b7068cfc6ef08ee76b645c', 'http://lenta.ru/news/2013/07/13/third/', 'Число погибших при крушении самолета в Сан-Франциско возросло', 'Число погибших при аварии лайнера Boeing 777 в Сан-Франциско возросло до трех человек. Утром 12 июля умер ребенок, находившийся в критическом состоянии. Девочка была гражданкой Китая. Ранее сообщалось о двух погибших. Boeing 777 авиакомпании Asiana Airlines потерпел крушение 6 июля.', NULL, '{Мир}', '2013-07-12 23:19:00', false, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (416, 12, 'e47af149fc1b071b0349d3f679d69e5c', 'http://lenta.ru/news/2013/07/13/medals/', 'Россияне завоевали 16 золотых медалей на седьмой день Универсиады', 'На шестой день XXVII Всемирной летней Универсиады в Казани студенческая сборная России заработала 16 золотых медалей. Сборная России, таким образом, установила новый рекорд по числу завоеванных золотых медалей, получив 88 высших наград. В общей сложности у России 169 медалей.', NULL, '{Спорт}', '2013-07-12 23:53:00', false, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (417, 12, '6e11b65aa8520b15332d9180ec2ad1b6', 'http://lenta.ru/news/2013/07/12/haidiev/', 'Напавшего с вилами на полицейского чеченца арестовали', 'В Пугачеве арестовали до 11 сентября чеченца, набросившегося с вилами на полицейского. Али Хайдаев был задержан утром 12 июля. При задержании он набросился на полицейского с вилами. В ответ сотрудник полиции ранил его из пистолета в обе ноги. На Хайдаева завели дело о нападении на полицейского.', NULL, '{Россия}', '2013-07-13 00:38:00', false, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (418, 12, '605a51318a5f026564e767ae0ed30fcc', 'http://lenta.ru/news/2013/07/13/romania/', 'Министра транспорта Румынии приговорили к пяти годам заключения', 'Министра транспорта Румынии Релу Фенекью приговорили к 5 годам заключения. По данным следствия, с 2002 по 2005 год Фенекью через компании, принадлежащие его братьям, закупил и перепродал старые трансформаторы под видом новых. Ущерб от действий министра составил 7 миллионов леев (около 2,5 миллиона долларов).', NULL, '{Мир}', '2013-07-13 01:30:00', false, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (419, 12, '9f7aee54451746761749e02431a05963', 'http://lenta.ru/news/2013/07/13/attack/', 'В результате взрыва в иракском кафе погибли 38 человек', 'В результате взрыва в кафе в городе Киркук на севере Ирака 12 июля погибли 38 человек. 29 человек получили ранения. Взрыв устроил террорист-смертник, который зашел в кафе со словами «Аллах акбар». Ответственность за атаку пока никто на себя не взял. По данным ООН, за июнь в Ираке были убиты не менее 760 человек.«»', NULL, '{Мир}', '2013-07-13 02:01:00', false, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (420, 12, 'e80b2a0abcb16b138c6c7fea1ba16c12', 'http://lenta.ru/news/2013/07/13/han/', 'Футбольный клуб «Фулхэм» купил пакистанский миллиардер', 'Миллиардер пакистанского происхождения Шахид Хан купил лондонский футбольный клуб «Фулхэм». Покупка 100 процентов акций клуба обошлась Хану в 200 миллионов фунтов стерлингов. Хан купил «Фулхэм» у 84-летнего египетского бизнесмена Мохаммеда Аль-Файеда, который владел клубом на протяжении 16 лет.', NULL, '{Спорт}', '2013-07-13 02:36:00', false, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (421, 12, '1118e0fc2d8c34ce714f02df58889aee', 'http://lenta.ru/news/2013/07/13/yakhont/', 'США обвинили Израиль в уничтожении склада российских ракет в Сирии', 'США обвинили Израиль в уничтожении склада российских противокорабельных ракет «Яхонт» в Сирии. По данным США, атака на склад произошла в Латакии 5 июля. В ходе удара погибли несколько военнослужащих. Министр обороны Израиля опроверг эти сообщения, заявив, что Иерусалим не вмешивается в сирийский конфликт.', NULL, '{Мир}', '2013-07-13 03:46:00', false, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (427, 12, 'f0cfc0320faf4f0881e40ec2d8dbfdb6', 'http://lenta.ru/news/2013/07/13/guard/', 'В Ингушетии убит личный охранник Доку Умарова', 'В ходе спецоперации, проведенной в субботу утром в Ингушетии в станице Нестеровская, ликвидированы два участника бандподполья. Они опознаны как находившийся с 2011 года в федеральном розыске Микаил Мусиханов и Ибрагим Циздоев. Первый являлся личным охранником Доку Умарова.', NULL, '{Россия}', '2013-07-13 08:51:00', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (428, 12, '7d1bbe61a3b1b8c7864b293b29cedcbd', 'http://lenta.ru/news/2013/07/13/lavrov/', 'Лавров порекомендовал Сноудену обратиться в ФМС', 'Министр иностранных дел России Сергей Лавров порекомендовал бывшему сотруднику ЦРУ Эдварду Сноудену обратиться в ФМС в случае, если он намерен получить политическое убежище в России. В ФМС заявили, что не получали такое прошение от Сноудена, но в случае получения рассмотрят его в законном порядке.', NULL, '{Россия}', '2013-07-13 09:29:00', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (430, 12, 'c906d61490fd8432f8e3f423cc43930f', 'http://lenta.ru/news/2013/07/13/dtp/', 'В результате ДТП в новой Москве погибли 14 человек', 'В районе поселения Щаповского на территории Троицкого района Москвы рейсовый автобус столкнулся с грузовиком со щебнем. По предварительным данным, в результате ДТП погибли 14 человек, в том числе ребенок, не менее 16 пострадали. К месту аварии направлены 30 бригад «скорой помощи» и вертолет с медиками.', NULL, '{Россия}', '2013-07-13 10:18:00', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (423, 12, '8fc211d1934b329eda5cca2ca8274ff4', 'http://lenta.ru/news/2013/07/13/help/', 'Кандидаты в губернаторы Подмосковья попросили помочь в сборе подписей', 'Пять претендентов на пост губернатора Подмосковья обратились в Совет муниципальных образований Московской области с просьбой помочь им преодолеть «муниципальный фильтр». Совет эту просьбу удовлетворил. Среди обратившихся за помощью — Геннадий Гудков, Александр Федулов, Глеб Фетисов, Николай Дижур и Юрий Топилин.', NULL, '{Россия}', '2013-07-13 06:37:00', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (422, 12, '0ac75c918994520d02c753371ad2daf0', 'http://lenta.ru/news/2013/07/13/iceland/', 'Рейкьявик решил разорвать отношения с Москвой из-за закона о гей-пропаганде', 'Мэр Рейкьявика Йон Гнарр заявил о намерении разорвать партнерские отношения с Москвой в связи с российской позицией в отношении ЛГБТ-сообщества. Москва и Рейкьявик заключили согласшение о побратимских отношениях в 2007 году. Йон Гнарр критиковал закон о гей-пропаганде после принятия его 11 июня.', NULL, '{Мир}', '2013-07-13 04:42:00', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (434, 12, 'd42dc9e0bf795c9cf846fdb3842a6489', 'http://lenta.ru/news/2013/07/13/dtp1/', 'Власти Москвы назвали количество пострадавших при столкновении автобуса и «КамАЗа»', 'В результате столкновения рейсового автобуса и грузовика со щебнем в Троицком округе Москвы пострадали 43 человека. Погибли 14 человек, в том числе один ребенок. Среди пострадавших, по словам Голухова, есть трое детей. В Минздраве РФ отмечают, что не менее 14 пострадавших находятся в тяжелом состоянии.', NULL, '{Россия}', '2013-07-13 11:03:00', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (424, 12, '0f5262188663a2207b6f13999688766a', 'http://lenta.ru/news/2013/07/13/gulnara/', 'Дочь президента Узбекистана лишили иммунитета', 'Дочь президента Узбекистана Ислама Каримова Гульнара лишена дипломатического иммунитета по решению узбекских властей, утверждает «Русская служба Би-Би-Си». Решение о лишении иммунитета было принято после того, как в середине июня во Франции прошли обыски в квартирах, которые приписывают Каримовой.', NULL, '{"Бывший СССР"}', '2013-07-13 07:15:00', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (436, 12, 'cedd371836227dfb28d9fdf42944f4fd', 'http://lenta.ru/news/2013/07/13/update/', 'По факту ДТП под Подольском завели уголовное дело', 'Правоохранительные органы Москвы возбудили уголовное дело по факту ДТП неподалеку от Подольска, в результате которого погибли 14 человек. По уточненным данным, водитель грузовика, который врезался в переполненный рейсовый автобус, остался в живых после аварии. Ранее сообщалось, что он погиб.', NULL, '{Россия}', '2013-07-13 11:53:00', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (435, 12, 'a71ed852e7435d4f59a7c96af4d69bc1', 'http://lenta.ru/news/2013/07/13/bullrun/', 'После забега с быками госпитализированы 21 человек', 'В испанской Памплоне в результате забега быков в субботу госпитализированы 21 человек. По словам врачей, двое из них находятся в тяжелом состоянии с ранениями грудной клетки. Большинство людей пострадали в результате давки, которая образовалась в конце пути, перед выходом на арену для боя быков.', NULL, '{"Из жизни"}', '2013-07-13 11:24:20', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (343, 4, '84414e65982d0c8634048a03ea718cb8', 'http://habrahabr.ru/post/186446/', 'Amazon, Reddit и Яндекс тоже платят разработчикам Adblock Plus', '<img src="http://habr.habrastorage.org/post_images/fcd/10a/bb2/fcd10abb271abdd22fb75aa78f17b0ac.jpg" align="right"/>В последнее время создателей популярнейшего браузерного расширения для блокировки рекламы Adblock Plus обвиняют во всех смертных грехах: в начале года сообщали о том, что они якобы предлагают включать сайты в белой список в обмен на целую треть рекламного бюджета компании, а в июне немецкий блогер заявил, что у Adblock Plus есть стратегические партнёры в рекламной индустрии, назвав ABP рекламной мафией.<br/>
<br/>
Недавно стало известно, что разработчики Adblock Plus получают деньги от Google за то, чтобы реклама от поисковой компании не блокировалась расширением. После <a href="http://habrahabr.ru/post/185786/">публикации этой новости на Хабре</a> со мной связались представители Adblock Plus и предложили провести интервью с одним из управляющих директоров компании Тиллем Фаида. Он не был особо откровенен, но поделился некоторыми деталями.<br/>
<br/>
<b>— На прошлой неделе немецкий сайт Horizont сообщил о том, что Google платит вам за то, чтобы ваше расширение не блокировало его рекламу. Вы можете подтвердить этот факт?</b><br/>
— Да, объявления и спонсорские ссылки в поисковой выдаче Google, а также реклама AdSense на сайтах их партнеров были внесены в список допустимой рекламы Adblock Plus.<br/>
 <a href="http://habrahabr.ru/post/186446/#habracut">Читать дальше &rarr;</a>', 'aleksandrit', '{Интервью,"Adblock Plus",Amazon,Reddit,Яндекс}', '2013-07-12 09:51:02', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (341, 4, '4ac971bf07fe6a0955740c0f1d11dc06', 'http://habrahabr.ru/post/185736/', 'Дайджест новостей из мира мобильной разработки за последнюю неделю №19 (1 — 7 июля 2013)', 'На этой неделе исследователи опубликовали данные о важной уязвимости в Android, открылась программа разработки для браслетов MYO, анонсированы мобильные сервисы Windows Azure, а кроме того Facebook опробовал новую модель монетизации и стал издателем.<br/>
<br/>
<img src="http://habrastorage.org/storage2/81d/530/dd1/81d530dd1e1753cc8086d2e287fdf7c4.jpg"/> <a href="http://habrahabr.ru/post/185736/#habracut">Читать дальше &rarr;</a>', 'DaryaZ', '{"Блог компании «Apps4All»","Mobile Development","разработка мобильных приложений",дайджест,android,"windows phone",устройства,маркетинг}', '2013-07-07 15:17:49', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (346, 4, '00b56a212c345905e882656b726c0ff7', 'http://habrahabr.ru/post/186460/', 'Microsoft помогал АНБ и ФБР шпионить за пользователями Hotmail, Skype и Outlook', '<img src="http://habr.habrastorage.org/post_images/c3d/3d0/e53/c3d3d0e533e305e7f67f57f88bcd0970.jpg" alt="image"/><br/>
<br/>
Эдвард Сноуден передал изданию The Guardian новые документы, свидетельствующие о том, что Microsoft помогал американским спецслужбам перехватывать переписку в Outlook.com и Hotmail, а также аудио- и видеоразговоры в Skype, <a href="http://mashable.com/2013/07/11/miscrosoft-helped-nsa-spy-hotmail-skype-outlook/">пишет</a> Mashable.<br/>
<br/>
«Полученные из Skype звонки были очень чёткими, а метаданные выглядят полными», — говорится в документе, где также положительно оценивается сотрудничество АНБ и ФБР: «Совместная работа стала ключевым моментом в успешном подключении к системе Prism ещё одного поставщика данных».<br/>
 <a href="http://habrahabr.ru/post/186460/#habracut">Читать дальше &rarr;</a>', 'aleksandrit', '{"Информационная безопасность","Эдвард Сноуден",PRISM,SkyDrive,Outlook.com,Hotmail,Skype,Microsoft,АНБ,ФБР}', '2013-07-12 11:26:52', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (397, 4, '506f8bd4cc4d2bed634a42c517c71a3a', 'http://habrahabr.ru/post/186506/', 'Наблюдения и Наука', 'Несколько последних постов позволили сформулироваться точке зрения, но чтобы она не выглядела «взятой с потолка», нужно сделать небольшой экскурс в историю. Пройтись во самым вехам, не сильно углубляясь в детали.<br/>
<br/>
Итак, начнём с древнегреческой науки  <a href="http://habrahabr.ru/post/186506/#habracut">Читать дальше &rarr;</a>', 'qehgt', '{Научно-популярное,наука,исследование,гравитация}', '2013-07-13 01:44:45', true, false);
INSERT INTO items (id, sub_id, hash, link, title, text, author, categories, created, readed, starred) VALUES (339, 4, 'deb28e5b3052f0b0a0aacbe6634c6181', 'http://habrahabr.ru/post/185820/', 'Размышления о кадровой ситуации', 'Несколько сжато — размышления о происходящих в российском ИТ процессах, с кадровой стороны, как я их себе вижу.<br/>
<br/>
Сначала текущая ситуация.<br/>
Сейчас в наличии мы имеем <b>перегретый рынок труда</b>: работы намного больше чем есть специалистов в наличии. Отсюда активный хантинг и некоторый рост зарплат (сильно зависит от конкретного региона и специальности, конечно).<br/>
<br/>
Теперь посмотрим на то, что происходит в образовании.<br/>
<a href="http://habrahabr.ru/post/150403/">30% студентов выбрали в 2012 году ИТ специальности</a> (интересно что покажет статистика 2013 года).<br/>
<b>ИТ в целом стало «модным»</b>, как это было раньше с экономистами и юристами.<br/>
Почему выпускники выбрали именно ИТ? По-видимому, как то, что «интересно, перспективно, инновационно». Помимо этого, люди идут туда где деньги, родители говорят детям — поступай вот туда, всегда будешь при деньгах.<br/>
ИТ работает и как «социальный лифт» — предоставляет возможность при желании и наличии способностей подняться в более высокие сферы, не располагая деньгами и нужными связями.<br/>
 <a href="http://habrahabr.ru/post/185820/#habracut">Читать дальше &rarr;</a>', 'nzeemin', '{"Human Resources","кадры решают все",IT}', '2013-07-07 17:20:54', true, false);


--
-- Name: seq_items; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('seq_items', 436, true);


--
-- Name: seq_subs; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('seq_subs', 14, true);


--
-- Data for Name: subs; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO subs (id, user_id, url, name, last_update, "order") VALUES (4, '6f757c56f8b344979c94786f26a24aff', 'http://habrahabr.ru/rss/hubs/', 'Хабрахабр / Захабренные / Тематические / Посты', '2013-07-13 11:59:40.254', 2);
INSERT INTO subs (id, user_id, url, name, last_update, "order") VALUES (12, '6f757c56f8b344979c94786f26a24aff', 'http://lenta.ru/rss', 'Lenta.ru : Новости', '2013-07-13 11:59:44.731', 0);


--
-- Data for Name: tags; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tags (user_id, sub_id, tag) VALUES ('6f757c56f8b344979c94786f26a24aff', 4, 'Habr');
INSERT INTO tags (user_id, sub_id, tag) VALUES ('6f757c56f8b344979c94786f26a24aff', 4, 'Программирование');
INSERT INTO tags (user_id, sub_id, tag) VALUES ('6f757c56f8b344979c94786f26a24aff', 12, 'Новости');


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO users (id, name) VALUES ('6f757c56f8b344979c94786f26a24aff', 'undefined undefined');


--
-- Name: ind_hash; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY items
    ADD CONSTRAINT ind_hash UNIQUE (hash);


--
-- Name: pk_items; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY items
    ADD CONSTRAINT pk_items PRIMARY KEY (id);


--
-- Name: pk_subs; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY subs
    ADD CONSTRAINT pk_subs PRIMARY KEY (id);


--
-- Name: pk_tags; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tags
    ADD CONSTRAINT pk_tags PRIMARY KEY (user_id, sub_id, tag);


--
-- Name: pk_users; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT pk_users PRIMARY KEY (id);


--
-- Name: fki_items_subs; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX fki_items_subs ON items USING btree (sub_id);


--
-- Name: fki_tags_subs; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX fki_tags_subs ON tags USING btree (sub_id);


--
-- Name: ind_subs_userurl; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX ind_subs_userurl ON subs USING btree (user_id, url);


--
-- Name: fk_items_subs; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY items
    ADD CONSTRAINT fk_items_subs FOREIGN KEY (sub_id) REFERENCES subs(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: fk_subs_users; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY subs
    ADD CONSTRAINT fk_subs_users FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: fk_tags_subs; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tags
    ADD CONSTRAINT fk_tags_subs FOREIGN KEY (sub_id) REFERENCES subs(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: fk_tags_users; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tags
    ADD CONSTRAINT fk_tags_users FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

CREATE ROLE rss;
ALTER ROLE rss WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION PASSWORD 'md5d71962958e360a04cd1ebd3ea35ad29c' VALID UNTIL 'infinity';


REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- Name: seq_items; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE seq_items FROM PUBLIC;
REVOKE ALL ON SEQUENCE seq_items FROM postgres;
GRANT ALL ON SEQUENCE seq_items TO postgres;
GRANT USAGE ON SEQUENCE seq_items TO rss;


--
-- Name: items; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE items FROM PUBLIC;
REVOKE ALL ON TABLE items FROM postgres;
GRANT ALL ON TABLE items TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE items TO rss;


--
-- Name: seq_subs; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE seq_subs FROM PUBLIC;
REVOKE ALL ON SEQUENCE seq_subs FROM postgres;
GRANT ALL ON SEQUENCE seq_subs TO postgres;
GRANT USAGE ON SEQUENCE seq_subs TO rss;


--
-- Name: subs; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE subs FROM PUBLIC;
REVOKE ALL ON TABLE subs FROM postgres;
GRANT ALL ON TABLE subs TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE subs TO rss;


--
-- Name: tags; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE tags FROM PUBLIC;
REVOKE ALL ON TABLE tags FROM postgres;
GRANT ALL ON TABLE tags TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE tags TO rss;


--
-- Name: users; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE users FROM PUBLIC;
REVOKE ALL ON TABLE users FROM postgres;
GRANT ALL ON TABLE users TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE users TO rss;


--
-- PostgreSQL database dump complete
--

