daomodule = require './dao'
dao = daomodule.getDao()

exports.getUser = (id, callback) -> dao.getUser id, callback
exports.saveUser = (user, callback) -> dao.saveUser user, callback
