daomodule = require './dao'
feedparser = require 'feedparser'
request = require 'request'
htmlparser = require 'htmlparser2'

dao = daomodule.getDao()

exports.discoveryFeeds = (url, callback) ->
  getFeed = (url, callback) ->
    request(url)
      .on('error', (err) -> callback err)
      .pipe(new feedparser)
      .on('error', (err) -> callback err)
      .on('meta', (meta) -> callback null, meta)

  getFeed url, (err, meta) ->
    if err
      request url, (err, res, body) ->
        if !err and res.statusCode == 200
          urls = []
          parser = new htmlparser.Parser({
            onopentag: (name, attrs) ->
              urls.push attrs.href if name == 'link' and attrs.rel == 'alternate' and attrs.href?
            onclosetag: (name) ->
              parser.reset() if name == 'head' # stop parse
          })
          parser.write body
          parser.end()

          feeds = []
          recursiveGetFeeds = (urls) ->
            url = urls.pop()
            if url?
              getFeed url, (err, meta) ->
                feeds.push meta unless err
                recursiveGetFeeds urls
            else
              callback null, feeds # exit
          recursiveGetFeeds urls
        else
          callback err ? new Error("Http request #{url} return #{res.statusCode} status.")
    else
      callback null, [meta]


exports.discoveryItems = (url, callback) ->
  items = []
  error = null
  request(url)
    .on('error', (err) -> callback err)
    .pipe(new feedparser)
    .on('error', (err) -> error = err)
    .on('readable', ->
      stream = this
      while item = stream.read()
        items.push item
    )
    .on('end', -> callback error, items)


exports.discoveryAndSaveItems = (feed, callback)->
  @discoveryItems feed.link, (err, items)->
    if err
      callback err
    else
      dao.feedLastUpdate feed.id, (err)->
        if err
          callback err
        else
          dao.saveItems feed.id, items, (err)->
            callback err


exports.getFeeds = (user_id, callback) ->
  dao.getFeeds user_id, callback

exports.saveFeed = (user_id, feed, callback) ->
  dao.saveFeed user_id, feed, callback

exports.removeFeed = (user_id, feed_id, callback) ->
  dao.removeFeed user_id, feed_id, callback

exports.tagFeed = (user_id, feed_id, tag, callback) ->
  dao.tagFeed user_id, feed_id, tag, callback


exports.getItems = (feed_id, options, callback) ->
  if feed_id == "recommended"
    return callback new Error('Not Implemented')

  if !feed_id?
    feed_id = "all"

  if feed_id == "starred"
    options.starred = true

  if feed_id.substring(0, 4) == "tag:"
    options.tag = feed_id.substring(4)

  if isNaN(parseInt(feed_id))
    dao.getItems options, (err, items) ->
      if err
        callback err
      else
        dao.getItemsCount options, (err, count) ->
          if err
            callback err
          else
            callback null, { id: feed_id, unread: count }, items
  else
    dao.getFeed feed_id, (err, feed) =>
      if err
        return callback err
      if !feed
        return callback new Error('Not Found')

      options.feed_id = feed.id
      if getUtcNow() - feed.lastupdate > 1000 * 60 * 5 # 5min
        @discoveryAndSaveItems feed, (err) ->
          if err
            callback err
          else
            dao.getItems options, (err, items) -> callback err, feed, items
      else
        dao.getItems options, (err, items) -> callback err, feed, items

exports.readItems = (ids, read, callback) ->
  dao.readItems ids, read, callback

exports.starItem = (item_id, state, callback) ->
  dao.starItem item_id, state, callback


getUtcNow = ->
  now = new Date()
  now.getTime() + now.getTimezoneOffset() * 60000