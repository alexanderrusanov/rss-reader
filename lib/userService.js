// Generated by CoffeeScript 2.0.0-beta5
void function () {
  var dao, daomodule;
  daomodule = require('./dao');
  dao = daomodule.getDao();
  exports.getUser = function (id, callback) {
    return dao.getUser(id, callback);
  };
  exports.saveUser = function (user, callback) {
    return dao.saveUser(user, callback);
  };
}.call(this);
