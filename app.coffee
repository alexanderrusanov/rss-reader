configs = require('./configs')
express = require('express')
routes = require('./routes/index')
http = require('http')
path = require('path')

app = express()

#all environments
app.set('port', process.env.PORT || configs.port)
app.set('views', __dirname + '/views')
app.set('view engine', 'jade')
app.use(express.favicon())
app.use(express.logger('dev'))
app.use(express.bodyParser())
app.use(express.methodOverride())
app.use(app.router)
app.use(require('less-middleware')({ src: __dirname + '/public' }))

###
  map both directories as static
  So bower packages will be availible at /packages/...
  And our static will be at /static/...
  This will help to route balancer later
###
app.use('/static',express.static(path.join(__dirname, 'public')));
app.use('/new_client',express.static(path.join(__dirname, '/new_client/build')));
app.use('/bin',express.static(path.join(__dirname, '/new_client/bin')));
#app.use('/packages',express.static(path.join(__dirname, '/client_lib')));


#development only
if ('development' == app.get('env'))
  app.use(express.errorHandler())

###
  Configure route urls
###
app.get('/', routes.index)
app.post('/authorize', routes.authorize)
app.get('/feeds', routes.feeds)
app.get('/add_feed', routes.addFeed)
app.post('/unsubscribe', routes.unsubscribe)
app.post('/toggle_tag', routes.tagFeed)
app.get('/feed/:id', routes.feedItems)
app.post('/feed/read', routes.readItem)
app.post('/toggle_starred', routes.starItem)

http
  .createServer(app)
  .listen(app.get('port'), -> console.log("Express server listening on port " + app.get('port')))

