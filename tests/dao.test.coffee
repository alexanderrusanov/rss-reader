nodeunit = require 'nodeunit'

exports.setUp = (done) ->
  console.log 'dao test setup'
  @dao = new (require '../lib/dao').Dao('postgres://postgres:111111@localhost/rss')
  done()

exports.tearDown = (done) ->
  console.log 'dao test teardown'
  @dao.close() if @dao
  done()

exports.CRUDUser = (test) ->
  user = {
    id: 'id1'
    name: 'Nikolay Ivanov'
  }
  self = this

  self.dao.saveUser user, (err, u) ->
    nodeunit.assert.ifError err

    self.dao.getUser u.id, (err, u) ->
      nodeunit.assert.ifError err
      test.equal u.id, user.id
      test.equal u.name, user.name

      self.dao.getUser 'not found', (err, u) ->
        nodeunit.assert.ifError err
        test.equal u, null

        test.done()

exports.CRUDFeed = (test) ->
  user = {
    id: 'id2'
    name: 'Test User'
  }
  self = this

  self.dao.saveUser user, (err, u) ->
    feed = {
      id: null,
      link: 'http://example.com'
      title: 'Test Feed'
    }
    self.dao.saveFeed u.id, feed, (err, saved) ->
      nodeunit.assert.ifError err
      test.equal saved.link, feed.link
      test.equal saved.title, feed.title
      test.ok saved.id

      self.dao.getFeed saved.id, (err, getted) ->
        nodeunit.assert.ifError err
        test.equal getted.id, saved.id
        test.equal getted.link, saved.link
        test.equal getted.title, saved.title
        test.ok getted.lastupdate
        test.ok getted.tag

        self.dao.saveFeed u.id, feed, (err, saved) ->
          nodeunit.assert.ifError err
          test.equal saved.link, feed.link
          test.equal saved.title, feed.title
          test.ok saved.id

          self.dao.getFeeds u.id, (err, feeds) ->
            nodeunit.assert.ifError err
            test.equal feeds.length, 1
            test.done()
