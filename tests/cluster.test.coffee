cluster = require 'cluster'
os = require 'os'
util = require 'util'

tasks = [1..10]

startTasks = ->
  if Object.keys(cluster.workers).length < os.cpus().length and (task = tasks.pop())?
    cluster.fork({ task: task })
    startTasks()

if cluster.isMaster
  cluster.on 'exit', (worker, code, signal) ->
    util.log "worker #{worker.process.pid} exit";
    startTasks()

  startTasks()
else if cluster.isWorker
  util.log "process task #{process.env.task}"
  setTimeout ->
    process.exit()
  ,1000
