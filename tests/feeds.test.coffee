nodeunit = require 'nodeunit'
rssService = require '../lib/rssService'
express = require 'express'
path = require 'path'

app = new express
port = 3000
server = undefined

exports.setUp = (done) ->
  dir = path.join __dirname, '/../public'
  app.use '/static', express.static dir
  server = app.listen port
  console.log "\nStart listen http://localhost:#{port} at #{dir}"
  done()

exports.tearDown = (done) ->
  server.close() if server
  new (require '../lib/dao').Dao('postgres://postgres:111111@localhost/rss').close()
  console.log "Stop listen\n"
  done()

exports.discoveryFeeds = (test) ->
  url = 'http://localhost:3000/static/multirss.html'
  rssService.discoveryFeeds url, (err, feeds) ->
    nodeunit.assert.ifError err
    test.equal feeds.length, 2
    test.equal feeds[0].title, 'Teamlab Portal'
    test.equal feeds[1].title, 'Teamlab Community'
    console.log "getFeeds #{url}, feeds count: #{feeds.length}"
    test.done()

exports.discoveryItems = (test) ->
  url = 'http://localhost:3000/static/feed1.xml'
  rssService.discoveryItems url, (err, items) ->
    nodeunit.assert.ifError err
    test.equal items.length, 3
    test.equal items[0].title, "Community: Change structure of project table"
    test.equal items[1].title, "Community: Releases frequency / modular development"
    test.equal items[2].title, "Community: Releases frequency / modular development"
    console.log "getItems #{url}, items count: #{items.length}"
    test.done()