fs = require('fs')
url = require('url')
path = require('path')
favicon = require('favicon')
wget = require('wget')
imagemagick = require('imagemagick')


getTempDir = ()->
  tmpNames = ['TMPDIR', 'TMP', 'TEMP']
  tmpDir = null
  for tmpName in tmpNames when tmpDir is null
    do (tmpName)->
      if process.env[tmpName]? and typeof process.env[tmpName] != 'undefined'
        tmpDir = process.env[tmpName]
  return tmpDir

if process.platform.match(/^win/)
  imagemagick.convert.path = 'C:\\Program Files (x86)\\ImageMagick-6.8.6-Q8\\convert'


exports.get = (siteUrl, callback) ->
  parsedUrl = url.parse(siteUrl, false, true)
  favicon "#{parsedUrl.protocol}//#{parsedUrl.host}", (err, faviconUrl)->
    if faviconUrl?
      tempDir =  getTempDir()
      tmpFilename = path.join(tempDir, 'reader-fav-'+Date.now() + path.extname(faviconUrl));
      filename = path.join(tempDir,'reader-fav-'+Date.now() + '.png');
      download = wget.download(faviconUrl, tmpFilename)
      download.on 'error', (err)->
        callback(err, null)
      download.on 'end', ->
        imagemagick.convert [tmpFilename, '-thumbnail', '16x16', '-flatten', '-strip',filename], (err, stdout)->
          process.nextTick -> fs.unlink(tmpFilename)
          if err?
            callback(err,null)
          else
            fs.readFile filename, (err,data)->
              if err?
                callback(err,null)
              else
                process.nextTick -> fs.unlink(filename)
                callback(null,'data:image/png;base64,'+ data.toString('base64'))
    else
      callback(err,null)