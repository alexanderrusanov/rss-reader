(function() {
  var favicon, fs, getTempDir, imagemagick, path, url, wget;

  fs = require('fs');

  url = require('url');

  path = require('path');

  favicon = require('favicon');

  wget = require('wget');

  imagemagick = require('imagemagick');

  getTempDir = function() {
    var tmpDir, tmpName, tmpNames, _i, _len;
    tmpNames = ['TMPDIR', 'TMP', 'TEMP'];
    tmpDir = null;
    for (_i = 0, _len = tmpNames.length; _i < _len; _i++) {
      tmpName = tmpNames[_i];
      if (tmpDir === null) {
        (function(tmpName) {
          if ((process.env[tmpName] != null) && typeof process.env[tmpName] !== 'undefined') {
            return tmpDir = process.env[tmpName];
          }
        })(tmpName);
      }
    }
    return tmpDir;
  };

  if (process.platform.match(/^win/)) {
    imagemagick.convert.path = 'C:\\Program Files (x86)\\ImageMagick-6.8.6-Q8\\convert';
  }

  exports.get = function(siteUrl, callback) {
    var parsedUrl;
    parsedUrl = url.parse(siteUrl, false, true);
    return favicon("" + parsedUrl.protocol + "//" + parsedUrl.host, function(err, faviconUrl) {
      var download, filename, tempDir, tmpFilename;
      if (faviconUrl != null) {
        tempDir = getTempDir();
        tmpFilename = path.join(tempDir, 'reader-fav-' + Date.now() + path.extname(faviconUrl));
        filename = path.join(tempDir, 'reader-fav-' + Date.now() + '.png');
        download = wget.download(faviconUrl, tmpFilename);
        download.on('error', function(err) {
          return callback(err, null);
        });
        return download.on('end', function() {
          return imagemagick.convert([tmpFilename, '-thumbnail', '16x16', '-flatten', '-strip', filename], function(err, stdout) {
            process.nextTick(function() {
              return fs.unlink(tmpFilename);
            });
            if (err != null) {
              return callback(err, null);
            } else {
              return fs.readFile(filename, function(err, data) {
                if (err != null) {
                  return callback(err, null);
                } else {
                  process.nextTick(function() {
                    return fs.unlink(filename);
                  });
                  return callback(null, 'data:image/png;base64,' + data.toString('base64'));
                }
              });
            }
          });
        });
      } else {
        return callback(err, null);
      }
    });
  };

}).call(this);
