nodeunit = require 'nodeunit'


exports.testFavicon = (test) ->
  require('../favicon').get 'http://www.google.com', (err,base64data)->
    nodeunit.assert.ifError err
    test.ok base64data
    test.ok typeof base64data == 'string'
    test.ok base64data.indexOf('data:image/png;base64,') == 0
    test.done()

exports.testBadFavicon = (test) ->
  require('../favicon').get 'blabla.com', (err,base64data)->
    test.ok err
    test.ok base64data==null
    test.done()