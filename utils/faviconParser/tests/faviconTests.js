(function() {
  var nodeunit;

  nodeunit = require('nodeunit');

  exports.testFavicon = function(test) {
    return require('../favicon').get('http://www.google.com', function(err, base64data) {
      nodeunit.assert.ifError(err);
      test.ok(base64data);
      test.ok(typeof base64data === 'string');
      test.ok(base64data.indexOf('data:image/png;base64,') === 0);
      return test.done();
    });
  };

  exports.testBadFavicon = function(test) {
    return require('../favicon').get('blabla.com', function(err, base64data) {
      test.ok(err);
      test.ok(base64data === null);
      return test.done();
    });
  };

}).call(this);
