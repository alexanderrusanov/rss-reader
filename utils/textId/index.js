(function() {
  exports.getTextId = function(text, wordCount) {
    if (wordCount == null) {
      wordCount = 20;
    }
    return text.split(/[,\.;\s:!\?"']/i).filter(function(e) {
      return e && e !== '';
    }).slice(0, wordCount).join(' ').trim(' ').toLowerCase();
  };

}).call(this);
