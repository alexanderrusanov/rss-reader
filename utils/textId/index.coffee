exports.getTextId = (text, wordCount=20)->
  #Remove punctuation and get first N words then join and convert to lower
  text.split(/[,\.;\s:!\?"']/i).filter((e)->e and e!='').slice(0,wordCount).join(' ').trim(' ').toLowerCase()
