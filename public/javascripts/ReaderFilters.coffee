###
  Filters
  TODO: Move to separate files
###
angular.module('ReaderFilters', [])
  .filter('unread', ()-> return (input)-> if input>999 then '999+' else input)
  .filter('byTag', ()-> return (input,tag)->
    #extract items by tag
    output={}
    output[key]=item for own key,item of input when tag in item.tag
    return output
  )
  .filter('stripHtml', ()-> return (input)->
    #strip html tags from string
    regex = /(<([^>]+)>)/ig;
    return input.replace(regex, "")
  )
  .filter('momentAgo', ()-> return (input)->
    #use moment.js to format time
    moment(input).fromNow()
  )