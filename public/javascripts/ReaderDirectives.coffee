

###
  Directives
  TODO: Move to separate file
###
angular.module('ReaderDirectives', [])
  .directive('scrollIf', ()-> return (scope,element,attributes)->
    setTimeout(()->
      if scope.$eval(attributes.scrollIf)
        window.scrollTo(0, element[0].offsetTop - 100)
    )
  )
  .directive("bsTooltip",()-> return (scope,element,attributes)->
    attributes.$observe 'title', (value)->
      element.removeData('tooltip');
      element.tooltip({placement:attributes['bsTooltip']});
  )
  .directive("readerScrollPosition",['$window',($window)-> return (scope,element,attributes)->
    ###
    Feed selection by scroling
    ###


    attributes.$observe 'readerScrollPosition',(jsonSettings)->

      windowEl = angular.element($window)
      settings = angular.fromJson(jsonSettings)

      isEnabled = true
      if attributes.readerScrollPositionDisabled?
        isEnabled = scope.$eval attributes.readerScrollPositionDisabled
        scope.$watch attributes.readerScrollPositionDisabled, (value,oldValue)->
          isEnabled = !value
          if value!=oldValue && isEnabled
            $selected = $("##{settings.id}#{scope[settings.scopeVar]}")
            scope.$evalAsync ()->
              offset = $selected.offset()
              if offset?
                $('html, body').animate(
                  scrollTop: offset.top - 50
                , 500)

      onScroll = ()->
        ## find a closest feed item
        if !isEnabled
          return#return if disabled
        scroll= windowEl.scrollTop()
        $(settings.selector).each ()->
          el = $(@)
          if (el.offset().top+el.height()) > scroll
            ## get Id and pass to scope
            id = el.attr('id').substr(settings.id.length)
            ## Apply scope variable
            scope.$apply ()-> scope[settings.scopeVar] = id
            return false
      throttled = _.throttle(onScroll, 300)
      windowEl.on 'scroll', throttled
      #ondestroy remove handler
      scope.$on('$destroy', ()->
        windowEl.off 'scroll', throttled)
      null
  ])
  .directive("readerNavigate",()-> return (scope,element,attributes)->
    ###
    Up and down feed navigation directive.
    TODO: it depends on markup, shoul fix it
    ###
    settings={}
    onClick = ()->
      #Find all by selector
      selected = $(settings.activeSelector).first()
      if selected.length==0
        selectElement($(settings.selector).first())
      else if settings.direction is 'up'
        #scroll up
        newSelection = selected.parent().prevAll(":visible").find(settings.selector).last()
      else if settings.direction is 'down'
        #scroll up
        newSelection = selected.parent().nextAll(":visible").find(settings.selector).first()

      selectElement(newSelection)
      return false

    selectElement = ($selected)->
      if $selected? and $selected.length>0
        id = $selected[0].id.substr(settings.id.length)
        #scroll to element
        $('html, body').animate(
          scrollTop: $selected.offset().top - 50
        , 500);
        ## Apply scope variable
        scope.$apply ()-> scope[settings.scopeVar] = id

    element.on 'click', onClick
    #ondestroy remove handler
    scope.$on('$destroy', ()->
      element.on 'click', onClick)
    attributes.$observe 'readerNavigate',(jsonSettings)->
      settings = angular.fromJson(jsonSettings)
    null
  )