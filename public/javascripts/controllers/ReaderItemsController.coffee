angular.module('Reader').controller 'ReaderItemsCtrl', ['$scope', '$http','$timeout','readerData','readerSettings', ($scope, $http,$timeout,readerData) ->
  $scope.feedItems = []
  $scope.wasSelectedItems = []




  $scope.nextItems = ()->
    ##paging with inifinite scroll
    if $scope.loading
      return
    if $scope.feedItems.length>0
      fromLastId = $scope.feedItems.length
      readerData.getFeedItems($scope.selectedFeedId,
      (data)->
        $scope.selectedFeedAllDataLoaded = data.length == 0
        $scope.feedItems.push item for item in data
        $scope.feedItems = processFeedItems($scope.feedItems)
        null
      , fromLastId)

  #TODO: Refactor this to use on variable for all selected of viewed item
  $scope.$watch 'selectedFeedItemId', (itemId,oldValue)->
    if itemId? and itemId!=oldValue
      markUnread(parseInt(itemId))

  $scope.$watch 'settings.showUnreadItems', (itemId,oldValue)->
    #reload items when settings changed
    if $scope.selectedFeedId? then readerData.getFeedItems $scope.selectedFeedId, (data)->
      $scope.feedItems = processFeedItems(_.union($scope.feedItems, data))

  processFeedItems=(items)->
    _.unique(_.sortBy(items, (item)-> -item.created),true,(item)->item.id)

  $scope.expandItem=(itemId)->
    if itemId == $scope.selectedFeedItemId
      $scope.selectedFeedItemId = null
    else
      $scope.selectedFeedItemId = itemId
    null

  #Watch selected item from outer scope
  $scope.$watch 'selectedFeedId', (newFeedId,oldValue)->
    $scope.wasSelectedItems = [];
    $scope.selectedFeedItemId = null
    if newFeedId?
      # queryservice
      readerData.getFeedItems newFeedId, (data)->
        $scope.feedItems = data
        $scope.feedItems = processFeedItems($scope.feedItems)


  $scope.wasSelected=(itemId)->
    itemId in $scope.wasSelectedItems

  ###
  Helper for mark as unread
  ###
  markUnread=(id)->
    item = _.find($scope.feedItems,(item)->item.id==id)
    if item? and item.unread
      item.unread=false

      if $scope.feeds[$scope.selectedFeedId]?
        $scope.feeds[$scope.selectedFeedId].unread--
      if item.feedId?
        $scope.feeds[item.feedId].unread--

      $scope.wasSelectedItems.push id
      readerData.markReaded(id)
    null

  #Mark all items as read
  $scope.$on 'event:markAllAsRead', ()->
    items = for item in $scope.feedItems when item.unread
      do (item)->
        item.unread = false
        $scope.wasSelectedItems.push item.id
    readerData.markReaded(items)


  ## Watch the selectedFeedItemId
  $scope.$watch('selectedFeedItemId', (newValue, oldValue)->
    markUnread(parseInt(oldValue)) if oldValue?
  )
]
