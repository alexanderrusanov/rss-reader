angular.module('Reader').controller 'ReaderCtrl', ['$scope', '$http','$timeout','readerData','readerSettings','readerLogin','$dialog', ($scope, $http,$timeout,readerData,readerSettings,readerLogin, $dialog) ->
  #initial settings
  initDefaults=(scope)->
    scope.settings = defaults=
        tagState:{}
        showUnread:true
        expandedMode:true
        showUnreadItems:true
    scope.feeds = {}
    $scope.predefinedFeeds = {}
    scope.selectedFeedId = null
    scope.selectedFeedItemId = null
    scope.selectedFeedAllDataLoaded = false
    scope.refreshTimeout = null
    if scope.refreshTimeout?
      $timeout.cancel(scope.refreshTimeout)

  initDefaults($scope)


  $scope.refresh = () ->
    ###
    Reset all here
    ###
    $scope.selectedFeedId = null
    $scope.selectedFeedItemId = null
    $scope.getFeeds()

  $scope.getFeeds = ()->
    # Query service
    readerData.getFeeds (data)->
      $scope.feeds=_.extend($scope.feeds, data)#merge data
      $scope.getPredefinedFeeds()
      #set refrsh timeout
      if $scope.refreshTimeout?
        $timeout.cancel($scope.refreshTimeout)
      $scope.refreshTimeout=$timeout(
        ()-> $scope.getFeeds()
      ,180000) ##refresh timeout = 3 min

  $scope.$on 'event:feed-subscribed', (evt,feedId, feed)->
    $scope.feeds[feedId] = feed
    $scope.selectFeed(feedId)


  $scope.unreadCountByTag = (tag) ->
    counter=0;
    counter=counter+item.unread for own key,item of $scope.feeds when tag in item.tag
    return counter

  $scope.getPredefinedFeeds = ()->
    unread=0
    unread=unread+feed.unread for own key,feed of $scope.feeds
    return $scope.predefinedFeeds =
      all:
        tag:[]
        unread:unread
        title:'All items'
      starred:
        tag:[]
        unread:0
        title:'Starred'

  $scope.tags = ()->
    tags={}
    for own key,item of $scope.feeds
      do (item)->
        tags[tag]=null for tag in item.tag when not tags.hasOwnProperty(tag) and tag?
        null
    return tags

  $scope.hasTag = (feedId,tag) ->
    if feedId? and tag? and $scope.feeds? and $scope.isRealFeed(feedId)
      _.indexOf($scope.feeds[feedId].tag,tag)!=-1
    else false

  $scope.tagOpened = (tag, newState)->
    if newState?
      $scope.settings.tagState[tag]=newState
    return $scope.settings.tagState[tag] ? true

  $scope.isRealFeed = (feedId) -> not /all|starred|tag:.*/i.test feedId

  $scope.addTag= (feedId)->
    d = $dialog.dialog({templateUrl:'add_tag.html',controller:'DialogCtrl'});
    d.open().then (tagname)->
      if tagname?
        if $scope.feeds?
          #add to feeds
          feed = $scope.feeds[feedId]
          if feed?
            feed.tag = feed.tag || []
            feed.tag.push tagname
            feed.tag = _.uniq(feed.tag)
        #Tell service that tags changed
        readerData.toggleTag(feedId, tagname)

  $scope.toggleTag= (feedId, tagname)->
    #add to selected first
    if $scope.feeds?
      #add to feeds
      feed = $scope.feeds[feedId]
      if feed?
        feed.tag = feed.tag || []
        if _.indexOf(feed.tag,tagname)!=-1
          #remove
          feed.tag = _.without(feed.tag,tagname)
        else
          feed.tag.push tagname
        feed.tag = _.uniq(feed.tag)
      readerData.toggleTag(feedId, tagname)

  $scope.unsubscribe = (feedId)->
    readerData.unsubscribe(feedId, (data)->
      $scope.selectFeed(null)
    )

  $scope.selectFeed = (feedId) ->
    $scope.selectedFeedId = feedId
    #if it's not aggreagate by tag or by all
    $scope.$broadcast('event:update-list')

  $scope.getSelectedFeedTitle = ()->
    if $scope.selectedFeedId?
      if /all|starred/i.test $scope.selectedFeedId
        $scope.predefinedFeeds[$scope.selectedFeedId].title
      else if /tag:.*/i.test $scope.selectedFeedId
        $scope.selectedFeedId.substr('tag:'.length)
      else
        $scope.feeds[$scope.selectedFeedId].title

  $scope.markAllAsRead=()->
    $scope.$broadcast('event:markAllAsRead')

  $scope.setExpandedMode = (state)->
    $scope.settings.expandedMode = state

  $scope.setShowUnreadItems=(state)->
    $scope.settings.showUnreadItems = state

  $scope.signOut = ()->
    initDefaults($scope)
    readerSettings.destroy()
    readerLogin.signOut()

  init = (userToken)->
    initDefaults($scope)
    #settings
    $scope.settings = readerSettings.getAndWatch(userToken,
        defaults=
          tagState:{}
          showUnread:true
          expandedMode:true
          showUnreadItems:true
      )
    $scope.refresh()

  #setup initialization
  readerLogin.onSignIn = init
  if $scope.isAuthorized then init($scope.authorization)

  null
]


angular.module('Reader').controller 'DialogCtrl', ['$scope','dialog',($scope,dialog)->
  $scope.close = (result)->
   dialog.close(result)
]