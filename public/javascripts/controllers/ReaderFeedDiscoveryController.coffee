angular.module('Reader').controller 'ReaderFeedDiscoveryCtrl', ['$scope', '$http','$timeout','readerData', ($scope, $http,$timeout,readerData) ->

  $scope.$watch 'discovery.active', (value)->
    null

  queryService= (url)->
    readerData.performGetRequest "/add_feed?url=#{encodeURIComponent(url)}", (data)->
      if data? and data.subscribed and data.feed? and data.feedId?
        #push feed to all feeds
        $scope.$emit 'event:feed-subscribed', data.feedId, data.feed
        $scope.discovery.active=false
      else if data? and not data.subscribed and data.multiple
        #we have multiple feeds on that url
        $scope.discovery.feeds = data.feeds
        null

  $scope.discoverFeed = ()->
    if $scope.discovery.feeds?
      #query service with selected
      queryService item.url for item in $scope.discovery.feeds when item.checked
      null
    #make query to discovery
    queryService($scope.discovery.feedUrl)

  null
]