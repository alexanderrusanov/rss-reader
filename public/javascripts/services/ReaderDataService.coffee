class DataService
  constructor: (@scope,@http,@store) ->
    @scope.loading = false
    @scope.error = null

    @urls=
      feedUrl:"/feeds"
      feedItemsBase:"/feed/"
      feedReaded:"/feed/read"
      toggleTag:"toggle_tag"
      unsubscribe:"unsubscribe"
    @readed=[]

    @__pushReaded= _.throttle(()=>
      #send readed ids to server
      @http.post(@.urls.feedReaded, _.uniq(_.flatten(@readed)))
        .success (data)=>
          @readed=[]
          null
    , 1000)

    @scope.$on 'event:quata-exceeded', ()->
      #delete old
      stored = @store.get("feedSaved",{})
      min = new Date()
      minId = null
      for own id,date of stored
        do (id,date)->
          if min > date
            min = date
            minId = id
      if minId?
        delete stored[minId]
        @store.remove "feed#{minId}"
        @store.set("feedSaved",stored)

  toggleTag: (feedId, tagname, callback)->
    @.performRequest('post',@.urls.toggleTag, {feedId:feedId, tag:tagname},callback)

  unsubscribe: (feedId, callback)-> @.performRequest('post',@.urls.unsubscribe, {feedId:feedId},callback)

  getFeeds: (callback)->
    @.performGetRequest @.urls.feedUrl,
      (data)=>
        #store localy
        @store.set("feeds",data)
        callback(data)
      ,(data,status) =>
        #get local stored
        storedData = @store.get("feeds",{})
        if storedData? then callback(storedData);true else false

  getFeedItems: (id,callback,from=null)->
    params = {}
    if @scope.settings.showUnreadItems then params.unread=true
    if from? then params.from=from
    query=("#{key}=#{value}" for own key,value of params).join('&')
    @.performGetRequest ("#{@.urls.feedItemsBase}#{id}"+if query!='' then "?#{query}" else ''),
      (data)=>
        #store localy
        if from?
          #if from specified the append items
          storedData = @store.get("feed#{id}",[])
          storedData.push item for item in data
          @.setOfflineItems(id,storedData)
        else
          @.setOfflineItems(id,data)
        callback(data)
      ,(data,status) =>
        #get local stored
        storedData = @store.get("feed#{id}",[])
        #todo: check from id
        if from?
          storedData = _.filter(storedData, (item)-> return item.id > from )
        if storedData? and storedData.length>0 then callback(storedData);true else false

  setOfflineItems: (id, items)=>
    stored = @store.get("feedSaved",{})
    stored[id]=new Date()
    @store.set("feedSaved",stored)
    @store.set("feed#{id}",_.unique(_.sortBy(items, (item)->item.created),true,(item)->item.id))

  markReaded: (ids)->
    @readed.push ids
    @__pushReaded()



  performRequest: (type,url,data,callback,error)->
    @scope.loading = true
    @http[type](url,data)
      .success (data)=>
        @scope.loading = false
        @scope.error= null
        #got items from server
        if data?
          callback(data)
      .error (data,status)=>
        #TODO: Add cache loading
        @scope.loading = false
        if !error?(data,status) then @scope.error= "##{status} Network error occured" else @scope.error=null

  performGetRequest: (url,callback,error)->
    @.performRequest('get',url,null, callback, error)

angular.module('ReaderServices')
  .factory("readerData", ['$http', '$rootScope','readerStorage', ($http, $rootScope,readerStorage)->
    return new DataService($rootScope,$http,readerStorage)
  ])
