###
Login service
###
class LoginService
  constructor: (@scope,@http,@store,@authService) ->
    @scope.$watch 'authorization', (newValue,oldValue)=>
      #set auth header if it's stored
      if newValue? and oldValue!=newValue
        @store.set("auth",newValue)
        @http.defaults.headers.common['Authorization']=newValue
        @.signIn()
      else if !newValue?
        delete @http.defaults.headers.common.Authorization
        @store.set("auth",null)

    @scope.authorization = @store.get("auth",null)
    #set immideatly
    if @scope.authorization
      @http.defaults.headers.common['Authorization']=@scope.authorization
      @.signIn()

    @scope.isAuthorized = ()=>
      @scope.authorization?

    @scope.$on 'event:auth-loginConfirmed', (event,data)=>
      #login confirmed store token
      if data?
        @scope.authorization = data

    @scope.$on 'event:auth-loginRequired', (event,data)=>
      #login failed
      @scope.authorization = null
      @scope.loading = false

    @scope.$on 'event:auth-loginToken', (event,token)=>
      #login confirmed store token
      if token?
        @scope.loading = true
        @http.post("/authorize",{token:token})
          .success (data)=>
            @scope.loading = false
            if data? and data.authToken?
              @authService.loginConfirmed(data.authToken,{headers:{'Authorization':data.authToken}})
            else if data.error?
              @scope.authorizationError = data.error
          .error (data,status)=>
            @scope.authorizationError = data
            @scope.loading = false
        null
  signIn: ()=>
    @.onSignIn?(@scope.authorization)
    null

  signOut: ()->
    @scope.authorization = null


angular.module('ReaderServices')
  .factory("readerLogin", ['$http', '$rootScope','readerStorage','authService', ($http, $rootScope,readerStorage,authService)->
    return new LoginService($rootScope,$http,readerStorage,authService)
  ])
