###
  Service for storing local data
###
class StorageService
  constructor: (@win,@scope) ->
    #todo: add fallback if no storage present
    @baseKey='reader:'
  get: (key, defaults)->
    if @win.localStorage?
      stored = @win.localStorage?.getItem("#{@baseKey}:#{key}")
      if stored? then angular.fromJson(stored) else defaults or null
  set: (key,value)->
    try
      if @win.localStorage? then @win.localStorage?.setItem("#{@baseKey}:#{key}", if value? then angular.toJson(value) else null)
    catch e
      if e == e.QUOTA_EXCEEDED_ERR
        @scope.$broadcast('event:quata-exceeded')

  remove: (key)->
    if @win.localStorage? then @win.localStorage?.removeItem("#{@baseKey}:#{key}")


angular.module('ReaderServices')
  .factory("readerStorage", ['$window','$rootScope', ($window,$rootScope)->
    return new StorageService($window,$rootScope)
  ])
