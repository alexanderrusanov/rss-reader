// Generated by CoffeeScript 1.6.3
(function() {
  var SettingsService;

  SettingsService = (function() {
    function SettingsService(scope, storage) {
      this.scope = scope;
      this.storage = storage;
    }

    SettingsService.prototype.getAndWatch = function(key, defaults) {
      var settings,
        _this = this;
      settings = this.storage.get("settings:" + key, defaults);
      this.scope.settings = _.extend(this.scope.$new(), settings);
      this.scope.settings.$watch(function(currentScope) {
        var scopeSettings;
        scopeSettings = _.pick(currentScope, _.keys(settings));
        if (!_.isEqual(scopeSettings, settings)) {
          _this.storage.set("settings:" + key, scopeSettings);
          return settings = scopeSettings;
        }
      });
      return this.scope.settings;
    };

    SettingsService.prototype.destroy = function() {
      return this.scope.settings.$destroy();
    };

    return SettingsService;

  })();

  angular.module('ReaderServices').factory("readerSettings", [
    '$rootScope', 'readerStorage', function($rootScope, readerStorage) {
      return new SettingsService($rootScope, readerStorage);
    }
  ]);

}).call(this);
