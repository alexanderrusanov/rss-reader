class SettingsService
  constructor: (@scope,@storage)->

  getAndWatch: (key,defaults)->
    settings = @storage.get("settings:#{key}", defaults)
    @scope.settings= _.extend(@scope.$new(),settings)
    @scope.settings.$watch(
      (currentScope)=>
        scopeSettings = _.pick(currentScope,_.keys(settings))
        if not _.isEqual(scopeSettings,settings)
          #TODO: Check fields if the are objects - check for diference
          @storage.set("settings:#{key}",scopeSettings)
          settings =scopeSettings
    )
    return @scope.settings

  destroy: ()->
    @scope.settings.$destroy()


angular.module('ReaderServices')
  .factory("readerSettings", ['$rootScope','readerStorage', ($rootScope, readerStorage)->
    return new SettingsService($rootScope,readerStorage)
  ])