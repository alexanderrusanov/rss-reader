path = require 'path'
fs = require 'fs'
request = require 'request'
crypto = require 'crypto'
rssService = require '../lib/rssService'
userService = require '../lib/userService'


exports.index = (req, res) ->
  ###
  Open index.html and stream to responce,
  since weare planning to make a single page offline app we don't need
  template engines
  ###
  res.sendfile path.join __dirname, '../views/index.html'


exports.authorize = (req, res) ->
  if req.body?.token?
    request 'http://ulogin.ru/token.php?token=' + req.body.token, (error, response, body) ->
      if !error and response.statusCode == 200
        profile = JSON.parse(body)
        authtoken = crypto.createHash('md5').update(profile.identity).digest("hex") #return md5 token
        userService.saveUser { id: authtoken, name: profile.first_name + ' ' + profile.last_name }
        res.json { authToken: authtoken }
      else
        res.json 400, { error: error }
  else
    res.json 400, { error: 'No authorization token' }


exports.feeds = (req, res) ->
  if !req.get('Authorization')?
    return res.send 401
  userService.getUser req.headers.authorization, (err, user) ->
    if err
      res.json 500, { error: err }
    else if !user?
      res.send 401
    else
      rssService.getFeeds req.headers.authorization, (err, feeds) ->
        if err
          res.json 500, { error: err }
        else
          dic = { }
          feeds.map (f) -> dic[f.id] = f
          res.jsonp dic


exports.addFeed = (req, res) ->
  if !req.get('Authorization')?
    return res.send 401
  rssService.discoveryFeeds req.param('url'), (err, feeds) ->
    if err
      res.json 500, { error: err }
    else if feeds.length == 0
      res.json 404, { error: "Not found" }
    else if feeds.length == 1
      rssService.saveFeed req.headers.authorization, feeds[0], (err, feed) ->
        if err
          res.json 500, { error: err.toString() }
        else
          res.json { subscribed: true, feedId: feed.id, feed: feeds[0] }
    else
      res.json { subscribed: false, multiple: true, feeds: feeds }


exports.unsubscribe = (req, res) ->
  if !req.get('Authorization')?
    return res.send 401
  if req.body?.feedId?
    rssService.removeFeed req.headers.authorization, req.body.feedId, (err) ->
      if err
        res.json 500, { error: err.toString() }
      else
        res.json 200
  else
    res.json 400


exports.tagFeed = (req, res) ->
  if !req.get('Authorization')?
    return res.send 401
  if req.body? and req.body.feedId? and req.body.tag?
    rssService.tagFeed req.headers.authorization, req.body.feedId, req.body.tag, (err) ->
      if err
        res.json 500, { error: err }
      else
        res.json 200
  else
    res.json 400


exports.feedItems = (req, res) ->
  if !req.get('Authorization')?
    return res.send 401
  params =
    user_id: req.headers.authorization
    from: req.param('from')
    to: req.param('to')
    unread: req.param('unread')
    order: req.param('order')

  rssService.getItems(req.param('id'), params, (err, feed, items) ->
      if err
        httpStatus = 500
        httpStatus = 501 if err.message == "Not Implemented"
        httpStatus = 404 if err.message == "Not Found"
        res.json httpStatus, { error: err }
      else
        res.jsonp { feed: feed, items: items }
  )


exports.readItem = (req, res) ->
  if !req.get('Authorization')?
    return res.send 401
  ids = []
  if req.body.length?
    ids = req.body
  else
    for own key, value of req.body
      if req.body[key].length?
        ids.push v for v in req.body[key]
  rssService.readItems ids, true, (err) ->
    if err
      res.json 500, { error: err.toString() }
    else
      res.json 200


exports.starItem = (req, res) ->
  if !req.get('Authorization')?
    return res.send 401
  if req.body? and req.body.feedId? and req.body.starredState?
    rssService.starItem req.body.feedId, req.body.starredState, (err) ->
      if err
        res.json 500, { error: err }
      else
        res.json 200
  else
    res.json 400
