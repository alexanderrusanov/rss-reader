path = require('path')
fs = require('fs')
request = require('request')
crypto = require('crypto')


exports.index = (req, res) ->
  ###
  Open index.html and stream to responce,
  since weare planning to make a single page offline app we don't need
  template engines
  ###
  res.sendfile(path.join(__dirname, '../views/index.html'))

###
Utility function to get random in range
###
getRandom = (min, max) ->
  return Math.floor(Math.random() * (max - min + 1)) + min;

###
Generate dummy feed content for testing client side
###
generateFeed = (name, tag = null, unreadCount = 0) ->
  unread: unreadCount
  title: name
  tag: tag
  link: 'http://google.ru'
  lastupdate:new Date()

exports.feeds = (req, res) ->
  #Dummy authorization
  if !req.get('Authorization')?
    return res.send(401)
  feeds = {}
  tags = [null, 'Tag1', 'Tag2', 'Tag3']
  for num in [getRandom(10, 100)..1]
    do (num)->
      feed = generateFeed("Rss #{num}", tags[getRandom(0, tags.length)..getRandom(0, tags.length)], getRandom(0, 2) * 100)
      feeds[getRandom(10, 100)]=feed
  ###
  Return generate feeds to client
  ###
  res.jsonp(feeds)
  #res.send(500)


exports.feedItems = (req, res) ->
  id = req.param('id')
  ###
  read sample file contents
  ###
  readFiles = (dirpath)->
    files = fs.readdirSync(dirpath)
    fs.readFileSync(path.join(dirpath,file),{encoding:"utf8"}) for file in files

  feedTexts = readFiles(path.join(__dirname, '../sample_feeds'))
  ###
  Generate dummy feed items content for testing client side
  ###
  generateFeedItem = (id,title, text, unread = true, starred=false) ->
    global.lastId=(global.lastId|0)+1
    item =
      id:global.lastId
      title: title
      text: text
      unread: unread
      starred: starred
      author: "Vasya"
      categories: ['news','it']
      created: new Date()
      link:'http://www.google.com'

  feedItems = []
  allLoaded = getRandom(0, 3);
  if allLoaded>0
    for num in [1..getRandom(1, 30)]
      do (num)->
        feedItems.push generateFeedItem(num,"Chanel: #{id} Feed #{num}",feedTexts[getRandom(0, feedTexts.length-1)], getRandom(0, 1) ==1, getRandom(0, 1) ==1)
  ###
  Return generate feeds to client
  ###
  feed = generateFeed('Some feed',['Some tag'],100);
  feed.id =id
  res.jsonp({feed:feed , items:feedItems})
  #res.send(500)


exports.authorize = (req,res)=>
  if req.body?.token?
    request('http://ulogin.ru/token.php?token=' + req.body.token,
      (error, response, body) ->
        if !error and response.statusCode == 200
          profile = JSON.parse(body)
          authtoken = crypto.createHash('md5').update(profile.identity).digest("hex") #return md5 token
          return res.json({authToken:authtoken})#return md5 token
        else
          return res.json(400,{error:error})
    )
  else
    return res.json(400,{error:'No authorization token'})

exports.addFeed = (req,res)->
  random = getRandom(0,10)
  urlToSubscribe = req.param('url')
  if random<5
    return res.json({subscribed:true,feedId:getRandom(0,10),feed:generateFeed('Some new feed name',['newlyadded'],10)})
  else
    return res.json({subscribed:false, multiple:true,feeds:[{url:urlToSubscribe,title:'Feed to subscribe'},{url:urlToSubscribe,title:'Feed 2 to subscribe'}]})



###
  if random<5
    return res.json({subscribed:true,feedId:getRandom(0,10),feed:generateFeed('Some new feed name',['newlyadded'],10)})
  else
    return res.json({subscribed:false, multiple:true,feeds:[{url:urlToSubscribe,title:'Feed to subscribe'},{url:urlToSubscribe,title:'Feed 2 to subscribe'}]})
###

exports.readFeed = (req, res)->
  return res.json 200

exports.unsubscribe = (req, res)->
  return res.json 200