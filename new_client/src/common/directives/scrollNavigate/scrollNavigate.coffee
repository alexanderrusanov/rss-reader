angular.module('scrollNavigate', []).directive("scrollNavigate",
  ['$window', ($window)-> return (scope, element, attributes)->
    hasClass = (elem, classname)->
      ((" " + elem.className + " ").replace(/[\n\t]/g, " ").indexOf(" " + classname + " ") > -1);

    findPos = (obj)->
      curtop = 0;
      if obj.offsetParent
        curtop += obj.offsetTop
        while obj = obj.offsetParent
          curtop += obj.offsetTop
      return curtop;


    attributes.$observe 'scrollNavigate', (jsonSettings)->
      settings = angular.fromJson(jsonSettings)
      if settings? and settings.eventName?
        scope.$on settings.eventName, ()->
          scope.$evalAsync ()->
            #find element with classes
            if settings.className
              childs = element.children()
              childIndex = 0
              while childIndex < childs.length
                elem = childs[childIndex]
                childIndex++
                #check classes
                if hasClass(elem, settings.className)
                  #eval
                  elementScope = angular.element(elem).scope();
                  if elementScope.$$phase
                    res = elementScope.$eval settings.evalute
                  else
                    res = elementScope.$apply settings.evalute
                  #scroll to it
                  if res
                    toScroll = findPos(elem)-findPos(element[0])
                    $window.scrollTo(0,toScroll)
                    break

  ])