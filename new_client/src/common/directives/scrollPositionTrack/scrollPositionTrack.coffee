angular.module('scrollPositionTrack', [])
  .directive("scrollPositionTrack",
    ['$window', '$document', ($window, $document)-> return (scope, element, attributes)->
      ###
      Feed selection by scroling
      ###
      attributes.$observe 'scrollPositionTrack', (jsonSettings)->
        settings = angular.fromJson(jsonSettings)

        isEnabled = true
        if attributes.scrollPositionTrack?
          isEnabled = scope.$eval attributes.scrollPositionTrack
          scope.$watch attributes.scrollPositionTrackDisabled, (value, oldValue)->
            isEnabled = !value
            if value != oldValue && isEnabled
              $selected = $("##{settings.id}#{scope[settings.scopeVar]}")
              scope.$evalAsync ()->
                offset = $selected.offset()
                if offset?
                  $('html, body').animate(
                    scrollTop: offset.top - 50
                  , 500)

        hasClass = (elem, classname)->
          ((" " + elem.className + " ").replace(/[\n\t]/g, " ").indexOf(" " + classname + " ") > -1);

        onScroll = ()->
          ## find a closest feed item
          if isEnabled
            #return if disabled
            scroll = $window.document.documentElement.scrollTop || $window.document.body.scrollTop
            childs = element.children()
            childIndex = 0
            while childIndex < childs.length
              elem = childs[childIndex]
              childIndex++
              if hasClass(elem, settings.className)
                if (elem.offsetTop + elem.clientHeight) > (scroll+$window.document.documentElement.clientHeight/4)
                  elementScope = angular.element(elem).scope();
                  if elementScope.$$phase
                    elementScope.$eval settings.evalute
                  else
                    elementScope.$apply settings.evalute
                  break
          return false

        throttled = _.throttle(onScroll, 300)
        scrollFn = $window.onscroll
        $window.onscroll = ()->
          scrollFn?.apply?(@, arguments)
          throttled()

        #ondestroy remove handler
        scope.$on('$destroy', ()->
          $window.onscroll = scrollFn)

        onScroll()
    ])