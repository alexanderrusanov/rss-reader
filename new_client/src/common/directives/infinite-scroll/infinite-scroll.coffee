mod = angular.module('infinite-scroll', [])

mod.directive 'infiniteScroll', ['$rootScope', '$window', '$timeout', ($rootScope, $window, $timeout) ->
  link: (scope, elem, attrs) ->
    element = elem[0]
    # infinite-scroll-distance specifies how close to the bottom of the page
    # the window is allowed to be before we trigger a new scroll. The value
    # provided is multiplied by the window height; for example, to load
    # more when the bottom of the page is less than 3 window heights away,
    # specify a value of 3. Defaults to 0.
    scrollDistance = 0
    if attrs.infiniteScrollDistance?
      scope.$watch attrs.infiniteScrollDistance, (value) ->
        scrollDistance = parseInt(value, 10)

    if attrs.infiniteScrollWatch?
      scope.$watch attrs.infiniteScrollWatch, (value) ->
        handler()

    # infinite-scroll-disabled specifies a boolean that will keep the
    # infnite scroll function from being called; this is useful for
    # debouncing or throttling the function call. If an infinite
    # scroll is triggered but this value evaluates to true, then
    # once it switches back to false the infinite scroll function
    # will be triggered again.
    scrollEnabled = true
    checkWhenEnabled = false
    listenNextDom = true

    if attrs.infiniteScrollDisabled?
      scope.$watch attrs.infiniteScrollDisabled, (value) ->
        scrollEnabled = !value
        if scrollEnabled && checkWhenEnabled
          checkWhenEnabled = false
          handler()

    # throttle function so scroll event don't hit
    # handler every time it's emmitted
    throttle = (fn, delay) ->
      return fn if delay is 0
      timer = false
      return ->
        return if timer
        timer = true
        $timeout (-> timer = false), delay unless delay is -1
        scope.$evalAsync(-> fn arguments...)


    getContainerBottom = ()->
      if attrs.infiniteScrollContainer?
        el = $window.document.getElementById(attrs.infiniteScrollContainer)
        el.clientHeight + el.scrollTop
      else
        $window.document.documentElement.clientHeight + ($window.scrollY || $window.document.documentElement.scrollTop || $window.document.body.scrollTop)

    getContainer = ()->
      if attrs.infiniteScrollContainer?
        $window.document.getElementById(attrs.infiniteScrollContainer)
      else
        $window.document.documentElement

    getScrollContainer = ()->
      if attrs.infiniteScrollContainer?
        $window.document.getElementById(attrs.infiniteScrollContainer)
      else
        $window

    # infinite-scroll specifies a function to call when the window
    # is scrolled within a certain range from the bottom of the
    # document. It is recommended to use infinite-scroll-disabled
    # with a boolean that is set to true when the function is
    # called in order to throttle the function call.
    handler = ()->
        listenNextDom = false
        windowBottom = getContainerBottom()
        elementBottom = element.offsetTop + element.clientHeight
        remaining = elementBottom - windowBottom
        shouldScroll = remaining <= getContainer().clientHeight * scrollDistance
        if shouldScroll && scrollEnabled
          if $rootScope.$$phase
            scope.$eval attrs.infiniteScroll
          else
            scope.$apply attrs.infiniteScroll
          listenNextDom = true
        else if shouldScroll
          checkWhenEnabled = true


    throttled = _.debounce handler, 300
    domThrottled = _.debounce ()->
      if getContainer().clientHeight > element.clientHeight
        #element wider than window
        listenNextDom = false
        handler()
    , 100

    containerElement = getScrollContainer()
    scrollFn = containerElement.onscroll
    containerElement.onscroll = ()->
      scrollFn?.apply?(@, arguments)
      throttled()

    element.addEventListener?('DOMNodeInserted',domThrottled)

    scope.$on '$destroy', ->
      containerElement.onscroll = scrollFn
      element.removeEventListener?('DOMNodeInserted',domThrottled)

    $timeout (->
      if attrs.infiniteScrollImmediateCheck
        if scope.$eval(attrs.infiniteScrollImmediateCheck)
          handler()
      else
        handler()
    ), 0
]
