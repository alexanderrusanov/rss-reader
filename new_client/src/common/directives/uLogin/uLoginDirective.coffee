angular.module('uLogin', [])
  .directive 'uLogin', ($window,$document)->
    link: (scope, element, attrs)->
      uLoginInit = attrs.uLogin
      #get id
      uLoginId = new Date().getTime()
      uLoginCallback =  "#{uLoginId}_loginCallback"
      uLoginData = uLoginInit.substr(uLoginInit.indexOf(';')+1)+"callback=#{uLoginCallback}"
      uloginReady = ()->
        if uLogin?
          element.attr('id',uLoginId).attr('data-ulogin',uLoginData).removeClass('loading')
          uLogin.customInit uLoginId
          #add global function
          $window[uLoginCallback]=(token)->
            scope.$emit(events.login.token, token)

      if not uLogin?
        #Load ulogin script
        script=$document[0].createElement('script')
        script.setAttribute("type","text/javascript")
        script.setAttribute("src", '//ulogin.ru/js/ulogin.js')
        $document[0].getElementsByTagName("head")[0].appendChild(script)
        script.onload = uloginReady
        script.onreadystatechange = ()->
          if  @.readyState == 'complete' then uloginReady()
      else
        uloginReady()
      null