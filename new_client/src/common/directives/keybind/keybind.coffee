mod = angular.module('$keybind', [])

mod.factory "$keybind", ['$window', '$timeout', ($window, $timeout)->
  ###
  Keycodes dictionaries
  ###
  _modifier_event_mapping =
    "cmd": "metaKey"
    "ctrl": "ctrlKey"
    "shift": "shiftKey"
    "alt": "altKey"

  _keycode_dictionary =
    0: "\\"          # Firefox reports this keyCode when shift is held
    8: "backspace"
    9: "tab"
    12: "num"
    13: "enter"
    16: "shift"
    17: "ctrl"
    18: "alt"
    19: "pause"
    20: "caps"
    27: "escape"
    32: "space"
    33: "pageup"
    34: "pagedown"
    35: "end"
    36: "home"
    37: "left"
    38: "up"
    39: "right"
    40: "down"
    44: "print"
    45: "insert"
    46: "delete"
    48: "0"
    49: "1"
    50: "2"
    51: "3"
    52: "4"
    53: "5"
    54: "6"
    55: "7"
    56: "8"
    57: "9"
    65: "a"
    66: "b"
    67: "c"
    68: "d"
    69: "e"
    70: "f"
    71: "g"
    72: "h"
    73: "i"
    74: "j"
    75: "k"
    76: "l"
    77: "m"
    78: "n"
    79: "o"
    80: "p"
    81: "q"
    82: "r"
    83: "s"
    84: "t"
    85: "u"
    86: "v"
    87: "w"
    88: "x"
    89: "y"
    90: "z"
    91: "cmd"
    92: "cmd"
    93: "cmd"
    96: "num_0"
    97: "num_1"
    98: "num_2"
    99: "num_3"
    100: "num_4"
    101: "num_5"
    102: "num_6"
    103: "num_7"
    104: "num_8"
    105: "num_9"
    106: "num_multiply"
    107: "num_add"
    108: "num_enter"
    109: "num_subtract"
    110: "num_decimal"
    111: "num_divide"
    124: "print"
    144: "num"
    145: "scroll"
    186: ";"
    187: "="
    188: ","
    189: "-"
    190: "."
    191: "/"
    192: "`"
    219: "["
    220: "\\"
    221: "]"
    222: "\'"
    223: "`"
    224: "cmd"
  # Opera weirdness
    57392: "ctrl"
    63289: "num"

  _prevent_default = (e) ->
    if e.preventDefault then e.preventDefault() else e.returnValue = false
    e.stopPropagation() if e.stopPropagation

  pressedKeys = []
  attachHandler = (target, event, handler) ->
    if target.addEventListener
      target.addEventListener event, handler, false
    else if target.attachEvent
      target.attachEvent "on#{event}", handler

  attachHandler $window.document.body, "keydown", (e) ->
    if e.target? and e.target != $window.document.body
      return
    _key = _keycode_dictionary[e.keyCode]
    e = e or $window.event
    if _modifier_event_mapping.hasOwnProperty(_key)
      return

    pressedKeys.push _key
    $timeout ->
      ind = pressedKeys.indexOf(_key)
      pressedKeys.splice(ind, 1)
    , 1000, false
    #push modifiers
    mods = {}
    for mod, event_mod of _modifier_event_mapping
      continue unless e[event_mod]
      continue if mod is _key or mod in pressedKeys
      mods[mod] = true
    #Check for pressed keys
    shouldPrevent = 0
    matched = for own command, binding of keybind.__commands
      #check pressed keys
      match = true
      for mod in binding.mods when match
        match = mods.hasOwnProperty(mod) and mods[mod]

      if match
        shouldPrevent = 1
        matchCount = 0
        maxMatch = 0
        for keyIndex in [0..pressedKeys.length-1]
          if pressedKeys[keyIndex] == binding.keys[matchCount]
            matchCount++
          else
            matchCount = 0
          if matchCount == binding.keys.length
            break
          if maxMatch < matchCount then maxMatch = matchCount

        if maxMatch > 0
          shouldPrevent += maxMatch
        match = matchCount == binding.keys.length

      if shouldPrevent>2
        _prevent_default e

      if match
        _prevent_default e
        binding
      else
        continue

    for match in matched when match?
      match.callback?()


  attachHandler $window.document.body, "keyup", (e) ->
    e = e or $window.event
    _key = _keycode_dictionary[e.keyCode]
    if _modifier_event_mapping.hasOwnProperty(_key)
      return
    $timeout ->
      ind = pressedKeys.indexOf(_key)
      pressedKeys.splice(ind, 1)
    , 1000, false

  attachHandler $window, "blur", ->
    #Clear keys
    pressedKeys = []

  keybind = {}
  keybind.__commands = {}
  keybind.setCommand = (commandName, keybinding, callback)->
    #parse keybinding
    if typeof keybinding == 'function'
      callback = keybinding
      #find keybinding
      if not keybind.__commands.hasOwnProperty(commandName)
        keybind.__commands[commandName] = {mods: [], keys: [], callback: callback}
      else
        keybind.__commands[commandName].callback = callback
    else
      if not keybind.__commands.hasOwnProperty(commandName)
        binding = {mods: [], keys: [], callback: callback}
      else
        binding = keybind.__commands[commandName]
        binding.mods = []
        binding.keys = []
      keys = keybinding.split(/\+|,/i)
      for key in keys
        if _modifier_event_mapping.hasOwnProperty(key)
          binding.mods.push key
        else
          binding.keys.push key
      keybind.__commands[commandName] = binding

  keybind.unsetCommand = (commandName)->
    delete keybind.__commands[commandName]
  keybind
]

mod.directive 'ngKeybind', ['$window', '$keybind', ($window, $keybind) ->
  restrict: 'AC'
  terminal: true
  link: (scope, elem, attrs)->
    #set test
    callback = (params)->
      if params? and params.eval?
        scope.eval(params.eval)
      else
        elem.triggerHandler('click')

    param = (paramToParse)->
      match = paramToParse.match(/^(.+)(?:\sdo\s(.+))$/i)
      if match? and match.length is 3
        key: match[1]
        eval: match[2]
      else
        key: paramToParse

    parsedParams = param(attrs.ngKeybind)
    $keybind.setCommand(parsedParams.key, ->
      callback(parsedParams)) if parsedParams?

    scope.$on '$destroy', ->
      $keybind.unsetCommand(parsedParams.key) if scope.params?
]


