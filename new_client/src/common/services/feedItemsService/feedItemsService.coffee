class FeedItemsService
  constructor: (@scope, @dataService,@sanitize) ->
    @feedItems = []
    @openedItems = {}
    @currentFeedId = null
    @currentFeed = null
    @allItemsLoaded = false

    @itemsLoaded = (feedInfo, callback)->
      items = feedInfo.items
      regex = /(<([^>]+)>)/ig;  #regex for stripping HTML tags

      @currentFeed = feedInfo.feed
      if items?
        #process items before push
        for item in items
          do (item)=>
            item.text = @preprocessHtml(item.text)
            item.preview = item.text.replace(regex, "").replace(/\s+/,"").split(" ",100).join(' ')+'...'

        #merge data
        combinedData = _.chain((@feedItems || []).concat(items)).unique((item)->item.id).sortBy("created").value()
        if @scope.settings.order=='newest'
          combinedData = combinedData.reverse()
        if @feedItems?
          size = @feedItems.length
        @feedItems = combinedData
        if  @feedItems.length == size
          #no feeds was retrieved
          @allItemsLoaded = true
        #Call a callback
        callback?(@feedItems)

    @preprocessHtml = (html)->
      html = html.replace(/<(a)([^>]+)>/ig,'<$1 target="_blank"$2>')
      ##TODO: Add image lazyload
      return html

  reloadFeedItems: (feedId, callback)=>
    @feedItems = []
    @openedItems = {}
    @currentFeedId = null
    @allItemsLoaded = false

    @loadFeedItems(feedId, callback)

  loadFeedItems: (feedId, callback)=>
    from = null
    if feedId != @currentFeedId
      @feedItems = []
      @currentFeedId = feedId
      @allItemsLoaded = false
    else if @feedItems? and @feedItems.length > 0
      from = @feedItems.length

    if !@allItemsLoaded
      @dataService.getFeedItems(@currentFeedId,
      (data)=>
        @itemsLoaded(data,callback)
      ,from)

  getFeed: ()->
    @currentFeed

  getFeedTitle: ()->
    if @currentFeed? and @currentFeed.id?
      if /tag:.*/i.test @currentFeed.id
        @currentFeed.id.substr('tag:'.length)
      else
        @currentFeed.title


  isAllItemsLoaded: ()->
    @allItemsLoaded

  isRealFeed: (feedId) -> not /all|starred|tag:.*/i.test feedId

  getFeedItems: ()->
    @feedItems

  feedsCount: ()->
    if @feedItems? then @feedItems.length else 0

  getFeedIndex: (feed)->
    if @feedItems? then _.indexOf(@feedItems, feed) else -1

  getByIndex: (index)->
    if @feedItems? and @feedItems.length>0 and @feedItems.length > index and index >=0 then @feedItems[index]
    else if index < 0 then @feedItems[0]
    else  @feedItems[@feedItems.length-1]

  wasOpened: (feedItemId)->
    @openedItems.hasOwnProperty(feedItemId)

  getFeedItem: (itemId)->
    _.find(@feedItems,(item)->item.id==itemId)

  toggleStarred: (feedItem)->
    if feedItem? and _.isObject(feedItem)
      feedItem.starred = !feedItem.starred
      @dataService.toggleStarred(feedItem.id, feedItem.starred)


  markAsReaded: (feedItem)->
    if @feedItems? and @currentFeed?
      unreadDelta = 0
      if feedItem?
        if _.isObject(feedItem)
          item = feedItem
        else
          item = _.find(@feedItems, (item)->item.id==feedItem)
        if item? and item.unread
          @dataService.markReaded(item.id, @currentFeed.id)
          @openedItems[item.id] = true
          item.unread = false
          unreadDelta = 1
      else
        items = []
        for item in @feedItems when item.unread
          do (item)=>
            item.unread = false
            unreadDelta+=1
            items.push item.id
            @openedItems[item.id] = true
            null
        @dataService.markReaded(items, @currentFeed.id)
      @currentFeed.unread-=unreadDelta
      @scope.$broadcast(events.feed.unreadDelta,@currentFeedId, unreadDelta)


angular.module('feedItemsService',['dataService'])
  .factory("feedItemsService", ['$rootScope','dataService','$sanitize', ($rootScope, dataService,$sanitize)->
    return new FeedItemsService($rootScope, dataService,$sanitize)
  ])