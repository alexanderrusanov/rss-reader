class FeedService
  constructor: (@dataService) ->
    @feeds = {}
    @feedsByTags = {}
    @__populateFeedsByTags = ()->
      delete @.feedsByTags[key] for own key,item of @.feedsByTags
      for own key,item of @.feeds
        do (item)=>
          item.id = key
          if item.tag?
            if item.tag.length == 0
              #add to empty
              if @.feedsByTags.hasOwnProperty(null) then @.feedsByTags[null].push item else @.feedsByTags[null] = [item]
            else
              for tag in item.tag
                do (tag)=>
                  if @.feedsByTags.hasOwnProperty(tag) then @.feedsByTags[tag].push item else @.feedsByTags[tag] = [item]
                  null
          null

  loadFeeds: (callback)->
    # Query service
    @.dataService.getFeeds (data)=>
      @.feeds=_.extend(@.feeds, data)#merge data
      #Make feeds by tag
      @.__populateFeedsByTags()
      callback(@.feeds)

  getFeeds: ()-> @.feeds[id]
  getFeed: (id)-> @.feeds[id]
  getFeedsByTag: ()-> @.feedsByTags;

  unsubscribeFromFeed: (feed)->
    if feed? and feed.id?
      #Remove from
      delete @.feeds[feed.id]
      @.__populateFeedsByTags()
      #send request to unsubscribe
      @.dataService.unsubscribe(feed.id)

  subscribeToFeed: (feed)->
    if feed? and  feed.id?
      #add to
      @.feeds[feed.id]= feed
      @.__populateFeedsByTags()
      #send request to subscribe
      @.dataService.subscribe(feed.id)

  toggleTag: (feedId, tagname)->
    feed = @getFeed(feedId)
    if feed?
      feed.tag = feed.tag || []
      if _.indexOf(feed.tag,tagname)!=-1
        #remove
        feed.tag = _.without(feed.tag,tagname)
      else
        feed.tag = _.without(feed.tag,null)
        feed.tag.push tagname
      feed.tag = _.uniq(feed.tag)
      @__populateFeedsByTags()
      @dataService.toggleTag(feedId, tagname)

  isSubcribed: (feedId)->
    @feeds.hasOwnProperty(feedId)

  discoverFeeds:(url,callback)->
    @dataService.discoverFeeds url, callback

  pushNew:(feedArray)->
    if feedArray? and _.isArray(feedArray)
      #start merging
      for newFeed in feedArray
        do (newFeed)=>
          if newFeed? and newFeed.id?
            @feeds[newFeed.id] = newFeed
          null
      @__populateFeedsByTags()

angular.module('feedService',['dataService'])
  .factory("feedService", ['dataService', (dataService)->
    return new FeedService(dataService)
  ])