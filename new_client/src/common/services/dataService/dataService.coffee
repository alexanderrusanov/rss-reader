class DataService
  constructor: (@scope, @http, @store) ->
    @scope.loading = false
    @scope.error = null
    @isItemsLoading = false
    @urls =
      feedUrl: "/feeds"
      feedItemsBase: "/feed/"
      feedReaded: "/feed/read"
      toggleTag: "/toggle_tag"
      toggleStarred: "/toggle_starred"
      unsubscribe: "/unsubscribe"
      subscribe: "/subscribe"
      discover: "/add_feed?url="
    @readed = []

    @__pushReaded = _.debounce(()=>
      #send readed ids to server
      if @readed? and @readed.length > 0
        localReaded = _.uniq(_.flatten(@readed))
        @readed = []
        @http.post(@.urls.feedReaded, localReaded)
    , 1000)

    @scope.$on 'event:quata-exceeded', ()->
      #delete old
      stored = @store.get("feedSaved", {})
      min = new Date()
      minId = null
      for own id,date of stored
        do (id, date)->
          if min > date
            min = date
            minId = id
      if minId?
        delete stored[minId]
        @store.remove "feed#{minId}"
        @store.set("feedSaved", stored)

  toggleTag: (feedId, tagname, callback)->
    @.performRequest('post', @.urls.toggleTag, {feedId: feedId, tag: tagname}, callback)

  toggleStarred: (feedId, starredState, callback)->
    @.performRequest('post', @.urls.toggleStarred, {feedId: feedId, starredState: starredState}, callback)

  subscribe: (feedId, callback)->
    @.performRequest('post', @.urls.subscribe, {feedId: feedId}, callback)
  unsubscribe: (feedId, callback)->
    @.performRequest('post', @.urls.unsubscribe, {feedId: feedId}, callback)

  getFeeds: (callback)->
    @.performGetRequest @.urls.feedUrl,
    (data)=>
      #store localy
      @store.set("feeds", data)
      callback(data)
    , (data, status) =>
      #get local stored
      storedData = @store.get("feeds", {})
      if storedData? then callback(storedData);true else false

  getFeedItems: (id, callback, from = null)->
    if @isItemsLoading then return
    @isItemsLoading = true
    params = {}
    #set unread if it's enabled in settings
    if @scope.settings.showUnreadItems then params.unread = true
    #set order
    params.order = @scope.settings.order

    if from? then params.from = from

    query = ("#{key}=#{value}" for own key,value of params).join('&')
    @.performGetRequest ("#{@.urls.feedItemsBase}#{id}" + if query != '' then "?#{query}" else ''),
    (data)=>
      item.created = new Date(item.created) for item in data.items
      #store localy
      if from?
        #if from specified the append items
        storedData = @store.get("feed#{id}", [])
        storedData.items = storedData.items || []
        storedData.items.push item for item in data.items
        @.setOfflineItems(id, storedData)
      else
        @.setOfflineItems(id, data)
      callback(data)
      @isItemsLoading = false
    , (data, status) =>
      @isItemsLoading = false
      #get local stored
      storedData = @store.get("feed#{id}", [])
      #todo: check from id
      if from? and _.isDate(from)
        storedData.items = _.filter(storedData.items || [], (item)-> return item.created > from)
      if storedData? and storedData.items? and storedData.items.length > 0 then callback(storedData);true else false

  setOfflineItems: (id, items)=>
    stored = @store.get("feedSaved", {})
    stored[id] = new Date()
    @store.set("feedSaved", stored)
    @store.set("feed#{id}", _.unique(_.sortBy(items, (item)->
      item.created), true, (item)->
      item.id))

  markReaded: (ids)->
    @readed.push [ids]
    @__pushReaded()

  discoverFeeds: (url, callback)->
    @performGetRequest "#{@urls.discover}#{encodeURIComponent(url)}", (data)=>
      if data? and data.subscribed and data.feed? and data.feedId?
        callback({subscribed: true, feed: data.feed, feedId: data.feedId})
      else if data? and not data.subscribed and data.multiple
        callback({subscribed: false, feeds: data.feeds})

  performRequest: (type, url, data, callback, error)->
    @scope.loading = true
    @http[type](url, data)
      .success (data)=>
        @scope.loading = false
        @scope.error = null
        #got items from server
        if data?
          callback(data)
      .error (data, status)=>
        #TODO: Add cache loading
        @scope.loading = false
        if !error?(data, status) then @scope.error = "##{status} Network error occured" else @scope.error = null

  performGetRequest: (url, callback, error)->
    @.performRequest('get', url, null, callback, error)

angular.module('dataService', ['storageService'])
  .factory("dataService", ['$http', '$rootScope', 'storageService', ($http, $rootScope, storageService)->
    return new DataService($rootScope, $http, storageService)
  ])
