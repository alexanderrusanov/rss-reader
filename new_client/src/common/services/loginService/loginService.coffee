###
Login service
###
class LoginService
  constructor: (@scope,@http,@store,@authService) ->
    @scope.auth = {}

    @scope.$watch 'auth.authorization', (newValue,oldValue)=>
      #set auth header if it's stored
      if newValue? and oldValue!=newValue
        @store.set("auth",newValue)
        @http.defaults.headers.common['Authorization']=newValue
        @.signIn()
      else if !newValue?
        delete @http.defaults.headers.common.Authorization
        @store.set("auth",null)

    @scope.auth.authorization = @store.get("auth",null)
    #set immideatly
    if @scope.auth.authorization
      @http.defaults.headers.common['Authorization']=@scope.auth.authorization
      @.signIn()

    @scope.auth.isAuthorized = ()=>
      @isAuthorized()

    @scope.$on 'event:auth-loginConfirmed', (event,data)=>
      #login confirmed store token
      if data?
        @scope.auth.authorization = data

    @scope.$on 'event:auth-loginRequired', (event,data)=>
      #login failed
      @.signOut()

    #on token from ulogin
    @scope.$on events.login.token, (event,token)=>
      #login confirmed store token
      if token?
        @scope.auth.loading = true
        @http.post("/authorize",{token:token})
          .success (data)=>
            @scope.auth.loading = false
            if data? and data.authToken?
              @authService.loginConfirmed(data.authToken,{headers:{'Authorization':data.authToken}})
            else if data.error?
              @scope.auth.authorizationError = data.error
          .error (data,status)=>
            @scope.auth.authorizationError = data
        null
  signIn: ()=>
    @scope.$broadcast(events.login.signedIn)
    null

  signOut: ()->
    @scope.auth.authorization = null
    @scope.$broadcast(events.login.signedOut)

  isAuthorized: ()->
    @scope.auth.authorization?



angular.module('loginService',['storageService','authService'])
  .factory("loginService", ['$http', '$rootScope','storageService','authService', ($http, $rootScope,storageService,authService)->
    return new LoginService($rootScope,$http,storageService,authService)
  ])
