class SettingsService
  constructor: (@scope,@storage)->

  getAndWatch: (key,defaults)->
    if !@scope.settings? and (defaults || null)?
      settings = @storage.get("settings:#{key}", defaults)
      @scope.settings= _.extend(@scope.$new(), _.defaults(settings,defaults))
      @scope.settings.$watch(
        (currentScope)=>
          scopeSettings = _.pick(currentScope,_.keys(settings))
          @storage.set("settings:#{key}",scopeSettings)
      )
    return @scope.settings || defaults || null

  destroy: ()->
    @scope.settings.$destroy()


angular.module('settingsService',['storageService'])
  .factory("settingsService", ['$rootScope','storageService', ($rootScope, storage)->
    return new SettingsService($rootScope,storage)
  ])