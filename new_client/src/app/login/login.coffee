loginModule = angular.module 'reader.login', [
  'ui.state',
  'titleService',
]

loginModule.config ($stateProvider)->
  $stateProvider.state 'login',
    url: '/login',
    views:
      main:
        controller: 'LoginCtrl',
        templateUrl: 'login/login.tpl.html'


loginModule.controller 'LoginCtrl', ($scope,$location, titleService)->
  titleService.setTitle('Login')
  $scope.$on 'event:auth-loginConfirmed', (evt,arg)->
    #Redirect
    $location.url('/')
