feedsModule = angular.module 'reader.feeds', [
  'ui.state',
  'titleService',
  'feedService',
  'settingsService'
]

feedsModule.config ($stateProvider)->
  $stateProvider
    .state 'feeds',
      url: '/feeds',
      views:
        main:
          controller: 'FeedsCtrl',
          templateUrl: 'feeds/feeds.tpl.html'
    .state 'feeds.settings',
      url: '^/settings',
      views:
        feedItems:
          controller: 'SettingsCtrl',
          templateUrl: 'settings/settings.tpl.html'
    .state 'feeds.items',
      url: '/:feedId?mode',
      views:
        data:
          viewMode: 'list',
        feedItems:
          controller: 'FeedItemsCtrl',
          templateUrl: 'feedItems/feedItems.tpl.html'
        feedList:
          controller: 'FeedListCtrl',
          templateProvider: (['$templateCache', 'settingsService' ,
            ($templateCache, settingsService)->
              settings = settingsService.getAndWatch('default')
              if settings?
                $templateCache.get("feedList/feed-#{settings.viewMode||'list'}.tpl.html")
          ])



feedsModule.controller 'FeedsCtrl', ($scope,$dialog, $state,$timeout, settingsService, titleService, feedService)->
  titleService.setTitle('Feeds')

  $scope.feeds = {}
  $scope.feedItems = {}
  #declared in root controller

  $scope.$on events.feed.unreadDelta, (evt, feedId, delta)->
    feedService.getFeed(feedId)?.unread -= delta

  $scope.toggleTag = (tag)->
    $scope.settings.tagState[tag] = !$scope.isTagOpened(tag)

  $scope.isTagOpened = (tag)->
    if $scope.settings.tagState?.hasOwnProperty(tag)
      $scope.settings.tagState[tag]
    else
      true

  $scope.isCurrentInTag = (tag)->
    if $scope.feedItems.selectedId == 'tag:' + tag
       true
    item = feedService.getFeed($scope.feedItems.selectedId)
    item? and item.tag? and tag in item.tag

  $scope.allTags = ()->
    if $scope.feeds? and $scope.feeds.channels? then _.keys($scope.feeds.channels) else []

  $scope.unreadCountByTag = (tag)->
    unread = 0
    unread = unread + item.unread for item in $scope.feeds.channels[tag]
    unread

  #Load feeds initialy
  $scope.refresh = ()->
    feedService.loadFeeds ()->
      $scope.feeds.channels = feedService.getFeedsByTag()
      $timeout $scope.refresh, 60000 #create an update timeout

  $scope.$on events.feed.tagsChanged, (evt)->
    #Update tags
    $scope.feeds.channels = feedService.getFeedsByTag()

  $scope.addFeed = ()->
    $dialog.dialog(
      keyboard: true,
      templateUrl: "feeds/addFeed.tpl.html",
      backdropClick: true,
      dialogFade: true,
      controller: 'AddFeedCtrl'
    ).open().then (feeds)->
      if feeds? and _.isArray(feeds)
        feedService.pushNew(feeds)
        #Get new items
        $scope.feeds.channels = feedService.getFeedsByTag()
        $state.transitionTo("feeds.items",{feedId:feeds[feeds.length-1].id})

  $scope.refresh()

#initial refresh


feedsModule.controller 'AddFeedCtrl', ['$scope', 'dialog','feedService', ($scope, dialog,feedService)->
  $scope.discovery = {}

  queryDiscovery=(url,callback)->
      #make query to discovery
    feedService.discoverFeeds url, (result)->
      if result? and result.subscribed and result.feedId
        result.feed.id =result.feedId
        callback(result.feed)
      else if result? and not result.subscribed and result.feeds?
        #multiple feeds
        $scope.discovery.feeds = result.feeds

  $scope.discoverFeed = ()->
    if $scope.discovery.feeds?
      #query service with selected
      discoveredItems =[]
      itemCounter = 0
      for item in $scope.discovery.feeds when item.checked
        do (item)->
          itemCounter++
          queryDiscovery $scope.discovery.feedUrl,(discoveredItem)->
            discoveredItems.push discoveredItem
            itemCounter--
            if itemCounter == 0
              dialog.close(discoveredItems)
      null
    else
      queryDiscovery $scope.discovery.feedUrl,(item)->
        dialog.close([item])



  $scope.close = (result)->
    dialog.close(result)
]