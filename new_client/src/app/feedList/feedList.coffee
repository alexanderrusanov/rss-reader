angular.module('reader.feedList', ['feedService', 'settingsService', 'feedItemsService'])
  .controller 'FeedListCtrl', ($scope, $stateParams, titleService, feedService, feedItemsService)->

    $scope.feedItems.selectItem = (item)->
      if $scope.feedItems.selectedFeed != item
        $scope.feedItems.selectedFeed = item
        if item.unread
          feedItemsService.markAsReaded(item)

    $scope.feedItems.expandItem = (item)->
      if $scope.feedItems.selectedFeed == item
        $scope.feedItems.selectedFeed = null
      else
        $scope.feedItems.selectItem(item)

    $scope.feedItems.scrollToItem = (item)->
      #also mark previous
      $scope.feedItems.selectItem(item)
      index = feedItemsService.getFeedIndex(item)
      while index-- > 0
        do ->
          feedItemsService.markAsReaded(feedItemsService.getByIndex(index))
      null



    $scope.feedItems.shouldShowItem = (item)->
      if $scope.settings.showUnreadItems and item.unread or feedItemsService.wasOpened(item.id)
        true
      else if !$scope.settings.showUnreadItems
        true
      else
        false

    $scope.feedItems.selectByDelta = (delta)->
      #Get current index
      item = $scope.feedItems.selectedFeed
      if item?
        #get index
        index = feedItemsService.getFeedIndex($scope.feedItems.selectedFeed)
        while index>=0 and index< feedItemsService.feedsCount()
          index+=delta
          item = feedItemsService.getByIndex(index)
          if item? and $scope.feedItems.shouldShowItem(item) then break
        if item? and item!=$scope.feedItems.selectedFeed
          #set as selected
          if $scope.feedItems.selectedFeed?
            feedItemsService.markAsReaded($scope.feedItems.selectedFeed)
          $scope.feedItems.selectedFeed = item
      else
        item = feedItemsService.getByIndex(0) #just get first
        $scope.feedItems.selectedFeed = item
      #TODO: trigger select event
      $scope.$broadcast('event:feedSelected')

    $scope.feedItems.getItems = ()->
      item for item in feedItemsService.getFeedItems() when $scope.feedItems.shouldShowItem(item)

    $scope.feedItems.loadNextItems = ()->
      feedItemsService.loadFeedItems $stateParams.feedId, (items)->
        $scope.feedItems.items = items

    $scope.feedItems.isAllItemsLoaded = ()->
      feedItemsService.isAllItemsLoaded()

    $scope.feedItems.markAsReaded = (id)->
      feedItemsService.markAsReaded(id)

    $scope.feedItems.toggleStarred = (item)->
      feedItemsService.toggleStarred(item)

    #Select first item on load
    if $scope.feedItems.selectedFeed?
      $scope.feedItems.selectByDelta(1)
    else
      $scope.$broadcast('event:feedSelected')#just fire event on view change