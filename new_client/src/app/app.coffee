appModule = angular.module 'reader', [
  'templates-app',
  'templates-common',
  'ui.bootstrap',
  'toggle-switch',

  ##App controllers
  'reader.login',
  'reader.feeds',
  'reader.feedItems',
  'reader.feedList',
  'reader.dialog',
  'reader.settings',

  ##Ui helpers
  'ui.state',
  'ui.route',

  ##Directives
  'uLogin',
  'angularMoment',
  'ngSanitize',
  'infinite-scroll',
  'scrollPositionTrack',
  'scrollNavigate',
  '$keybind',

  ##Services
  'storageService',
  'authService',
  'loginService',
  'settingsService',


  #filters
  'reader.filters'
]

appModule.config ($stateProvider, $urlRouterProvider)->
  $urlRouterProvider.otherwise('/feeds')

appModule.run (titleService)->
  titleService.setSuffix(' | reader')

appModule.controller 'AppCtrl',( $scope, $location, $keybind, loginService,settingsService)->
  $scope.settings = settingsService.getAndWatch('default',
    defaults=
      tagState:{}
      showUnread:true
      showUnreadItems:true
      showFavicons:true
      viewMode:'list',
      order:'newest'
  )

  #Default keybinds
  $keybind.setCommand('nextFeedItem','space')

  if not loginService.isAuthorized()
    $location.url('/login')
  $scope.$on '$stateChangeStart',(event, toState, toParams, fromState, fromParams)->
    #If not authorized - prevent from all transitions
    if not loginService.isAuthorized() and toState.name != 'login'
      event.preventDefault();

  ##Redirect to login if signed out
  $scope.$on events.login.signedOut , (evt)->
    $location.url('/login')

