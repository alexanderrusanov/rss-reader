feedItemsModule=angular.module('reader.feedItems',['feedService','feedItemsService','settingsService','reader.dialog'])

feedItemsModule.controller 'FeedItemsCtrl', ($scope,$stateParams,$state,$dialog, titleService, feedService, feedItemsService)->
    if $scope.feedItems.selectedId != $stateParams.feedId
      $scope.feedItems.selectedFeed = null
      $scope.feedItems.items = []
      #we are loading new items
      feedItemsService.loadFeedItems $stateParams.feedId, (items)->
        $scope.feedItems.items = items
        titleService.setTitle($scope.feedItems.feedTitle())

    #Access parent scope feeds
    $scope.feedItems.selectedId = $stateParams.feedId
    $scope.feedItems.selected = ()-> feedItemsService.getFeed()
    $scope.feedItems.feedTitle = ()-> feedItemsService.getFeedTitle()
    $scope.feedItems.feedUnreadCount = ()-> feedItemsService.getFeed()?.unread
    $scope.feedItems.isSubscribed = ()-> feedService.isSubcribed($stateParams.feedId) || !feedItemsService.isRealFeed($stateParams.feedId)
    $scope.feedItems.isRealFeed = ()-> feedItemsService.isRealFeed($stateParams.feedId) && $scope.feedItems.isSubscribed()

    $scope.feedItems.tags = () ->
      selectedTags = {}
      tags = $scope.feedItems.selected()?.tag || []
      for tag in $scope.allTags() when tag?
        do (tag)->
          if tag!='null'
            selectedTags[tag]= tag in tags
      return selectedTags

    $scope.markAsReaded = (id)->
      feedItemsService.markAsReaded(id)

    $scope.changeView = (viewName)->
      $scope.settings.viewMode = viewName
      $state.transitionTo('feeds.items',{feedId:$stateParams.feedId,mode:viewName}, false); #refresh feed to update view


    ###
    Watch settings and reload items if nessesary
    ###
    $scope.$watch 'settings.showUnreadItems', (val, oldVal)->
      #reload items
      if val? and val!=oldVal and !val
        feedItemsService.reloadFeedItems $stateParams.feedId, (items)->
          $scope.feedItems.items = items

    $scope.$watch 'settings.order', (val, oldVal)->
      if val? and val!=oldVal
        feedItemsService.reloadFeedItems $stateParams.feedId, (items)->
          $scope.feedItems.items = items

    $scope.toggleTag= (feedId, tagname)->
      #add to selected first
      feedService.toggleTag(feedId, tagname)

    $scope.addTag= (feedId)->
      d = $dialog.dialog({templateUrl:'feedItems/addTagDialog.tpl.html',controller:'DialogCtrl'});
      d.open().then (tagname)->
        if tagname?
          $scope.toggleTag(feedId, tagname)


    $scope.feedItems.subscribe = ()->
      feedService.subscribeToFeed($scope.feedItems.selected())

    $scope.feedItems.unsubscribe = ()->
      feedService.unsubscribeFromFeed($scope.feedItems.selected())
